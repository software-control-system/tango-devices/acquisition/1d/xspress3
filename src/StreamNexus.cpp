/*******************************************************************************/
/*! 
 *  \file   StreamNexus.cpp
 *  \brief  Class used to manage the stream of acquisition data in nexus files
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*******************************************************************************/

#include "StreamNexus.h"

namespace Xspress3_ns
{
#if defined(USE_NX_FINALIZER)
nxcpp::NexusDataStreamerFinalizer StreamNexus::m_data_streamer_finalizer;
bool StreamNexus::m_is_data_streamer_finalizer_started = false;
#endif

//----------------------------------------------------------------------------------------------------------------------
StreamNexus::StreamNexus(Tango::DeviceImpl *dev)
: Stream(dev), IExceptionHandler(),
m_target_path("TO_BE_DEFINED"),
m_file_name("TO_BE_DEFINED"),
m_write_mode("TO_BE_DEFINED"),
m_nb_acq_per_file(0),
m_nb_bins(4096),
m_nb_frames(1),
m_writer(0)
{
     YAT_INFO << "StreamNexus::StreamNexus()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);
#if defined(USE_NX_FINALIZER)
    if ( !StreamNexus::m_is_data_streamer_finalizer_started )
    {
        YAT_VERBOSE << "Start NexusDataStreamerFinalizer" << std::endl;
        StreamNexus::m_data_streamer_finalizer.start();
        StreamNexus::m_is_data_streamer_finalizer_started = true;
    }
#endif

}

//----------------------------------------------------------------------------------------------------------------------
StreamNexus::~StreamNexus()
{
    YAT_INFO << "StreamNexus::~StreamNexus()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);
    if (m_writer)
    {
        YAT_INFO << "StreamNexus::~StreamNexus() - [delte m_writer]" << std::endl;
        //- this might be required, in case we could not pass the writer to the finalizer	 
        delete m_writer;
        m_writer = 0;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::init(yat::SharedPtr<DataStore> data_store)
{
    YAT_INFO << "StreamNexus::init()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);
    m_store = data_store;
    m_channel_names.clear();
    m_realtime_names.clear();
    m_deadtime_names.clear();
    m_icr_names.clear();
    m_ocr_names.clear();
    m_events_inrun_names.clear();

    int nb_channels = data_store->get_nb_channels();
    m_nb_frames = data_store->get_nb_frames();
    m_nb_bins = data_store->get_nb_bins();

    YAT_INFO << "StreamNexus::init() - Create DataStreamer :" << std::endl;
    YAT_INFO << "\t- target path = " 	 << m_target_path << std::endl;
    YAT_INFO << "\t- file name = "		 << m_file_name << std::endl;
    YAT_INFO << "\t- memory mode = " 	 << m_memory_mode << std::endl;	
    YAT_INFO << "\t- write mode = " 	 << m_write_mode << std::endl;
    YAT_INFO << "\t- nb frames = "	     << m_nb_frames << std::endl;
    YAT_INFO << "\t- nb acq per file = " << m_nb_acq_per_file << std::endl;
    YAT_INFO << "\t- nb bins = "         << m_nb_bins << std::endl;

    YAT_INFO << "\t- write data for : " << std::endl;
	for(unsigned i = 0;i < m_stream_items.size();i++)
    {
        YAT_INFO << "\t\t. " << m_stream_items.at(i) << std::endl;
    }
    YAT_INFO << std::endl;
	
    try
    {
        m_writer = new nxcpp::DataStreamer(m_file_name, m_nb_frames, m_nb_acq_per_file);

		YAT_VERBOSE << "- SetExceptionHandler()" << std::endl;
        m_writer->SetExceptionHandler(this);

        // configure the Memory mode 
		nxcpp::DataStreamer::MemoryMode memory_mode;
		if (m_memory_mode == "NO_COPY" )
		{
			YAT_VERBOSE << "StreamNexus - Configure the Memory mode : NO_COPY" << std::endl;
			memory_mode = nxcpp::DataStreamer::NO_COPY;
		}
		else//by default is COPY
		{
			YAT_VERBOSE << "StreamNexus - Configure the Memory mode : COPY" << std::endl;
			memory_mode = nxcpp::DataStreamer::COPY;
		}
		
        // configure the Writer mode 
        if (m_write_mode == "ASYNCHRONOUS" )
        {
            YAT_VERBOSE << "StreamNexus - Configure the Writer mode : ASYNCHRONOUS" << std::endl;		
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::ASYNCHRONOUS);
        }
        else//by default is SYNCHRONOUS	
        {
            YAT_VERBOSE << "StreamNexus - Configure the Writer mode : SYNCHRONOUS" << std::endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::SYNCHRONOUS);
        }
		
        m_writer->Initialize(m_target_path);	
		
        for (int ichan = 0; ichan < nb_channels; ichan++)
        {
            //- Channels        
            if ( m_is_channel_enabled )
            {
				std::string str_name = m_dataset_prefix + "channel%02d";
                m_channel_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                YAT_VERBOSE << "\t- AddDataItem1D(" << m_channel_names[ichan] << "," << m_nb_bins << ")" << std::endl;
                m_writer->AddDataItem1D(m_channel_names[ichan], m_nb_bins);
				m_writer->SetDataItemMemoryMode(m_channel_names[ichan], memory_mode);
            }

            //- Realtimes
            if ( m_is_realtime_enabled )
            {
				std::string str_name = m_dataset_prefix + "realtime%02d";
                m_realtime_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                YAT_VERBOSE << "\t- AddDataItem0D(" << m_realtime_names[ichan] << ")" << std::endl;
                m_writer->AddDataItem0D(m_realtime_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_realtime_names[ichan], memory_mode);				
            }

             //- Deadtimes
            if ( m_is_deadtime_enabled )
            {
				std::string str_name = m_dataset_prefix + "deadtime%02d";
                m_deadtime_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                YAT_VERBOSE << "\t- AddDataItem0D(" << m_deadtime_names[ichan] << ")" << std::endl;
                m_writer->AddDataItem0D(m_deadtime_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_deadtime_names[ichan], memory_mode);				
            }
            //- ICRs
            if ( m_is_icr_enabled )
            {
				std::string str_name = m_dataset_prefix + "icr%02d";
                m_icr_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                YAT_VERBOSE << "\t- AddDataItem0D(" << m_icr_names[ichan] << ")" << std::endl;
                m_writer->AddDataItem0D(m_icr_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_icr_names[ichan], memory_mode);				
            }

            //- OCRs
            if ( m_is_ocr_enabled )
            {
				std::string str_name = m_dataset_prefix + "ocr%02d";
                m_ocr_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                YAT_VERBOSE << "\t- AddDataItem0D(" << m_ocr_names[ichan] << ")" << std::endl;
                m_writer->AddDataItem0D(m_ocr_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_ocr_names[ichan], memory_mode);				
            }

            //- EventsInRuns
           if ( m_is_events_in_run_enabled )
            {
				std::string str_name = m_dataset_prefix + "eventsInRun%02d";
                m_events_inrun_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                YAT_VERBOSE << "\t- AddDataItem0D(" << m_events_inrun_names[ichan] << ")" << std::endl;
                m_writer->AddDataItem0D(m_events_inrun_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_events_inrun_names[ichan], memory_mode);
            }
        }
	
    }
    catch(const nxcpp::NexusException &ex)
    {
        Tango::Except::throw_exception((ex.errors[0].reason).c_str(),
                                       (ex.errors[0].desc).c_str(),
                                       (ex.errors[0].origin).c_str());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::open()
{
    YAT_INFO << "StreamNexus::open()" << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::close()
{
    YAT_INFO << "StreamNexus::close()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);            
    if (m_writer)
    {        
#if defined (USE_NX_FINALIZER) 
        //- Use NexusFinalizer to optimize the finalize which was extremly long !!!
        YAT_VERBOSE << "StreamNexus::close() - NexusDataStreamerFinalizer" << std::endl;
        nxcpp::NexusDataStreamerFinalizer::Entry *e = new nxcpp::NexusDataStreamerFinalizer::Entry();
        e->data_streamer = m_writer;
        m_writer = 0;
        StreamNexus::m_data_streamer_finalizer.push(e);
#else  
        YAT_VERBOSE << "StreamNexus::close() - Finalize()" << std::endl;
        m_writer->Finalize();
        delete m_writer;
        m_writer = 0;
#endif 
    }
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::abort()
{
    YAT_INFO << "StreamNexus::abort()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);    
    if (m_writer)
    {        
        YAT_VERBOSE << "StreamNexus::abort() - Stop()" << std::endl;
        m_writer->Stop();
        delete m_writer;
        m_writer = 0;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_memory_mode(const std::string & memory_mode)
{
    YAT_INFO << "StreamNexus::set_memory_mode(" << memory_mode << ")" << std::endl;
    m_memory_mode = memory_mode;
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_write_mode(const std::string & write_mode)
{
    YAT_INFO << "StreamNexus::set_write_mode(" << write_mode << ")" << std::endl;
    m_write_mode = write_mode;
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_target_path(const std::string & target_path)
{
    YAT_INFO << "StreamNexus::set_target_path(" << target_path << ")" << std::endl;
    m_target_path = target_path;
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_file_name(const std::string & file_name)
{
    YAT_INFO << "StreamNexus::set_file_name(" << file_name << ")" << std::endl;
    m_file_name = file_name;
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_nb_acq_per_file(int nb_acq_per_file)
{
    YAT_INFO << "StreamNexus::set_nb_acq_per_file(" << nb_acq_per_file << ")" << std::endl;
    m_nb_acq_per_file = nb_acq_per_file;
}

void StreamNexus::set_dataset_prefix(const std::string& dataset_prefix)
{
    YAT_INFO << "StreamNexus::set_dataset_prefix(" << dataset_prefix << ")" << std::endl;
    m_dataset_prefix = dataset_prefix;
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::store_statistics(int channel, double realtime, double deadtime, double icr, double ocr, unsigned long events_in_run)
{
    yat::MutexLock scoped_lock(m_data_lock);
    if (m_writer)
    {
        // realtime
        if ( m_is_realtime_enabled )
        {
            m_writer->PushData(m_realtime_names[channel], &realtime);
        }

        // deadtime
        if ( m_is_deadtime_enabled )
        {
            m_writer->PushData(m_deadtime_names[channel], &deadtime);
        }

        // icr
        if ( m_is_icr_enabled )
        {
            m_writer->PushData(m_icr_names[channel], &icr);
        }

        // ocr
        if ( m_is_ocr_enabled )
        {
            m_writer->PushData(m_ocr_names[channel], &ocr);
        }

        // eventsInRun
        if ( m_is_events_in_run_enabled )
        {
            m_writer->PushData(m_events_inrun_names[channel], &events_in_run);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::store_spectrum_data(int channel, uint32_t* data, size_t length)
{
    yat::MutexLock scoped_lock(m_data_lock);
    if (m_writer)
    {
        if ( m_is_channel_enabled )
        {
            m_writer->PushData(m_channel_names[channel], data);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::update_data(int current_frame, int ichannel, yat::SharedPtr<DataStore> data_store)
{
    yat::MutexLock scoped_lock(m_data_lock);
    YAT_VERBOSE << "StreamNexus::update_data()" <<current_frame<< endl;
    YAT_VERBOSE << "\t- current_frame = " <<current_frame<< endl;
    store_statistics(
                     ichannel,
                     data_store->get_realtime(current_frame, ichannel),
                     data_store->get_deadtime(current_frame, ichannel),
                     data_store->get_input_count_rate(current_frame, ichannel),
                     data_store->get_output_count_rate(current_frame, ichannel),
                     data_store->get_events_in_run(current_frame, ichannel)
                     );
        
    store_spectrum_data(ichannel,
                (uint32_t*) & data_store->get_frame_spectrum_data(current_frame,ichannel)[0],
                data_store->get_frame_spectrum_data(current_frame,ichannel).size());
}

//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::reset_index()
{
    YAT_INFO << "StreamNexus::reset_index()" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    nxcpp::DataStreamer::ResetBufferIndex();
}
//----------------------------------------------------------------------------------------------------------------------
}