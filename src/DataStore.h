/*************************************************************************
/*! 
 * \file DataStore.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class DataStore
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef DATA_STORE_H
#define DATA_STORE_H

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Mutex.h>
#include <yat/memory/DataBuffer.h>
#include <yat4tango/DeviceTask.h>
#include <yat/any/Any.h>
#include <yat/time/Timer.h>
#include <vector>

const size_t DATASTORE_TASK_PERIODIC_MS      = 100;//ms
const size_t DATASTORE_PROCESS_DATA_MSG      = yat::FIRST_USER_MSG + 600;
const size_t DATASTORE_CLOSE_DATA_MSG        = yat::FIRST_USER_MSG + 601;
const size_t DATASTORE_ABORT_DATA_MSG        = yat::FIRST_USER_MSG + 602;
const size_t DATASTORE_ABORT_MSG             = yat::FIRST_USER_MSG + 603;

namespace Xspress3_ns
{
//------------------------------------------------------------------
class FrameData
{
public:
	FrameData()
	{
        m_dead_time = 0.0;
		m_real_time = 0.0;
        m_input_count_rate = 0.0;
		m_output_count_rate = 0.0;
		m_events_in_run = 0;
		m_deadtime_correction_factor = 0.0;
	}

    double m_real_time;
    double m_dead_time;
    double m_input_count_rate; //ICR
    double m_output_count_rate; //OCR
    unsigned long m_events_in_run;
    std::vector<uint32_t> m_spectrum_datas;
    double m_deadtime_correction_factor;
};

//------------------------------------------------------------------
class ChannelsData
{
public:
	ChannelsData()
	{
	}
    std::vector<FrameData> m_channel_data;
};

//------------------------------------------------------------------
class AcquisitionFramesData
{
public:
	AcquisitionFramesData()
	{
	}
    std::vector<ChannelsData> m_frame_data;
};

//------------------------------------------------------------------
struct RoisData
{
	uint32_t begin_roi;
	uint32_t end_roi;
	uint32_t roi_sum;
};


//--------------------------------------------------------------------------------
//! \class DataStore
//! \brief Class to manage the data and notify obervers of data changes
//--------------------------------------------------------------------------------
class DataStore : public yat4tango::DeviceTask
{
public:
//! \brief Constructor
DataStore(Tango::DeviceImpl *dev);

//! \brief Destructor 
virtual ~DataStore();

//! \brief Initilalize Data Store data
void init(int nb_channels, int nb_frames, int nb_bins);

//! \brief Get numbre of xspress3 channels
int get_nb_channels();

//! \brief Get numbre of xspress3 bins
int get_nb_bins();

//! \brief Get the last state of the task
Tango::DevState get_state();

//! \brief Get the last status of the task
std::string get_status();

//! \brief Get spectrum data for the current frame and channel
//! \param frame current frame.
//! \param channel current channel.
std::vector<uint32_t> get_frame_spectrum_data(int frame,int channel);

//! \brief Store acquisition statistics  
void store_statistics_data(int frame, int channel, FrameData frame_data);

//! \brief Store acquisition spectrum data 
void store_spectrum_data(int current_frame, int channel, uint32_t* data, size_t length);    

//! \brief Subscribe to the controller observer
void subscribe(class Controller* observer);

//! \brief Abort storing data
void abort(std::string status);

//! \brief Get channel realtime attribute value
//! \param frame current frame.
//! \param channel current channel.
double get_realtime(int frame, int channel);

//! \brief Get channel deadtime attribute value
//! \param frame current frame.
//! \param channel current channel.
double get_deadtime(int frame, int channel);

//! \brief Get channel deOCRadtime attribute value
//! \param frame current frame.
//! \param channel current channel		
double get_output_count_rate(int frame, int channel);

//! \brief Get channel ICR attribute value
//! \param frame current frame.
//! \param channel current channel
double get_input_count_rate(int frame, int channel);

//! \brief Get channel events_in_run attribute value
//! \param frame current frame.
//! \param channel current channel
unsigned long get_events_in_run(int frame, int channel);

//! \brief Gets current frame number
int get_current_frame() const;

//! \brief Gets number of required frames
int get_nb_frames() const;

//! \brief Close data store
void close_data();

void set_rois_data(int roi_channel, std::vector< std::vector<RoisData> >& rois_data);
void remove_rois_data(int roi_channel);

std::vector< std::vector<RoisData> > get_rois_data() const;

void reset_rois();

protected:
//! \brief Implements yat4tango::DeviceTask pure virtual method
//! \param msg Message to handle.
void process_message(yat::Message& msg);      

private:
//! \brief Set the task state
//! \param state The state.
void set_state(Tango::DevState state);

//! \brief Set the task status
//! \param state The status.
void set_status(const std::string& status); 

//! \brief Inform controller that data  hanged
void notify_data(int ichannel, int current_frame);

//! \brief Update rois values with acquisition results
void update_rois_data(int current_frame, int roi_channel);

//! \brief m_state: Device state
Tango::DevState m_state;

//! \brief m_state: Device status
std::stringstream m_status;

//! \brief m_state_lock: Mutex used to protect state and status access
yat::Mutex m_state_lock;

//! \brief m_data_lock: Mutex used to protect data access
yat::Mutex m_data_lock;

//! \brief m_datas: Acquisition datas
AcquisitionFramesData m_datas;

//! \brief m_controller: the observer class
class Controller* m_controller;

//! \brief m_nb_channels: The number of channels
int m_nb_channels;

//! \brief m_nb_frames: The number of requested frames
int m_nb_frames;

//! \brief m_nb_bins: The number of bins per channel
int m_nb_bins;

//!\brief m_last_acquired_frame: Last acquired frame number
int m_last_acquired_frame;

//! \brief Owner Device server object
Tango::DeviceImpl* m_device;

std::vector<std::vector<RoisData>> m_rois_data;

};

}

#endif // DATA_STORE_H
