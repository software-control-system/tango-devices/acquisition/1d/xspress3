/*************************************************************************
/*! 
 * \file Simulator.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class Simulator
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "Driver.h"

const std::string SIMULATOR_FIRMWARE_REVISION_STR = "99.99";
const std::string SIMULATOR_BOARD_TYPE_STR = "SIMULATOR";
const double NUMERIC_SCALAR_3 = 3.0;
const double NUMERIC_SCALAR_4 = 4.0;
const double NUMERIC_SCALAR_9 = 9.02;
const double NUMERIC_SCALAR_10 = 10.0;
const double SIMULATED_VALUE_CHANNEL_0 = 0;
const double SIMULATED_VALUE_CHANNEL_1 = 11;
const double SIMULATED_VALUE_CHANNEL_2 = 22;
const double SIMULATED_VALUE_CHANNEL_3 = 33;
const double SIMULATED_VALUE_CHANNEL_OTHERS = 55;

namespace Xspress3_ns
{
//--------------------------------------------------------------------------------
//! \class Simulator
//! \brief specialisation class used to simulate the behavior of an Xspress3 module
//--------------------------------------------------------------------------------
class Simulator : public Driver
{
public:
//! \brief Constructor
Simulator(Tango::DeviceImpl* dev,  std::vector<std::string> vec_properties);
                        
//! \brief Destructor 
virtual ~Simulator();

//! \brief Gets numbre of xspress3 cards
int get_nb_cards() const;

//! \brief Gets numbre of xspress3 channels
int get_nb_channels() const;

//! \brief Gets xspress3 firmware revision
std::string get_firmware_revision() const;

//! \brief Gets type of xspress3 card
std::string get_board_type() const;

//! \brief Get current acquired frame number
int get_current_frame() const;

//! \brief Get frames number requested by user
int get_nb_frames() const;

//! \brief Set frames number requested by user
//! \param frames Frame number requested
void set_nb_frames(int frames);

//! \brief Get bins number for xspress3 channel
int get_nb_bins() const;

//! \brief Set bins number for xspress3 channel
//! \param frames Bins number for xspress3 channel
void set_nb_bins(int bins);

//! \brief Get number of acquisition channel scalars
int get_nb_scalars() const;

//! \brief Set bins number of channel scalars
//! \param scalars scalars number for xspress3 channel
void set_nb_scalars(int scalars);

//! \brief Get number element in spectrum data
ulong get_nb_elements() const;

//! \brief Get requested trigger mode
TriggerMode get_trigger_mode() const;

//! \brief Set requested trigger mode
//! \param trigger_mode Requested trigger mode
void set_trigger_mode(TriggerMode trigger_mode);

//! \brief Get the requested exposure time per frame
double get_exposure_time() const;

//! \brief Set the requested exposure time per frame
//! \param exposure_time The exposure time value
void set_exposure_time(double exposure_time);

//! \brief Get current device state
Tango::DevState get_state() ;

//! \brief Get current device status
std::string get_status();

//! \brief Start acquisition action
void start_acquisition();

//! \brief Stop acquisition action
void stop_acquisition();

//! \brief Get is stop acquisition is requested
bool is_abort() const;

//! \brief Set is stop acquisition is requested
//! \param abort Boolean to send stop acquisition request
void abort(bool abort);

//! \brief Chek is acquisition is running
bool is_running() const;


//! \brief Get spectrum data
std::vector<Tango::DevULong> get_spectrum_data(int current_frame, int channel);

//! \brief Get statistics data
void get_statistics_data(int frame, int channel, const char* name, void* value);

private:
//! \brief Set current device state
void set_state(Tango::DevState state);

//! \brief Vector of string containing tango properties values
std::vector<std::string> m_vec_properties;

//! \brief The number of xspress3 cards that constitute the xspress3 system 
int m_nb_cards;

//! \brief The number of channels to be configured in the xspress3 system
int m_nb_channels;

//! \brief Boolean to check stop acquisition action
bool m_is_abort;

//! \brief Boolean to check if acquisition is running
bool m_is_running;

//! \brief The number of bins per channel
int m_nb_bins;

//! \brief Number of scalars in frames
int m_nb_scalars;

//! \brief Device state
Tango::DevState m_state;

//! \brief Device status
std::string m_status;

//! \brief Acquisition exposure time
double m_exp_time;

//! \brief Number of frames to acquired
int m_nb_frames;

//! \brief Maximmum frames number
int m_max_frames;

//! \brief Number of frames acquired
int m_nb_acq_frames;

//! \brief Vector of vector<uint32_t*> contaning acquired frames data
std::vector<std::vector<uint32_t*>> m_pixel_datas;

//! \brief Map of statistics parameters 
std::map<std::string, int> m_statistics_attribute_map;

//! \brief m_state_lock: Mutex used to protect state and status access
yat::Mutex m_state_lock;

//! \brief Mutex to prevent concurrent access
yat::Mutex m_locker;

//! \brief Trigger mode
TriggerMode m_trigger_mode;

};

}

#endif // SIMULATOR_H
