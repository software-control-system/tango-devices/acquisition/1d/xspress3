/*************************************************************************
/*! 
 * \file Xspress3Handler.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class Xspress3Handler
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef XSPRESS3_HANDLER_H
#define XSPRESS3_HANDLER_H

#include "Driver.h"
#include <yat/threading/Mutex.h>
#include <yat/threading/Condition.h>
#include <yat/threading/Thread.h>
#include <yat/memory/SharedPtr.h>

enum ItfgTriggerMode
{
    Burst,				///<  Run burst of back to back frames.
    SoftwarePause,		///< Pause before every frame and wait for rising edge on CountEnb bit.
    HardwarePause,		///< Pause before every frame and wait for rising edge on TTL_IN(1).
    SoftwareOnlyFirst,	///< Pause before first frame and wait for rising edge on CountEnb bit.
    HardwareOnlyFirst	///< Pause before first frame and wait for rising edge on TTL_IN(1).
};

enum ItfgGapMode 
{
    Gap25ns,	///< Minimal gap between frames. Care when using multiple boxes. Short cables and/or termination. 0 debounce time.
    Gap200ns,	///< 200ns gap between frames. Use short cables and short (approx 10 cycle debounce time) when using multiple boxes.
    Gap500ns,	///< 500ns gap between frames. Use approx 30 cycle debounce time when using multiple boxes.
    Gap1us		///< 1us gap between frames. Allows long cables and  approx 70 cycle debounce time when using multiple boxes.
};

enum ClockSrc 
{
    /*IntClk = */XSP3_CLK_SRC_INT,		///< channel processing clock comes from fpga processor (testing only)
    /*XtalClk = */XSP3_CLK_SRC_XTAL,	///< adc and channel processing clock from crystal on the ADC board (normal single board or master operation).
    /*ExtClk =  */XSP3_CLK_SRC_EXT,  	///< adc and channel processing clock from lemo clock connector on ADC board (slave boards)
    /*Future =  */XSP3_CLK_SRC_FPGA,		///< not implemented, for future expansion
    /*Mini =  */XSP3M_CLK_SRC_CDCM61004, //< X3m cdmc clock
    /*X4AdcLMK =  */XSP3M_CLK_SRC_LMK61E2,      ///< X4 ADC board lmk at 100mhz
    /*X4MplLMK =  */XSP4_CLK_SRC_MIDPLN_LMK61E2 ///< X4 midplane clock at 100mhz
};

enum ClockFlags 
{
    /*Master = */XSP3_CLK_FLAGS_MASTER,				///< this clock generate clocks for other boards in the system
    /*NoDither =  */XSP3_CLK_FLAGS_NO_DITHER,		///< disables dither within the ADC
    /*Stage1Only = */XSP3_CLK_FLAGS_STAGE1_ONLY,	///< performs stage of the lmk 03200 setup, does not enable zero delay mode
    /*NoCheck = */XSP3_CLK_FLAGS_NO_CHECK,			///< dont check for lock detect from lmk 03200
    /*TpEnb = */XSP3_CLK_FLAGS_TP_ENB,				///< enable test pattern from spartans
    /*DisOverTemp = */XSP3_CLK_FLAGS_DIS_OVER_TEMP,	///< Disable Over temperature protection on ADC Board
    /*Shutdown0 = */XSP3_CLK_FLAGS_SHUTDOWN0,		///< Shutdown ADC channel 0
    /*Shutdown123 =  */XSP3_CLK_FLAGS_SHUTDOWN123,	///< Shutdown ADC channels 123
    /*Shutdown4 =  */XSP3_CLK_FLAGS_SHUTDOWN4,		///< Shutdown ADC channel 4 (middle (unused?))
    /*Shutdown5678 = */XSP3_CLK_FLAGS_SHUTDOWN5678	///< Shutdown ADC channel5678 last 4
};

namespace Xspress3_ns
{
//--------------------------------------------------------------------------------
//! \class Xspress3Handler
//! \brief specialisation class used to control an Xspress3 module
//--------------------------------------------------------------------------------
class Xspress3Handler : public Driver
{
public:
//! \brief Constructor
Xspress3Handler(Tango::DeviceImpl* dev, std::vector<std::string> vec_properties);
                      
//! \brief Destructor 
virtual ~Xspress3Handler();

//! \brief Gets numbre of xspress3 cards
int get_nb_cards() const;

//! \brief Get numbre of xspress3 cards
int get_nb_channels() const;

//! \brief Gets xspress3 firmware revision
std::string get_firmware_revision() const;

//! \brief Gets type of xspress3 card
std::string get_board_type() const;

//! \brief Get current acquired frame number
int get_current_frame() const; 

//! \brief Get frames number requested by user
int get_nb_frames() const;

//! \brief Set frames number requested by user
//! \param frames Frame number requested
void set_nb_frames(int frames);

//! \brief Get number element in spectrum data
ulong get_nb_elements() const;

//! \brief Get bins number for xspress3 channel
int get_nb_bins() const;

//! \brief Set bins number for xspress3 channel
//! \param frames Bins number for xspress3 channel
void set_nb_bins(int bins);

//! \brief Get number of acquisition channel scalars
int get_nb_scalars() const;

//! \brief Set bins number of channel scalars
//! \param scalars scalars number for xspress3 channel
void set_nb_scalars(int scalars);

//! \brief Get requested trigger mode
TriggerMode get_trigger_mode() const;

//! \brief Set requested trigger mode
//! \param trigger_mode Requested trigger mode
void set_trigger_mode(TriggerMode trigger_mode);

//! \brief Get the requested exposure time per frame
double get_exposure_time() const;

//! \brief Set the requested exposure time per frame
//! \param exposure_time The exposure time value
void set_exposure_time(double exposure_time);

//! \brief Get current device state
Tango::DevState get_state();

//! \brief Get current device status
std::string get_status();

//! \brief Start acquisition action
void start_acquisition();

//! \brief Stop acquisition action
void stop_acquisition();

//! \brief Get is stop acquisition is requested
bool is_abort() const;

//! \brief Set is stop acquisition is requested
//! \param abort Boolean to send stop acquisition request
void abort(bool abort);

//! \brief Chek is acquisition is running
bool is_running() const;


//! \brief Get spectrum data
std::vector<Tango::DevULong> get_spectrum_data(int current_frame, int channel);

//! \brief Get statistics data
void get_statistics_data(int frame, int channel, const char* name, void* value);

private:
//! \brief Connects to the Xspress3
void init();

//! \brief Select the currently active card
//! \param card is the number of the card in the xspress3 system
void set_card(int card) ;

//! \brief Set current device state
void set_state(Tango::DevState state);

//! \brief Set current device status
void set_status(const std::string& status);

//! \brief Check is frame is reading is done
//! \param frame_num Current frame number
void check_progress(int& frame_num);

//! \brief Read frame from hardware
//! \param frame_nb Number of frames requested
void read_frame(int frame_nb);

///! \brief Setup time framing source according to Trigger mode
void set_timing_mode();

//! \brief Setup time framing source
void set_timing(int time_src, int fixed_time, int alt_ttl_mode, int debounce, bool loop_io=false, bool f0_invert=false, bool veto_invert=false);

//! \brief Setup internal time frame generator
void set_itfg_timing(int nframes, int triggerMode, int gapMode);

//! \brief Setup ADC data processing clocks source
void setup_clocks(int clk_src, int flags, int tp_type=0);

//! \brief Set the run modes flags
void set_run_mode(bool playback=false, bool scope=false, bool scalers=true, bool hist=true);

//! \brief Restore all settings from file
void restore_settings(bool force_mismatch=false);

//! \brief Initalize the regions of interest. Removes any existing regions of interest
void init_roi(int chan);

//! \brief Start histogram
void start();

//! \brief Restart histogram
void restart();

//! \brief Pause histogram
void pause();

//! \brief Stop histogram
void stop();

//! \brief The number of xspress3 cards that constitute the xspress3 system 
int m_nb_cards;
//! \brief The number of channels to be configured in the xspress3 system
int m_nb_channels;
//! \brief The maximum number of time frames
int m_max_frames;
//! \brief The IP address of the host server
std::string m_base_ip_address;
//! \brief The IP port number or -1 to use the default
int m_base_port;
//! \brief The base MAC address for card 1
std::string m_base_mac_address;
//! \brief Integer flag to turn debug messages off/on/verbose
int m_debug;
//! \brief handle to the top level of the xspress3 system
int m_handle;
//! \brief Card number
int m_card;

//! \brief Device state
Tango::DevState m_state;

//! \brief Device status
std::stringstream m_status;

//! \brief Acquisition exposure time
double m_exp_time;

//! \brief Number of frames to acquired
int m_nb_frames;

//! \brief Number of frames acquired
int m_nb_acq_frames;

//! \brief Number of scalars in frames
int m_nb_scalars;

int m_nb_scalars_without_deadtime_factors;

//! \brief Vector of u_int32_t contaning acquired frames data
std::vector< yat::SharedPtr<u_int32_t> > m_data;

//! \brief Vector of string containing tango properties values
std::vector<std::string> m_vec_properties;

//! \brief Boolean to check stop acquisition action
bool m_is_abort;

//! \brief Boolean to check if acquisition is running
bool m_is_running;

//! \brief The number of bins per channel
int m_nb_bins;

//! \brief Mutex to prevent concurrent access
yat::Mutex m_locker;

//! \brief Mutex to prevent concurrent state and status access
yat::Mutex m_state_locker;

//! \brief Mutex to prevent concurrent m_data/m_vec_deadtime_parameters access
yat::Mutex m_data_locker;

//! \brief Trigger mode
TriggerMode m_trigger_mode;

//! \brief Map of statistics parameters 
std::map<std::string, int> m_statistics_attribute_map;

//! \brief Smart pointer to get data
yat::SharedPtr<u_int32_t> m_buffer;

//! \brief Path to calibrations files
std::string m_config_directory_name;

//! \brief Vector to store evtwidth/resets/allevt/ctime values for each channel
std::vector<std::vector<double>> m_vec_deadtime_parameters;

//! \brief The driver polling period
double m_expert_acquistion_polling_period ;

};

}

#endif // XSPRESS3_HANDLER_H
