/*************************************************************************/
/*! 
 *  \file   Controller.cpp
 *  \brief  Implementation of Controller methods.
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include "Controller.h"

#include <tango.h>
#include <yat/utils/Logging.h>

namespace Xspress3_ns
{
// Constructor
Controller::Controller(Tango::DeviceImpl* dev, std::vector<std::string> vec_properties, std::vector<std::string> vec_stream_items, std::string expert_stream_memory_mode, std::string expert_stream_write_mode)
 : yat4tango::DeviceTask(dev),
    m_device(dev),
    m_board_type(BOARD_TYPE_XSPRESS3_TYPE),
    m_vec_properties(vec_properties),
    m_is_file_generation(false),
    m_stream_items(vec_stream_items),
    m_stream_type(STREAM_TYPE_NO_STREAM),
    m_current_stream_type(STREAM_TYPE_NO_STREAM),
    m_stream_memory_mode(expert_stream_memory_mode),
    m_stream_write_mode(expert_stream_write_mode),
    m_stream_target_path("./"),
    m_dataset_prefix(""),
    m_current_stream_target_path(""),
    m_stream_target_file("stream"),
    m_current_stream_target_file("current"),
    m_stream_nb_acq_per_file(1),
    m_current_stream_nb_acq_per_file(1),
    m_is_acquisition_initialized(false)
{
    YAT_INFO << "Controller::Controller() "  << std::endl;

    if(m_vec_properties.size() > 0)
    {
        m_board_type = m_vec_properties.at(0);
    }

    enable_timeout_msg(false);
    enable_periodic_msg(false);
    set_periodic_msg_period(100);
    set_status("");
    set_state(Tango::OFF);
}

// Destructor
Controller::~Controller()
{
    YAT_INFO << "Controller::~Controller()" << endl;
    m_vec_properties.clear();
    if (get_state() != Tango::STANDBY && get_state() != Tango::OFF && m_is_acquisition_initialized )
    {
        stop_acquisition();
    }
}

// Controller::Initialize Acquisition / View / DataStore and Stream
void Controller::Initialize()
{
    YAT_INFO << "Controller::Initialize()" << std::endl;
    try
    {
        build_data_store();
        //subscribe Controller to the data store in order to notify controller if data store is changed.
        m_store->subscribe(this);
        
        build_acquisition();

        build_attributes();

        //update stream parameters and initialize adequate stream
        build_stream();
        update_stream_parameters(m_stream_type, m_stream_target_path, m_stream_target_file , m_dataset_prefix, m_stream_nb_acq_per_file);

        set_state(Tango::STANDBY);
        set_status("Xspress3 connection open");
    }
    catch (Tango::DevFailed& df)
    {
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << endl;
        status << "Desc\t: " << df.errors[0].desc << endl;
        YAT_ERROR << "Controller::Initialize() - " << status.str() << endl;
        set_status(status.str());
        set_state(Tango::FAULT);
        
        Tango::Except::throw_exception("CONFIG_ERROR",
                std::string(df.errors[0].desc).c_str(),
                "Controller::Initialize()");
    }
}

void Controller::write_attribute_at_init()
{
    m_attr_view->write_attribute_at_init();
}

void Controller::build_acquisition()
{
    YAT_INFO << "Controller::build_acquisition()" << std::endl;
    try
    {
        m_acquisition.reset(new Acquisition(m_device, m_vec_properties, m_store), yat4tango::DeviceTaskExiter());
        m_is_acquisition_initialized = true;
        m_acquisition->go();
    }
    catch (Tango::DevFailed& df)
    {
        m_is_acquisition_initialized = false;
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << std::endl;
        status << "Desc\t: " << df.errors[0].desc << std::endl;
        YAT_ERROR << "Controller::build_acquisition() - " << status.str()  << std::endl;
        set_status(status.str());

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                std::string(df.errors[0].desc).c_str(),
                "Controller::build_acquisition()");
    }
    return ;
}

void Controller::build_data_store()
{
    YAT_INFO << "Controller::build_data_store()" << std::endl;
    try
    {
        m_store.reset(new DataStore(m_device), yat4tango::DeviceTaskExiter());
        m_store->go();
    }
    catch (Tango::DevFailed& df)
    {
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << std::endl;
        status << "Desc\t: " << df.errors[0].desc << std::endl;
        YAT_ERROR << "Controller::build_data_store() - " << status.str()  << std::endl;
        set_status(status.str());

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                std::string(df.errors[0].desc).c_str(),
                "Controller::build_data_store()");
    }
    return ;
}

void Controller::build_attributes()
{
    YAT_INFO << "Controller::build_attributes()" << std::endl;
    try
    {
        m_attr_view.reset(new AttrView(m_device));
    }
    catch (Tango::DevFailed& df)
    {
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << std::endl;
        status << "Desc\t: " << df.errors[0].desc << std::endl;
        YAT_ERROR << "Controller::build_attributes() - " << status.str()  << std::endl;
        set_status(status.str());

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                std::string(df.errors[0].desc).c_str(),
                "Controller::build_attributes()");
    }
    return ;
}

void Controller::build_stream()
{
    YAT_INFO << "Controller::build_stream()" << std::endl;
    try
    {
        if(m_stream_type == STREAM_TYPE_NEXUS)
        {
            m_stream.reset(new StreamNexus(m_device));
            static_cast<StreamNexus*>(m_stream.get())->set_target_path(m_stream_target_path);
            static_cast<StreamNexus*>(m_stream.get())->set_file_name(m_stream_target_file);
            static_cast<StreamNexus*>(m_stream.get())->set_memory_mode(m_stream_memory_mode);
            static_cast<StreamNexus*>(m_stream.get())->set_write_mode(m_stream_write_mode);
            static_cast<StreamNexus*>(m_stream.get())->set_nb_acq_per_file(m_stream_nb_acq_per_file);
            static_cast<StreamNexus*>(m_stream.get())->set_dataset_prefix(m_dataset_prefix);
            
        }
        else if(m_stream_type == STREAM_TYPE_CSV)
        {
            m_stream.reset(new StreamCsv(m_device));
            static_cast<StreamCsv*>(m_stream.get())->set_target_path(m_stream_target_path);
            static_cast<StreamCsv*>(m_stream.get())->set_file_name(m_stream_target_file);
        }
        else if(m_stream_type == STREAM_TYPE_NO_STREAM)
        {
            m_stream.reset(new StreamNo(m_device));
        }
        else
        {
            std::ostringstream ossMsgErr;
            ossMsgErr   <<  "Wrong Stream Type:\n"
            "Possibles values are:\n"
            "NO_STREAM\n"
            "CSV_STREAM\n"
            "NEXUS_STREAM"
            <<std::endl;
            Tango::Except::throw_exception("LOGIC_ERROR",
                                           ossMsgErr.str().c_str(),
                                           "Controller::build_stream()");
        }
    }
    catch (Tango::DevFailed& df)
    {
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << std::endl;
        status << "Desc\t: " << df.errors[0].desc << std::endl;
        YAT_ERROR << "Controller::build_stream() - " << status.str() << std::endl;
        set_status(status.str());

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                std::string(df.errors[0].desc).c_str(),
                "Controller::build_stream()");
    }
    
    return;
}

void Controller::update_data(int ichannel, int current_frame)
{
    m_stream->update_data(current_frame, ichannel, m_store);
    m_attr_view->update_data(current_frame, ichannel, m_store);
}

// Controller::process_message
void Controller::process_message(yat::Message& msg)
{
    try
    {
        switch (msg.type())
        {
            case yat::TASK_INIT:
            {
                YAT_VERBOSE << "Controller::process_message::TASK_INIT" << std::endl;
                set_state(Tango::OFF);
            }
            break;

            case kMSG_STOP_CAPTURE:
            {
                YAT_VERBOSE << "Controller::process_message::kMSG_STOP_CAPTURE" << endl;
                m_acquisition->stop_acquisition();
            }
            break;

            case kMSG_START_CAPTURE:
            {
                YAT_VERBOSE << "Controller::process_message::kMSG_START_CAPTURE" << endl;
                m_acquisition->start_acquisition();
                yat::Message* msg = yat::Message::allocate(kMSG_END_CAPTURE, DEFAULT_MSG_PRIORITY, true);
                post(msg);
            }
            break;

            case kMSG_END_CAPTURE : 
            {
                YAT_VERBOSE << "Controller::process_message::kMSG_END_CAPTURE" << endl;
                set_state(Tango::STANDBY);
                yat::Message* msg = yat::Message::allocate(kMSG_GET_DATA, DEFAULT_MSG_PRIORITY, true);
                post(msg);
            }
            break;

            case kMSG_GET_DATA : 
            {
                YAT_VERBOSE << "Controller::process_message::kMSG_GET_DATA" << endl;
            }
            break;

            case yat::TASK_EXIT:
            {
               YAT_VERBOSE << "Controller::process_message::TASK_EXIT" << std::endl;
            }
            break;
            
            case yat::TASK_TIMEOUT:
            {
               YAT_ERROR << "Controller::process_message::TASK_TIMEOUT" << std::endl;
            }
            break;

            default:
            {
               YAT_ERROR << "Controller::process_message::unhandled msg type received!" << std::endl;
            }
            break;

        }//switch
    }//try
    catch (yat::Exception& ex)
    {
        std::stringstream error_msg("");
        error_msg.str("");
        error_msg << "Origin\t: " << ex.errors[0].origin << endl;
        error_msg << "Desc\t: " << ex.errors[0].desc << endl;
        YAT_ERROR << "Exception from - process_message() : " << error_msg.str()<<endl;
        set_status(error_msg.str());

        //- rethrow exception
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                    std::string(error_msg.str()).c_str(),
                                    "Controller::process_message()");
    }
}

int Controller::get_nb_cards() const
{
    return m_acquisition->get_nb_cards();
}

int Controller::get_nb_channels() const
{
    return m_acquisition->get_nb_channels();
}

std::string Controller::get_firmware_revision() const
{
    return m_acquisition->get_firmware_revision();
}

std::string Controller::get_board_type() const
{
    return m_acquisition->get_board_type();
}

std::string Controller::get_current_mode() const
{
    return "MAPPING";
}

int Controller::get_nb_bins() const
{
    return m_acquisition->get_nb_bins();
}

void Controller::set_state(const Tango::DevState state)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

Tango::DevState Controller::get_state()
{
    yat::MutexLock scoped_lock(m_state_lock);
    compute_state_status();
    return m_state;
}

void Controller::set_status(const std::string& status)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

std::string Controller::get_status()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

// Compute internal state/status of task
void Controller::compute_state_status()
{
    yat::MutexLock scoped_lock(m_state_lock);
    if(m_state != Tango::DISABLE && m_state != Tango::OFF && m_acquisition && m_store)
    {
        if (m_store->get_state() == Tango::FAULT)
        {
            set_state(Tango::FAULT);
            set_status(m_store->get_status());

        }
        if (m_acquisition->get_state() == Tango::FAULT)
        {
            set_state(Tango::FAULT);
            set_status(m_acquisition->get_status());
        }
        else if (m_acquisition->get_state() == Tango::RUNNING)
        {
            set_state(Tango::RUNNING);
            set_status("Acquisition is in progress ...");
        }                
        else if(m_store->get_state() == Tango::RUNNING)
        {
            set_state(Tango::RUNNING);
            set_status("Storage is in progress ...");
        }
        else
        {
            Tango::DevState state = m_acquisition->get_state();
            set_state(state);
            if(state == Tango::STANDBY)
            {
                set_status("Waiting for request ...");
            }
            else
            {
                set_status(m_acquisition->get_status());
            }
        }
    }
}

int Controller::get_current_frame() const
{
    return m_acquisition->get_current_frame();
}

int Controller::get_nb_frames() const
{
    return m_acquisition->get_nb_frames();
}

void Controller::set_nb_frames(int frames )
{
    m_acquisition->set_nb_frames(frames);
}

TriggerMode Controller::get_trigger_mode() const
{
    return m_acquisition->get_trigger_mode();
}

void Controller::set_trigger_mode(TriggerMode trigger_mode)
{
    m_acquisition->set_trigger_mode(trigger_mode);
}

double Controller::get_exposure_time() const
{
    return m_acquisition->get_exposure_time();
}

void Controller::set_exposure_time(double exposure_time)
{
    m_acquisition->set_exposure_time(exposure_time);
}

void Controller::set_is_file_generation(bool is_file_generation)
{
    m_is_file_generation = is_file_generation;
}

bool Controller::get_file_generation() const
{
    return m_is_file_generation;
}

string Controller::get_stream_type() const
{
    return m_stream_type;
}

void Controller::set_stream_type(std::string stream_type)
{
    m_stream_type = stream_type;
}

void Controller::set_stream_target_path(std::string stream_target_path)
{
    YAT_VERBOSE << "Controller::set_stream_target_path ( " << stream_target_path << " )" << std::endl;
    m_stream_target_path = stream_target_path;
}

std::string Controller::get_stream_target_path() const
{
    return m_stream_target_path;
}

void Controller::set_stream_target_file(std::string stream_target_file)
{
    YAT_VERBOSE << "Controller::set_stream_target_file ( " << stream_target_file << " )" << std::endl;
    m_stream_target_file = stream_target_file;
}

std::string Controller::get_stream_target_file() const
{
    return m_stream_target_file;
}

void Controller::set_stream_nb_acq_per_file(int stream_nb_acq_per_file)
{
    m_stream_nb_acq_per_file = stream_nb_acq_per_file;
}

int Controller::get_stream_nb_acq_per_file() const
{
    return m_stream_nb_acq_per_file;
}

void Controller::set_dataset_prefix(const std::string& dataset_prefix)
{
    m_dataset_prefix = dataset_prefix;
}

void Controller::update_stream_parameters(std::string stream_type, string stream_path, string stream_file, string stream_prefix,int stream_nb_acq_per_file)
{
    YAT_VERBOSE << "Controller::update_stream_parameters()" << std::endl;
    try
    {
        if(stream_type != m_current_stream_type)
        {
            m_current_stream_type = stream_type;
            build_stream();
        }

        if(stream_path != m_current_stream_target_path)
        {
            m_current_stream_target_path = stream_path;
            if(m_stream_type == STREAM_TYPE_NEXUS)
            {
                static_cast<StreamNexus*>(m_stream.get())->set_target_path(m_current_stream_target_path);
            }
            if(m_stream_type == STREAM_TYPE_CSV)
            {
                static_cast<StreamCsv*>(m_stream.get())->set_target_path(m_current_stream_target_path);
            }
        }
        
        if(stream_file != m_current_stream_target_file)
        {
            m_current_stream_target_file = stream_file;
            if(m_stream_type == STREAM_TYPE_NEXUS)
            {
                static_cast<StreamNexus*>(m_stream.get())->set_file_name(m_current_stream_target_file);
            }
            if(m_stream_type == STREAM_TYPE_CSV)
            {
                static_cast<StreamCsv*>(m_stream.get())->set_file_name(m_current_stream_target_file);
            }
        }

        if(stream_nb_acq_per_file != m_current_stream_nb_acq_per_file)
        {
            m_current_stream_nb_acq_per_file = stream_nb_acq_per_file;
            if(m_stream_type == STREAM_TYPE_NEXUS)
            {
                static_cast<StreamNexus*>(m_stream.get())->set_nb_acq_per_file(m_current_stream_nb_acq_per_file);
            }
        }

        if (stream_prefix != m_dataset_prefix)
        {
            m_dataset_prefix = stream_prefix;
            if (m_stream_type == STREAM_TYPE_NEXUS)
            {
                static_cast<StreamNexus*>(m_stream.get())->set_dataset_prefix(m_dataset_prefix);
            }
        }

        //enable/disable writing statistics
        m_stream->set_stream_items(m_stream_items);   
        //always in order to instantiate a new writer

        m_stream->init(m_store);

    }
    catch (Tango::DevFailed& df)
    {
        set_state(Tango::FAULT);

        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << std::endl;
        status << "Desc\t: " << df.errors[0].desc << std::endl;
        status << "Reason\t: " << df.errors[0].reason << std::endl;

        YAT_ERROR << "Controller::build_stream() - " << status.str() << std::endl;
        set_status(status.str());

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                std::string(df.errors[0].desc).c_str(),
                "Controller::update_stream_parameters()");
    }
    return ;
}

void Controller::start_acquisition()
{
    YAT_INFO << "Controller::start_acquisition()" << std::endl;
    try
    {
        yat::MutexLock scoped_lock(m_state_lock);
        int nb_frames = get_nb_frames();
        int nb_channel = get_nb_channels();
        int nb_bins = get_nb_bins();

        m_store->init(nb_channel, nb_frames, nb_bins);
        
        update_stream_parameters(m_stream_type, m_stream_target_path, m_stream_target_file , m_dataset_prefix, m_stream_nb_acq_per_file);

        //Reset current frame value at a new snap command
        m_attr_view->init_acquisition_parametres();
       
        yat::Message* msg = yat::Message::allocate(kMSG_START_CAPTURE, DEFAULT_MSG_PRIORITY, true);
        post(msg);
    }
    catch (Tango::DevFailed& df)
    {
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << endl;
        status << "Desc\t: " << df.errors[0].desc << endl;
        status << "Reason\t: " << df.errors[0].reason << endl;

        set_status(status.str());
         //- rethrow exception
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                   std::string(df.errors[0].desc).c_str(),
                                   "Controller::start_acquisition()");
    }
}

void Controller::update_view()
{
    set_state(Tango::DISABLE);
    set_status(std::string("Update attributes according to Rois configurations ..."));
    m_attr_view->init(m_store);
    set_state(Tango::STANDBY);
}

void Controller::stop_acquisition()
{
    YAT_INFO << "Controller::stop_acquisition()" << std::endl;
    try
    {
        yat::MutexLock scoped_lock(m_state_lock);
        yat::Message* msg = yat::Message::allocate(kMSG_STOP_CAPTURE, HIGHEST_MSG_PRIORITY, false);
        m_acquisition->post(msg);
    }
    catch (Tango::DevFailed& df)
    {
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << endl;
        status << "Desc\t: " << df.errors[0].desc << endl;
        status << "Reason\t: " << df.errors[0].reason << endl;
        
        set_status(status.str());
         //- rethrow exception
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                   std::string(df.errors[0].desc).c_str(),
                                   "Controller::stop_acquisition()");
    }
}

void Controller::close_stream()
{
    YAT_INFO << "Controller::close_stream() " << endl;
    m_stream->close();
}

void Controller::set_rois_data(int roi_channel, std::vector< std::vector<RoisData> >& rois_data)
{
	m_store->set_rois_data(roi_channel, rois_data);
}

void Controller::remove_rois_data(int roi_channel)
{
    m_store->remove_rois_data(roi_channel);
}

void Controller::reset_rois()
{
    m_store->reset_rois();
}

std::vector< std::vector<RoisData> > Controller::get_rois_data() const
{
	return m_store->get_rois_data();
}

void Controller::stream_reset_index()
{
    YAT_INFO << "Controller::stream_reset_index()" << endl;
    try
    {
        if(m_stream)
        {
            m_stream->reset_index();
        }
    }
    catch (Tango::DevFailed& df)
    {
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << endl;
        status << "Desc\t: " << df.errors[0].desc << endl;
        status << "Reason\t: " << df.errors[0].reason << endl;
        
        set_status(status.str());
         //- rethrow exception
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                   std::string(df.errors[0].desc).c_str(),
                                   "Controller::stream_reset_index()");
    }
}

}
