/*----- PROTECTED REGION ID(Xspress3Class.cpp) ENABLED START -----*/
static const char *RcsId      = "$Id:  $";
static const char *TagName    = "$Name:  $";
static const char *CvsPath    = "$Source:  $";
static const char *SvnPath    = "$HeadURL:  $";
static const char *HttpServer = "http://www.esrf.eu/computing/cs/tango/tango_doc/ds_doc/";
//=============================================================================
//
// file :        Xspress3Class.cpp
//
// description : C++ source for the Xspress3Class.
//               A singleton class derived from DeviceClass.
//               It implements the command and attribute list
//               and all properties and methods required
//               by the Xspress3 once per process.
//
// project :     Xspress3 TANGO device.
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <Xspress3Class.h>
#include <yat4tango/UndefDebugStream.h>


/*----- PROTECTED REGION END -----*/	//	Xspress3Class.cpp

//-------------------------------------------------------------------
/**
 *	Create Xspress3Class singleton and
 *	return it in a C function for Python usage
 */
//-------------------------------------------------------------------
extern "C" {
#ifdef _TG_WINDOWS_

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_Xspress3_class(const char *name) {
		return Xspress3_ns::Xspress3Class::init(name);
	}
}

namespace Xspress3_ns
{
//===================================================================
//	Initialize pointer for singleton pattern
//===================================================================
Xspress3Class *Xspress3Class::_instance = NULL;

//--------------------------------------------------------
/**
 * method : 		Xspress3Class::Xspress3Class(string &s)
 * description : 	constructor for the Xspress3Class
 *
 * @param s	The class name
 */
//--------------------------------------------------------
Xspress3Class::Xspress3Class(string &s):Tango::DeviceClass(s)
{
	cout2 << "Entering Xspress3Class constructor" << endl;
	set_default_property();
	write_class_property();

	/*----- PROTECTED REGION ID(Xspress3Class::constructor) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::constructor

	cout2 << "Leaving Xspress3Class constructor" << endl;
}

//--------------------------------------------------------
/**
 * method : 		Xspress3Class::~Xspress3Class()
 * description : 	destructor for the Xspress3Class
 */
//--------------------------------------------------------
Xspress3Class::~Xspress3Class()
{
	/*----- PROTECTED REGION ID(Xspress3Class::destructor) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::destructor

	_instance = NULL;
}


//--------------------------------------------------------
/**
 * method : 		Xspress3Class::init
 * description : 	Create the object if not already done.
 *                  Otherwise, just return a pointer to the object
 *
 * @param	name	The class name
 */
//--------------------------------------------------------
Xspress3Class *Xspress3Class::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new Xspress3Class(s);
		}
		catch (bad_alloc &)
		{
			throw;
		}
	}
	return _instance;
}

//--------------------------------------------------------
/**
 * method : 		Xspress3Class::instance
 * description : 	Check if object already created,
 *                  and return a pointer to the object
 */
//--------------------------------------------------------
Xspress3Class *Xspress3Class::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}



//===================================================================
//	Command execution method calls
//===================================================================
//--------------------------------------------------------
/**
 * method : 		SnapClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *SnapClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "SnapClass::execute(): arrived" << endl;
	((static_cast<Xspress3 *>(device))->snap());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		StopClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *StopClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "StopClass::execute(): arrived" << endl;
	((static_cast<Xspress3 *>(device))->stop());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		SetRoisFromListClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *SetRoisFromListClass::execute(Tango::DeviceImpl *device, const CORBA::Any &in_any)
{
	cout2 << "SetRoisFromListClass::execute(): arrived" << endl;
	const Tango::DevVarStringArray *argin;
	extract(in_any, argin);
	((static_cast<Xspress3 *>(device))->set_rois_from_list(argin));
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		RemoveRoisClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *RemoveRoisClass::execute(Tango::DeviceImpl *device, const CORBA::Any &in_any)
{
	cout2 << "RemoveRoisClass::execute(): arrived" << endl;
	Tango::DevLong argin;
	extract(in_any, argin);
	((static_cast<Xspress3 *>(device))->remove_rois(argin));
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		GetRoisClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *GetRoisClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "GetRoisClass::execute(): arrived" << endl;
	return insert((static_cast<Xspress3 *>(device))->get_rois());
}

//--------------------------------------------------------
/**
 * method : 		StreamResetIndexClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *StreamResetIndexClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "StreamResetIndexClass::execute(): arrived" << endl;
	((static_cast<Xspress3 *>(device))->stream_reset_index());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		GetDataStreamsClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *GetDataStreamsClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "GetDataStreamsClass::execute(): arrived" << endl;
	return insert((static_cast<Xspress3 *>(device))->get_data_streams());
}

//--------------------------------------------------------
/**
 * method : 		SetRoisFromFileClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *SetRoisFromFileClass::execute(Tango::DeviceImpl *device, const CORBA::Any &in_any)
{
	cout2 << "SetRoisFromFileClass::execute(): arrived" << endl;
	Tango::DevString argin;
	extract(in_any, argin);
	((static_cast<Xspress3 *>(device))->set_rois_from_file(argin));
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		GetRoisFilesAliasClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *GetRoisFilesAliasClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "GetRoisFilesAliasClass::execute(): arrived" << endl;
	return insert((static_cast<Xspress3 *>(device))->get_rois_files_alias());
}


//===================================================================
//	Properties management
//===================================================================
//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::get_class_property()
 *	Description : Get the class property for specified name.
 */
//--------------------------------------------------------
Tango::DbDatum Xspress3Class::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, returns  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::get_default_device_property()
 *	Description : Return the default value for device property.
 */
//--------------------------------------------------------
Tango::DbDatum Xspress3Class::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::get_default_class_property()
 *	Description : Return the default value for class property.
 */
//--------------------------------------------------------
Tango::DbDatum Xspress3Class::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}


//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::set_default_property()
 *	Description : Set default property (class and device) for wizard.
 *                For each property, add to wizard property name and description.
 *                If default value has been set, add it to wizard property and
 *                store it in a DbDatum.
 */
//--------------------------------------------------------
void Xspress3Class::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;
	vector<string>	vect_data;

	//	Set Default Class Properties

	//	Set Default device Properties
	prop_name = "BaseIPAddress";
	prop_desc = "Xspress3 IP address";
	prop_def  = "192.168.0.1";
	vect_data.clear();
	vect_data.push_back("192.168.0.1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "BaseMACAddress";
	prop_desc = "Xspress3 mac adress";
	prop_def  = "02.00.00.00.00.00";
	vect_data.clear();
	vect_data.push_back("02.00.00.00.00.00");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "BasePort";
	prop_desc = "Port number of the Xspress3 master";
	prop_def  = "30123";
	vect_data.clear();
	vect_data.push_back("30123");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "BoardType";
	prop_desc = "Type of the card, can be XSPRESS3 or SIMULATOR";
	prop_def  = "XSPRESS3";
	vect_data.clear();
	vect_data.push_back("XSPRESS3");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "Debug";
	prop_desc = "Xspress3 verbose mode";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "NbCards";
	prop_desc = "Number of Xspress3 cards";
	prop_def  = "2";
	vect_data.clear();
	vect_data.push_back("2");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "NbChannels";
	prop_desc = "Number of all acquisition channels for all available cards";
	prop_def  = "4";
	vect_data.clear();
	vect_data.push_back("4");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "ConfigurationFilePath";
	prop_desc = "The directory containing calibration files";
	prop_def  = "/etc/xspress3/calibration/initial/settings";
	vect_data.clear();
	vect_data.push_back("/etc/xspress3/calibration/initial/settings");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "ExpertStreamItems";
	prop_desc = "Define the list of Items managed by the Streamer. (Nexus)<BR>\nEach item defined here wil be saved in the stream for all available channels. <BR>\nAvailables values are (not sensitive case):<BR>\nCHANNEL<BR>\nICR<BR>\nOCR<BR>\nREALTIME<BR>\nDEADTIME<BR>\nEVENTSINRUN<BR>";
	prop_def  = "CHANNEL\nICR\nOCR";
	vect_data.clear();
	vect_data.push_back("CHANNEL");
	vect_data.push_back("ICR");
	vect_data.push_back("OCR");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "ExpertStreamMemoryMode";
	prop_desc = "Available only for Nexus format, used to set memory mode.<br>\nAvailable values :<br>\n- COPY<br>\n- NO_COPY";
	prop_def  = "COPY";
	vect_data.clear();
	vect_data.push_back("COPY");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "ExpertStreamWriteMode";
	prop_desc = "Available only for Nexus format. used to set write mode.<br>\nAvailable Values :<br>\n- SYNCHRONOUS<br>\n- ASYNCHRONOUS";
	prop_def  = "SYNCHRONOUS";
	vect_data.clear();
	vect_data.push_back("SYNCHRONOUS");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "Rois";
	prop_desc = "Memorize the latest Rois configuration, for channels.\nnum_channel;start_roi0;end_roi0;start_roi1;end_roi1;...";
	prop_def  = "-1;0;4095";
	vect_data.clear();
	vect_data.push_back("-1;0;4095");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "SpoolID";
	prop_desc = "Used  only by the Flyscan application";
	prop_def  = "TO_BE_DEFINED";
	vect_data.clear();
	vect_data.push_back("TO_BE_DEFINED");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "RoisFiles";
	prop_desc = "Define the list of rois files ``*.txt``  and their associated alias.";
	prop_def  = "ALIAS;FILE_PATH_NAME";
	vect_data.clear();
	vect_data.push_back("ALIAS;FILE_PATH_NAME");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "StreamItemsPrefix";
	prop_desc = "Prefix preceding each static attributes in Nexus files to differ from those coming from other Xspress3`s instances";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MemorizedStreamTargetFile";
	prop_desc = "Only the device could modify this property <br>\nMemorized file name for generated Stream files";
	prop_def  = "TO_BE_DEFINED";
	vect_data.clear();
	vect_data.push_back("TO_BE_DEFINED");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MemorizedStreamTargetPath";
	prop_desc = "Only the device could modify this property <br>\nMemorized root path for generated Stream files";
	prop_def  = "TO_BE_DEFINED";
	vect_data.clear();
	vect_data.push_back("TO_BE_DEFINED");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MemorizedStreamType";
	prop_desc = "Only the device could modify this property <br>\nMemorized stream type<br>\nAvailable Values :<br>\n- NO_STREAM<br>\n- NEXUS_STREAM<br>\n- CSV_STREAM";
	prop_def  = "NO_STREAM";
	vect_data.clear();
	vect_data.push_back("NO_STREAM");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MemorizedTriggerMode";
	prop_desc = "Only the device could modify this property<br>\nMemorized trigger mode<br>\nAvailable Values :<br>\n- INTERNAL<br>\n- GATE";
	prop_def  = "INTERNAL";
	vect_data.clear();
	vect_data.push_back("INTERNAL");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MemorizedExposureTime";
	prop_desc = "Only the device could modify this property <br>\nMemorized exposure time value in seconds";
	prop_def  = "1.0";
	vect_data.clear();
	vect_data.push_back("1.0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MemorizedNbFrames";
	prop_desc = "Only the device could modify this property<br>\nMemorized frames number";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MemorizedStreamNbAcqPerFile";
	prop_desc = "Only the device could modify this property<br>\nMemorized number of acquisitions for each Stream file";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "ExpertAcquisitionPollingPeriod";
	prop_desc = "Duration of the driver polling in order to read the current acquired frame.\n(specified in milliseconds)";
	prop_def  = "50";
	vect_data.clear();
	vect_data.push_back("50");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
}

//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::write_class_property()
 *	Description : Set class description fields as property in database
 */
//--------------------------------------------------------
void Xspress3Class::write_class_property()
{
	//	First time, check if database used
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Xspress3 TANGO device.");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("Device for Control & Acquisition of an Xspress3 module.");
	description << str_desc;
	data.push_back(description);

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("TANGO_BASE_CLASS");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	get_db_class()->put_property(data);
}

//===================================================================
//	Factory methods
//===================================================================

//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::device_factory()
 *	Description : Create the device object(s)
 *                and store them in the device list
 */
//--------------------------------------------------------
void Xspress3Class::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{
	/*----- PROTECTED REGION ID(Xspress3Class::device_factory_before) ENABLED START -----*/
	
	//	Add your own code
	//	Create devices and add it into the device list
	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
		device_list.push_back(new Xspress3(this, (*devlist_ptr)[i]));							 
	}

	//	Manage dynamic attributes if any
	//erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

	//	Export devices to the outside world
	for (unsigned long i=1 ; i<=devlist_ptr->length() ; i++)
	{
		//	Add dynamic attributes if any
		Xspress3 *dev = static_cast<Xspress3 *>(device_list[device_list.size()-i]);
		dev->add_dynamic_attributes();

		//	Check before if database used.
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(dev);
		else
			export_device(dev, dev->get_name().c_str());
	}
	return;
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::device_factory_before

	//	Create devices and add it into the device list
	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
		device_list.push_back(new Xspress3(this, (*devlist_ptr)[i]));
	}

	//	Manage dynamic attributes if any
	erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

	//	Export devices to the outside world
	for (unsigned long i=1 ; i<=devlist_ptr->length() ; i++)
	{
		//	Add dynamic attributes if any
		Xspress3 *dev = static_cast<Xspress3 *>(device_list[device_list.size()-i]);
		dev->add_dynamic_attributes();

		//	Check before if database used.
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(dev);
		else
			export_device(dev, dev->get_name().c_str());
	}

	/*----- PROTECTED REGION ID(Xspress3Class::device_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::device_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::attribute_factory()
 *	Description : Create the attribute object(s)
 *                and store them in the attribute list
 */
//--------------------------------------------------------
void Xspress3Class::attribute_factory(vector<Tango::Attr *> &att_list)
{
	/*----- PROTECTED REGION ID(Xspress3Class::attribute_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::attribute_factory_before

	//	Create a list of static attributes
	create_static_attribute_list(get_class_attr()->get_attr_list());
	/*----- PROTECTED REGION ID(Xspress3Class::attribute_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::attribute_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::pipe_factory()
 *	Description : Create the pipe object(s)
 *                and store them in the pipe list
 */
//--------------------------------------------------------
void Xspress3Class::pipe_factory()
{
	/*----- PROTECTED REGION ID(Xspress3Class::pipe_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::pipe_factory_before
	/*----- PROTECTED REGION ID(Xspress3Class::pipe_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::pipe_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::command_factory()
 *	Description : Create the command object(s)
 *                and store them in the command list
 */
//--------------------------------------------------------
void Xspress3Class::command_factory()
{
	/*----- PROTECTED REGION ID(Xspress3Class::command_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::command_factory_before


	//	Command Snap
	SnapClass	*pSnapCmd =
		new SnapClass("Snap",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pSnapCmd);

	//	Command Stop
	StopClass	*pStopCmd =
		new StopClass("Stop",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pStopCmd);

	//	Command SetRoisFromList
	SetRoisFromListClass	*pSetRoisFromListCmd =
		new SetRoisFromListClass("SetRoisFromList",
			Tango::DEVVAR_STRINGARRAY, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pSetRoisFromListCmd);

	//	Command RemoveRois
	RemoveRoisClass	*pRemoveRoisCmd =
		new RemoveRoisClass("RemoveRois",
			Tango::DEV_LONG, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pRemoveRoisCmd);

	//	Command GetRois
	GetRoisClass	*pGetRoisCmd =
		new GetRoisClass("GetRois",
			Tango::DEV_VOID, Tango::DEVVAR_STRINGARRAY,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pGetRoisCmd);

	//	Command StreamResetIndex
	StreamResetIndexClass	*pStreamResetIndexCmd =
		new StreamResetIndexClass("StreamResetIndex",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::EXPERT);
	command_list.push_back(pStreamResetIndexCmd);

	//	Command GetDataStreams
	GetDataStreamsClass	*pGetDataStreamsCmd =
		new GetDataStreamsClass("GetDataStreams",
			Tango::DEV_VOID, Tango::DEVVAR_STRINGARRAY,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pGetDataStreamsCmd);

	//	Command SetRoisFromFile
	SetRoisFromFileClass	*pSetRoisFromFileCmd =
		new SetRoisFromFileClass("SetRoisFromFile",
			Tango::DEV_STRING, Tango::DEV_VOID,
			"Alias of a roi file",
			"",
			Tango::OPERATOR);
	command_list.push_back(pSetRoisFromFileCmd);

	//	Command GetRoisFilesAlias
	GetRoisFilesAliasClass	*pGetRoisFilesAliasCmd =
		new GetRoisFilesAliasClass("GetRoisFilesAlias",
			Tango::DEV_VOID, Tango::DEVVAR_STRINGARRAY,
			"",
			"Array of string of rois Files alias",
			Tango::OPERATOR);
	command_list.push_back(pGetRoisFilesAliasCmd);

	/*----- PROTECTED REGION ID(Xspress3Class::command_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::command_factory_after
}

//===================================================================
//	Dynamic attributes related methods
//===================================================================

//--------------------------------------------------------
/**
 * method : 		Xspress3Class::create_static_attribute_list
 * description : 	Create the a list of static attributes
 *
 * @param	att_list	the ceated attribute list
 */
//--------------------------------------------------------
void Xspress3Class::create_static_attribute_list(vector<Tango::Attr *> &att_list)
{
	for (unsigned long i=0 ; i<att_list.size() ; i++)
	{
		string att_name(att_list[i]->get_name());
		transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
		defaultAttList.push_back(att_name);
	}

	cout2 << defaultAttList.size() << " attributes in default list" << endl;

	/*----- PROTECTED REGION ID(Xspress3Class::create_static_att_list) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::create_static_att_list
}


//--------------------------------------------------------
/**
 * method : 		Xspress3Class::erase_dynamic_attributes
 * description : 	delete the dynamic attributes if any.
 *
 * @param	devlist_ptr	the device list pointer
 * @param	list of all attributes
 */
//--------------------------------------------------------
void Xspress3Class::erase_dynamic_attributes(const Tango::DevVarStringArray *devlist_ptr, vector<Tango::Attr *> &att_list)
{
	Tango::Util *tg = Tango::Util::instance();

	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		Tango::DeviceImpl *dev_impl = tg->get_device_by_name(((string)(*devlist_ptr)[i]).c_str());
		Xspress3 *dev = static_cast<Xspress3 *> (dev_impl);

		vector<Tango::Attribute *> &dev_att_list = dev->get_device_attr()->get_attribute_list();
		vector<Tango::Attribute *>::iterator ite_att;
		for (ite_att=dev_att_list.begin() ; ite_att != dev_att_list.end() ; ++ite_att)
		{
			string att_name((*ite_att)->get_name_lower());
			if ((att_name == "state") || (att_name == "status"))
				continue;
			vector<string>::iterator ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
			if (ite_str == defaultAttList.end())
			{
				cout2 << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i] << endl;
				Tango::Attribute &att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
				dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
				--ite_att;
			}
		}
	}
	/*----- PROTECTED REGION ID(Xspress3Class::erase_dynamic_attributes) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Xspress3Class::erase_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : Xspress3Class::get_attr_object_by_name()
 *	Description : returns Tango::Attr * object found by name
 */
//--------------------------------------------------------
Tango::Attr *Xspress3Class::get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname)
{
	vector<Tango::Attr *>::iterator it;
	for (it=att_list.begin() ; it<att_list.end() ; ++it)
		if ((*it)->get_name()==attname)
			return (*it);
	//	Attr does not exist
	return NULL;
}


/*----- PROTECTED REGION ID(Xspress3Class::Additional Methods) ENABLED START -----*/

/*----- PROTECTED REGION END -----*/	//	Xspress3Class::Additional Methods
} //	namespace
