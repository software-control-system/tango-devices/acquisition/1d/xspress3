/*************************************************************************/
/*! 
 *  \file   Simulator.cpp
 *  \brief  Implementation of Simulator methods.
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include "Simulator.h"
#include <yat/threading/Thread.h>

namespace Xspress3_ns
{
// Constructor
Simulator::Simulator(Tango::DeviceImpl* dev, std::vector<std::string> vec_properties): 
    Driver(dev),
    m_vec_properties(vec_properties),
    m_nb_cards(2),
    m_nb_channels(4),
    m_nb_bins(4096),
    m_nb_scalars(11),
    m_max_frames(16384),
    m_exp_time(2.0),
    m_nb_frames(1),
    m_nb_acq_frames(0),
    m_is_abort(false),
    m_is_running(false),
    m_trigger_mode(TriggerMode::INTERNAL)
{
    YAT_INFO << "Simulator::Simulator()" << std::endl;
    if(m_vec_properties.size() == 10)
    {
        std::vector<std::string> board_type_simulator_params;
        yat::StringUtil::split(m_vec_properties.at(0), ';', &board_type_simulator_params, true);

        m_status = "Board Type : " + board_type_simulator_params.at(0);
        YAT_VERBOSE << "Simulator::Simulator() : " << m_status << std::endl;
        
        if(board_type_simulator_params.size() == 3)
        {
            std::istringstream (board_type_simulator_params.at(1)) >> m_nb_cards;
            std::istringstream (board_type_simulator_params.at(2)) >> m_nb_channels;
        }
        std::istringstream (m_vec_properties.at(3)) >> m_max_frames;
    }
    else
    {
       YAT_ERROR << "Simulator::Simulator() Uncorrect number of properties " <<endl;
        //set_state(Tango::FAULT);
       Tango::Except::throw_exception ("CONFIG_ERROR",
                                        "Incorrect number of properties",
                                        "Simulator::Simulator()");
    }

    m_nb_bins = m_max_frames/m_nb_channels;
    YAT_VERBOSE << "Simulator::Simulator() - Number of bins per channel: " << m_nb_bins << std::endl;
    
    if (m_statistics_attribute_map.empty() )
    {
        m_statistics_attribute_map["realTime"] = STAT_REALTIME;
        m_statistics_attribute_map["deadTime"] = STAT_DEADTIME;
        m_statistics_attribute_map["inputCountRate"] = STAT_INPUT_COUNT_RATE;
        m_statistics_attribute_map["outputCountRate"] = STAT_OUTPUT_COUNT_RATE;
        m_statistics_attribute_map["eventsInRun"] = STAT_EVENTS_IN_RUN;
        m_statistics_attribute_map["deadTimeCorrectionFactor"] = STAT_DEAD_TIME_CORRECTION_FACTOR;
    }

    m_pixel_datas.reserve(m_nb_channels);
    m_pixel_datas.resize(m_nb_channels);
    
    set_state(Tango::STANDBY);
}
                            
// Destructor
Simulator::~Simulator()
{
    YAT_INFO << "Simulator::~Simulator()" << endl;
    m_vec_properties.clear();
    m_statistics_attribute_map.clear();
    m_pixel_datas.clear();
}

// Numbre of simulated xspress3 cards
int Simulator::get_nb_cards() const
{
    return m_nb_cards;
}

// Numbre of simulated xspress3 channels
int Simulator::get_nb_channels() const
{
    return m_nb_channels;
}

// Firmware revision
std::string Simulator::get_firmware_revision() const
{
    return SIMULATOR_FIRMWARE_REVISION_STR;
}

std::string Simulator::get_board_type() const
{
    return SIMULATOR_BOARD_TYPE_STR;
}

// Current frame
int Simulator::get_current_frame() const
{
    return m_nb_acq_frames;
}

// Number of wanted frames
int Simulator::get_nb_frames() const
{
    return m_nb_frames;
}

// Number of wanted frames
void Simulator::set_nb_frames(int frames)
{
    m_nb_frames = frames;
}

// Number of Bins
int Simulator::get_nb_bins() const
{
    return m_nb_bins;
}

// Number of Bins
void Simulator::set_nb_bins(int bins)
{
    m_nb_bins = bins;
}

int Simulator::get_nb_scalars() const
{
    return m_nb_scalars;
}

void Simulator::set_nb_scalars(int scalars)
{
    m_nb_scalars = scalars;
}

TriggerMode Simulator::get_trigger_mode() const
{
    return m_trigger_mode;
}

void Simulator::set_trigger_mode(TriggerMode trigger_mode)
{
    m_trigger_mode = trigger_mode;
}

void Simulator::set_state(Tango::DevState state)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

Tango::DevState Simulator::get_state()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return m_state;
}

std::string Simulator::get_status()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return m_status;
}

ulong Simulator::get_nb_elements() const
{
    return (ulong)(m_nb_bins + m_nb_scalars);
}

double Simulator::get_exposure_time() const
{
    return m_exp_time;
}

void Simulator::set_exposure_time(double exposure_time)
{
    m_exp_time = exposure_time;
}

void Simulator::start_acquisition()
{
    YAT_INFO << "Simulator::start_acquisition()"<< std::endl;
    yat::AutoMutex<yat::Mutex> amutex(m_locker);
    if(TriggerMode::INTERNAL ==  m_trigger_mode)
    {
        struct timespec start, end;
        clock_gettime(CLOCK_REALTIME, &start);

        set_state(Tango::RUNNING);
        m_is_running = true;

        m_nb_acq_frames = 0;
        std::vector<uint32_t*> result;

        m_pixel_datas.clear();
        m_pixel_datas.resize(m_nb_channels);
		yat::Timer t_global0;	
        while (m_nb_acq_frames < m_nb_frames && is_running() && !is_abort() )
        {
            result.clear();
            result.resize(m_nb_bins);
            for (int i = 0; i < m_nb_channels; ++i)
            {
                result[i] = new uint32_t[m_nb_bins];
                m_pixel_datas[i] = result;
            }
            
			//wait an exposure time ...
			yat::Timer t0;				
			Tango::DevULong sec = (unsigned long)(m_exp_time);
			Tango::DevULong msec = ((m_exp_time - sec)*1000);					
			yat::ThreadingUtilities::sleep(sec, msec*1000000); // sec,nanosec;
			YAT_VERBOSE << "--------------------------------------------" << std::endl;
			YAT_VERBOSE << "Simulated Exposure Time elapsed: " << t_global0.elapsed_msec() <<" msec" << endl;
			YAT_VERBOSE << "--------------------------------------------" << std::endl;
            m_nb_acq_frames++;
        }

        clock_gettime(CLOCK_REALTIME, &end);
    
        double elapsed;
        elapsed = (end.tv_sec - start.tv_sec) * 1e9;
        elapsed = (elapsed + (end.tv_nsec - start.tv_nsec)) * 1e-9;

        YAT_INFO << "--------------------------------------------" << std::endl;
        YAT_INFO << "--------------------------------------------" << std::endl;
        YAT_INFO << " Number of acquired frames = " << m_nb_acq_frames << std::endl;
        YAT_INFO << " ExposureTime value per frame = " << m_exp_time <<" seconds" << std::endl;
        YAT_INFO << " The acquisition lasted :" << elapsed << setprecision(5) <<" seconds " << std::endl;;
        YAT_INFO << "--------------------------------------------" << std::endl;
        YAT_INFO << "--------------------------------------------" << std::endl;

        set_state(Tango::STANDBY);
        m_is_running = false;
    }
    else if(TriggerMode::GATE ==  m_trigger_mode)
    {
        YAT_INFO << "In SIMULATOR mode , acquisition is reserved for INTERNAL TRIGGER only"<< std::endl;                             
    }
    return;
}

// stop_acquisition()
void Simulator::stop_acquisition()
{
    YAT_INFO << "Simulator::stop_acquisition()"<< std::endl;
    m_is_running = false;
    set_state(Tango::STANDBY);
    return;
}

std::vector<Tango::DevULong> Simulator::get_spectrum_data(int current_frame, int channel)
{
    std::vector<Tango::DevULong> result;
    result.resize(m_nb_bins);

    if (channel == 0)
    {
        for(size_t i = 0; i < result.size(); i++)
            result.at(i) = (Tango::DevULong)(SIMULATED_VALUE_CHANNEL_0 + i );
    }
    else if (channel == 1)
    {
        for(size_t i = 0; i < result.size(); i++)
            result.at(i) = (Tango::DevULong)(SIMULATED_VALUE_CHANNEL_1 + i );
    }
    else if (channel == 2)
    {
        for(size_t i = 0; i < result.size(); i++)
            result.at(i) = (Tango::DevULong)(SIMULATED_VALUE_CHANNEL_2 + i );
    }
    else if (channel == 3)
    {
        for(size_t i = 0; i < result.size(); i++)
            result.at(i) = (Tango::DevULong)(SIMULATED_VALUE_CHANNEL_3 + i );
    }
    else
    {
        for(size_t i = 0; i < result.size(); i++)
            result.at(i) = (Tango::DevULong)(SIMULATED_VALUE_CHANNEL_OTHERS + i );
    }
    
    return result;
}


bool Simulator::is_abort() const
{
    return m_is_abort;
}

void Simulator::abort(bool abort)
{
    YAT_INFO << "Simulator::abort()" << std::endl;
    yat::Mutex aLock;
    aLock.lock();
    m_is_abort = abort;
    aLock.unlock();
}

void Simulator::get_statistics_data(int frame, int channel, const char* name, void* value)
{
    switch(m_statistics_attribute_map[name])
    {
        case STAT_REALTIME:
           *(static_cast<double*> (value)) = m_exp_time * 80000000.0;
            break;
        
        case STAT_DEADTIME:
            *(static_cast<double*> (value)) = NUMERIC_SCALAR_9 + frame + channel;
            break;
        
        case STAT_INPUT_COUNT_RATE:
           *(static_cast<double*> (value)) = NUMERIC_SCALAR_4 + frame + channel;
            break;
        
        case STAT_OUTPUT_COUNT_RATE:
            *(static_cast<double*> (value)) = NUMERIC_SCALAR_4 + frame + channel;
            break;

        case STAT_EVENTS_IN_RUN:
           *(static_cast<unsigned long*> (value)) = NUMERIC_SCALAR_3 + frame + channel;
            break;

        case STAT_DEAD_TIME_CORRECTION_FACTOR:
            *(static_cast<double*> (value)) = NUMERIC_SCALAR_10 + frame + channel;
            break;

        default:
            YAT_ERROR << "Simulator::get_statistics_data() - Unknown acquisition attribute name" << std::endl;
            break;
    }
}

bool Simulator::is_running() const
{
    return m_is_running;
}

}
