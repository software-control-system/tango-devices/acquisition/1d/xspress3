/*******************************************************************************/
/*! 
 *  \file   StreamCsv.cpp
 *  \brief  Class used to manage the stream of acquisition data in csv files
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*******************************************************************************/

#include "StreamCsv.h"
#include <yat/utils/Logging.h>

namespace Xspress3_ns
{
    //----------------------------------------------------------------------------------------------------------------------
    StreamCsv::StreamCsv(Tango::DeviceImpl *dev)
        : Stream(dev),
          m_target_path("TO_BE_DEFINED"),
          m_file_name("TO_BE_DEFINED")
    {
        YAT_INFO << "StreamCsv::StreamCsv() " << endl;
    }

    //----------------------------------------------------------------------------------------------------------------------
    StreamCsv::~StreamCsv()
    {
        YAT_INFO << "StreamCsv::~StreamCsv() " << endl;
    }

    //----------------------------------------------------------------------------------------------------------------------
    void StreamCsv::init(yat::SharedPtr<DataStore> data_store)
    {
        YAT_INFO << "StreamCsv::init()" << endl;
        yat::MutexLock scoped_lock(m_data_lock);        
        YAT_INFO << "\t- target path = " 	<< m_target_path << std::endl;
        YAT_INFO << "\t- file name = "		<< m_file_name << std::endl;
    }

    //----------------------------------------------------------------------------------------------------------------------
    void StreamCsv::open()
    {
        yat::MutexLock scoped_lock(m_data_lock);
    }

    //----------------------------------------------------------------------------------------------------------------------
    void StreamCsv::close()
    {
        yat::MutexLock scoped_lock(m_data_lock);
    }

    //----------------------------------------------------------------------------------------------------------------------
    void StreamCsv::abort()
    {
        yat::MutexLock scoped_lock(m_data_lock);
    }

    
    //----------------------------------------------------------------------------------------------------------------------
    void StreamCsv::set_target_path(const std::string &target_path)
    {
        YAT_INFO << "StreamCsv::set_target_path(" << target_path << ")" << std::endl;
        m_target_path = target_path;
    }

    //----------------------------------------------------------------------------------------------------------------------
    void StreamCsv::set_file_name(const std::string &file_name)
    {
        YAT_INFO << "StreamCsv::set_file_name(" << file_name << ")" << std::endl;
        m_file_name = file_name;
    }

    //----------------------------------------------------------------------------------------------------------------------
    void StreamCsv::update_data(int current_frame, int ichannel, yat::SharedPtr<DataStore> data_store)
    {
        yat::MutexLock scoped_lock(m_data_lock);
        if(current_frame < data_store-> get_current_frame())
        {
            std::string filename = yat::Format("{}/{}_frame_{}.csv").arg(m_target_path).arg(m_file_name).arg(current_frame + 1);
            std::ofstream output_file(filename, std::ios::out | std::ofstream::binary);

            if (output_file.fail()) 
            {
                std::string text = std::string("Couldn't open the file: ") + filename + std::string("!");
                YAT_ERROR << text << endl;
            }

            const int nb_channels = data_store->get_nb_channels();

            for (int channel_index = 0 ; channel_index < nb_channels ; channel_index++)
            {
                output_file << "Channel_" <<channel_index + 1 << std::endl;
                //Statistics
                output_file << "realTime: " << data_store->get_realtime(current_frame,channel_index) << std::endl;
                output_file << "deadTime: " << data_store->get_deadtime(current_frame,channel_index) << std::endl;
                output_file << "inputCountRate: " << data_store->get_input_count_rate(current_frame,channel_index) << std::endl;
                output_file << "outputCountRate: " << data_store->get_output_count_rate(current_frame,channel_index) << std::endl;
                output_file << "eventsInRun: " << data_store->get_events_in_run(current_frame,channel_index) << std::endl;
                
                // Spectrum
                const std::vector<uint32_t> spectrum = data_store->get_frame_spectrum_data(current_frame,channel_index);
                std::ostream_iterator<uint32_t> output_iterator(output_file, ";");
                std::copy(spectrum.begin(), spectrum.end(), output_iterator);
                output_file << std::endl;

                output_file << std::endl;
            }
            output_file.flush();
        }
    }

    void StreamCsv::reset_index()
    {
    }

}