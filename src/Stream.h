/*************************************************************************
/*! 
 * \file Stream.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class Stream
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef STREAM_H
#define STREAM_H

#include <tango.h>
#include <yat/memory/SharedPtr.h>
#include "DataStore.h"
#include <vector>

namespace Xspress3_ns
{
//--------------------------------------------------------------------------------
//! \class Stream
//! \brief Base class to manage data storage
//--------------------------------------------------------------------------------
class Stream : public Tango::LogAdapter
{
public:
    //! \brief Constructor
    Stream(Tango::DeviceImpl *dev);
    //! \brief Destructor
    ~Stream();

    virtual void init(yat::SharedPtr<DataStore> data_store) = 0;
    virtual void open() = 0;
    virtual void close() = 0;
    virtual void abort() = 0;
    virtual void update_data(int current_frame, int ichannel, yat::SharedPtr<DataStore> data_store) = 0;
    virtual void reset_index() = 0;

    const std::vector<std::string>& get_stream_items();
	void set_stream_items(const std::vector<std::string>& stream_items);    

protected:
    std::vector<std::string> m_stream_items;    
    //specturm
    bool m_is_channel_enabled;
    //statistics
    bool m_is_realtime_enabled;
    bool m_is_deadtime_enabled;
    bool m_is_icr_enabled;
    bool m_is_ocr_enabled;
    bool m_is_events_in_run_enabled;
};

}

#endif // STREAM_H
