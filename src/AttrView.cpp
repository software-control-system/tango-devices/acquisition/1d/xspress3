/*************************************************************************/
/*! 
 *  \file   AttrView.cpp
 *  \brief  Implementation of AttrView methods.
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include "AttrView.h"
#include <Xspress3.h>

#include <yat/utils/Logging.h>
#include <yat4tango/PropertyHelper.h>

namespace Xspress3_ns
{
    AttrView::AttrView(Tango::DeviceImpl *dev)
    : Tango::LogAdapter(dev),
    m_device(dev),
    m_dim(dev),
    m_nb_channels(0),
    m_nb_bins(0)
    {
        YAT_INFO << "AttrView::AttrView()" << endl;
        m_rois_data.clear();
    }

    AttrView::~AttrView()
    {
        YAT_INFO << "AttrView::~AttrView()" << endl;
        
        delete m_dyn_firmware_revision;
        delete m_dyn_board_type;
        delete m_dyn_nb_frames;
        delete m_dyn_current_frame;
        delete m_dyn_nb_modules;
        delete m_dyn_nb_channels;
        delete m_dyn_nb_bins;
        delete m_dyn_trigger_mode;
        delete m_dyn_exposure_time;
        delete m_dyn_current_mode;

        for(unsigned i = 0;i<m_dyn_realtime.size();i++)
        {
            delete m_dyn_realtime[i];
        }
        m_dyn_realtime.clear();

        for(unsigned i = 0;i<m_dyn_deadtime.size();i++)
        {
            delete m_dyn_deadtime[i];
        }
        m_dyn_deadtime.clear();

        for(unsigned i = 0;i<m_dyn_icr.size();i++)
        {
            delete m_dyn_icr[i];
        }
        m_dyn_icr.clear();

        for(unsigned i = 0;i<m_dyn_ocr.size();i++)
        {
            delete m_dyn_ocr[i];
        }
        m_dyn_ocr.clear();

        for(unsigned i = 0;i<m_dyn_events_in_run.size();i++)
        {
            delete m_dyn_events_in_run[i];
        }
        m_dyn_events_in_run.clear();	

        for(unsigned i = 0;i<m_dyn_channel.size();i++)
        {
            delete m_dyn_channel[i];
        }
        m_dyn_channel.clear();

        delete m_dyn_file_generation;
        delete m_dyn_stream_type;
        delete m_dyn_stream_target_path;
        delete m_dyn_stream_target_file;
        delete m_dyn_stream_nb_acq_per_file;
        
        for(unsigned i = 0;i < m_dyn_rois_data.size();++i)
        {
            for(unsigned j = 0;j < m_dyn_rois_data[i].size();++j)
            {
                delete m_dyn_rois_data[i][j];
            }
            m_dyn_rois_data[i].clear();
        }
        m_dyn_rois_data.clear();
        m_rois_data.clear();
    }

    void AttrView::init(yat::SharedPtr<DataStore> data_store)
    {
        YAT_INFO << "AttrView::init()" << endl;

        m_nb_channels = data_store->get_nb_channels();
        m_nb_bins = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_nb_bins();
    	
        m_rois_data = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_rois_data();
        // Display Rois list data 
        YAT_VERBOSE << "-------------------------------------------------------" << std::endl;
        YAT_VERBOSE << "AttrView::init() - Number of channels to manage for rois: " << m_rois_data.size() << std::endl;
        for(int i = 0; i < m_rois_data.size() ; ++i )
        {
            YAT_VERBOSE << "Number of Rois for channel "<< i << " : " << m_rois_data[i].size() << std::endl;
            for(int j = 0; j < m_rois_data[i].size(); ++j )
            {
                YAT_VERBOSE << "\t- [ch: " << i <<"][roi: " << j << "] begin =" << m_rois_data[i][j].begin_roi << std::endl;
                YAT_VERBOSE << "\t- [ch: " << i <<"][roi: " << j << "] end = " << m_rois_data[i][j].end_roi << std::endl;
                YAT_VERBOSE << "\t- [ch: " << i <<"][roi: " << j << "] value = " << m_rois_data[i][j].roi_sum << std::endl;
                YAT_VERBOSE << " " << std::endl;
            }
        }
        YAT_VERBOSE << "-------------------------------------------------------" << std::endl;
              
        create_dyn_attributes();  
        create_dyn_stream_attributes();
        create_dyn_roi_attributes();
        init_common_attributes();
    }    
    
    void AttrView::init_acquisition_parametres()
    {        
        m_dyn_current_frame->set_value(0);
    }

    // Updates the data of the attributes
    void AttrView::update_data(int frame, int channel, yat::SharedPtr<DataStore> data_store)
    {   
        //Read_Write Common attributes 
        m_dyn_current_frame->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_current_frame());
        
        //Update statistics attributes
        m_dyn_realtime[channel]->set_value(data_store->get_realtime(frame, channel));
        m_dyn_deadtime[channel]->set_value(data_store->get_deadtime(frame, channel));
        m_dyn_icr[channel]->set_value(data_store->get_input_count_rate(frame, channel));
        m_dyn_ocr[channel]->set_value(data_store->get_output_count_rate(frame, channel));
        m_dyn_events_in_run[channel]->set_value(data_store->get_events_in_run(frame, channel));
        
        //Update channels spectrum attributes
        m_dyn_channel[channel]->set_value(data_store->get_frame_spectrum_data(frame, channel));
        
        //Update roi attributes
        m_rois_data.clear();
        m_rois_data = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_rois_data();
        for(unsigned i = 0;i < m_dyn_rois_data.size();++i)
        {
            for(unsigned j = 0;j < m_dyn_rois_data[i].size();++j)
            {
                m_dyn_rois_data[i][j]->set_value((Tango::DevULong)m_rois_data[i][j].roi_sum);
            }
        }
    }

    // Updates dynamic attributes
    void AttrView::init_common_attributes()
    {
        YAT_INFO << "AttrView::init_common_attributes()." << endl;
        
        m_dyn_firmware_revision->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_firmware_revision());
        m_dyn_board_type->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_board_type());
        m_dyn_nb_modules->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_nb_cards());
        m_dyn_nb_channels->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_nb_channels());
        m_dyn_nb_frames->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_nb_frames());
        m_dyn_nb_bins->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_nb_bins());
        m_dyn_trigger_mode->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_trigger_mode());
        m_dyn_current_mode->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_current_mode());

        // statistics dynamic attributes
        for(int channel = 0 ; channel < m_nb_channels ; channel ++)
        {
            m_dyn_realtime[channel]->set_value(0.0);
            m_dyn_deadtime[channel]->set_value(0.0);
            m_dyn_icr[channel]->set_value(0.0);
            m_dyn_ocr[channel]->set_value(0.0);
            m_dyn_events_in_run[channel]->set_value(0);
        }    

        // stream dynamic attributes
        m_dyn_stream_type->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_type());
        m_dyn_file_generation->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_file_generation());
        m_dyn_stream_target_path->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_target_path());
        m_dyn_stream_target_file->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_target_file());
        m_dyn_stream_nb_acq_per_file->set_value(dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_nb_acq_per_file());    
    }

    // Creates all dynamic attributes
    void AttrView::create_dyn_attributes()
    {
        YAT_INFO << "AttrView::create_dyn_attributes()" << std::endl;   
        //- Remove all previous dyn attributes
        m_dim.dynamic_attributes_manager().remove_attributes();

        try
        {
            //Create firmwareRevision attribute
            {
                std::string name = "firmwareRevision";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_firmware_revision = new StringUserData(name);
                dai.set_user_data(m_dyn_firmware_revision);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_STRING;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%s";
                dai.tai.description = "The firmware revision of the xspress3 system";
                
                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create boardType attribute
            {
                std::string name = "boardType";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_board_type = new StringUserData(name);
                dai.set_user_data(m_dyn_board_type);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_STRING;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%s";
                dai.tai.description = "The type of the connected card. Available values :\n- XSPRESS3\n- SIMULATOR\n";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create nbModules attribute
            {
                std::string name = "nbModules";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data
                m_dyn_nb_modules = new ULongUserData(name);
                dai.set_user_data(m_dyn_nb_modules);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_ULONG;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%5d";
                dai.tai.description = "The number of xspress3 cards";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create nbChannels attribute
            {
                std::string name = "nbChannels";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data
                m_dyn_nb_channels = new ULongUserData(name);
                dai.set_user_data(m_dyn_nb_channels);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_ULONG;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%5d";
                dai.tai.description = "The current frame for acquisition operation";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create nbBins attribute
            {
                std::string name = "nbBins";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data
                m_dyn_nb_bins = new ULongUserData(name);
                dai.set_user_data(m_dyn_nb_bins);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_ULONG;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%5d";
                dai.tai.description = "Number of bins per channel";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create currentMode attribute
            {
                std::string name = "currentMode";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_current_mode = new StringUserData(name);
                dai.set_user_data(m_dyn_current_mode);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_STRING;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%s";
                dai.tai.description = "The current mode for TUMBA HMI - Available value : MAPPING";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create triggerMode attribute
            {
                std::string name = "triggerMode";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data     
                m_dyn_trigger_mode = new EnumUserData(name);
                dai.set_user_data(m_dyn_trigger_mode);
                //- describe the dynamic attr we want...
                dai.tai.data_type = Tango::DEV_ENUM;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ_WRITE;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%s";
                dai.tai.enum_labels.push_back("INTERNAL");
                dai.tai.enum_labels.push_back("GATE");
                dai.tai.description = "The curent trigger mode. Available values :\n";
                for( size_t i=0 ; i<dai.tai.enum_labels.size() ; i++ )
                {
                    dai.tai.description += "- " + dai.tai.enum_labels[i] + "\n";
                }

                //- instanciate the read callback (called when the dyn. attr. is read)
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //- instanciate the write callback (called when the dyn. attr. is writen)
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_attribute_callback);
                //- add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create nbFrames attribute
            {
                std::string name = "nbFrames";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data
                m_dyn_nb_frames = new ULongUserData(name);
                dai.set_user_data(m_dyn_nb_frames);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_ULONG;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ_WRITE;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%5d";
                dai.tai.description = "The number of frames for acquisition operation";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //- instanciate the write callback (called when the dyn. attr. is writen)
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);

                //Initialize write/read parts of nbFrames attribute
                try
                {  
                    Tango::WAttribute &nb_frame_attr = dai.dev->get_device_attr()->get_w_attr_by_name("nbFrames");
                    int val = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_nb_frames();
                    m_dyn_nb_frames->set_value(val);
                    nb_frame_attr.set_write_value((Tango::DevULong*)&m_dyn_nb_frames->get_value());
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_nb_frames(val);
                }
                catch( Tango::DevFailed& df )
                {
                    std::stringstream m_status_message;
                    m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                    YAT_ERROR << m_status_message.str() << endl;
                }       
            }

            //Create currentFrame attribute
            {
                std::string name = "currentFrame";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data
                m_dyn_current_frame = new ULongUserData(name);
                dai.set_user_data(m_dyn_current_frame);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_ULONG;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " ";
                dai.tai.format = "%5d";
                dai.tai.description = "The current frame for acquisition operation";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
            }

            //Create exposureTime attribute
            {
                std::string name = "exposureTime";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data
                m_dyn_exposure_time = new DoubleUserData(name);
                dai.set_user_data(m_dyn_exposure_time);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_DOUBLE;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ_WRITE;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = " sec ";
                dai.tai.format = "%6.2f";
                dai.tai.description = "The exposure time per frame for acquisition operation (specified in seconds)";

                //Instanciate the read callback (called when the dyn. attr. is read)    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);

                //Initialize write/read parts of exposureTime attribute
                try
                {  
                    Tango::WAttribute &exposure_time_attr = dai.dev->get_device_attr()->get_w_attr_by_name("exposureTime");
                    double val = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_exposure_time();
                    m_dyn_exposure_time->set_value(val);
                    exposure_time_attr.set_write_value((Tango::DevDouble*)&m_dyn_exposure_time->get_value());
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_exposure_time(val); 
                }
                catch( Tango::DevFailed& df )
                {
                    std::stringstream m_status_message;
                    m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                    YAT_ERROR << m_status_message.str() << endl;
                }   
            }    

            //Create statistics attributes
            {
                m_dyn_realtime.resize(m_nb_channels);
                m_dyn_deadtime.resize(m_nb_channels);
                m_dyn_icr.resize(m_nb_channels);
                m_dyn_ocr.resize(m_nb_channels);
                m_dyn_events_in_run.resize(m_nb_channels);

                for(int i = 0; i < m_nb_channels; i++)
                {
                    stringstream ss("");
                    std::string name("");
                    
                    //Create realTime(i) attributes
                    {
                        name = "realTime";
                        ss << name << std::setfill('0') << std::setw(2)<<i;
                        YAT_VERBOSE << "\t- Create dynamic attribute [" << ss.str() << "]" << endl;
                        yat4tango::DynamicAttributeInfo dai;
                        dai.dev = m_device;
                        dai.tai.name = ss.str();
                        m_dyn_realtime[i] = new DoubleUserData(ss.str());
                        dai.set_user_data(m_dyn_realtime[i]);
                        dai.tai.data_type = Tango::DEV_DOUBLE;
                        dai.tai.data_format = Tango::SCALAR;
                        dai.tai.writable = Tango::READ;
                        dai.tai.disp_level = Tango::OPERATOR;
                        dai.tai.unit = "sec";
                        dai.tai.format = "%.3f";
                        dai.tai.description = "TotalTicks - scaler8/80000000.0";

                        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                        m_dim.dynamic_attributes_manager().add_attribute(dai);
                    }

                    //Create deadTime(i) attributes
                    {
                        ss.str("");
                        name = "deadTime";
                        ss<<name<<std::setfill('0') << std::setw(2)<<i;
                        YAT_VERBOSE << "\t- Create dynamic attribute [" << ss.str() << "]" << endl;
                        yat4tango::DynamicAttributeInfo dai;
                        dai.dev = m_device;
                        dai.tai.name = ss.str();
                        m_dyn_deadtime[i] = new DoubleUserData(ss.str());
                        dai.set_user_data(m_dyn_deadtime[i]);
                        dai.tai.data_type = Tango::DEV_DOUBLE;
                        dai.tai.data_format = Tango::SCALAR;
                        dai.tai.writable = Tango::READ;
                        dai.tai.disp_level = Tango::OPERATOR;
                        dai.tai.unit = "%";
                        dai.tai.format = "%.2f";
                        dai.tai.description = "Deadtime %";

                        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                        m_dim.dynamic_attributes_manager().add_attribute(dai);
                    }

                    //Create inputCountRate(i) attributes
                    {
                        ss.str("");
                        name = "inputCountRate";
                        ss<<name<<std::setfill('0') << std::setw(2)<<i;
                        YAT_VERBOSE << "\t- Create dynamic attribute [" << ss.str() << "]" << endl;
                        yat4tango::DynamicAttributeInfo dai;
                        dai.dev = m_device;
                        dai.tai.name = ss.str();
                        m_dyn_icr[i] = new DoubleUserData(ss.str());
                        dai.set_user_data(m_dyn_icr[i]);
                        dai.tai.data_type = Tango::DEV_DOUBLE;
                        dai.tai.data_format = Tango::SCALAR;
                        dai.tai.writable = Tango::READ;
                        dai.tai.disp_level = Tango::OPERATOR;
                        dai.tai.unit = "cts/sec";
                        dai.tai.format = "%.2f";
                        dai.tai.description = "Input Count Rate = OCR/(1-(Deadtime /100))";

                        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                        m_dim.dynamic_attributes_manager().add_attribute(dai);
                    }

                    //Create outputCountRate(i) attributes
                    {
                        ss.str("");
                        name = "outputCountRate";
                        ss<<name<<std::setfill('0') << std::setw(2)<<i;
                        YAT_VERBOSE << "\t- Create dynamic attribute [" << ss.str() << "]" << endl;
                        yat4tango::DynamicAttributeInfo dai;
                        dai.dev = m_device;
                        dai.tai.name = ss.str();
                        m_dyn_ocr[i] = new DoubleUserData(ss.str());
                        dai.set_user_data(m_dyn_ocr[i]);
                        dai.tai.data_type = Tango::DEV_DOUBLE;
                        dai.tai.data_format = Tango::SCALAR;
                        dai.tai.writable = Tango::READ;
                        dai.tai.disp_level = Tango::OPERATOR;
                        dai.tai.unit = "cts/sec";
                        dai.tai.format = "%.2f";
                        dai.tai.description = "Output Count Rate = AllGood/Time.";

                        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                        m_dim.dynamic_attributes_manager().add_attribute(dai);
                    }

                    //Create eventsInRun(i) attributes
                    {
                        ss.str("");
                        name = "eventsInRun";
                        ss<<name<<std::setfill('0') << std::setw(2)<<i;
                        YAT_VERBOSE << "\t- Create dynamic attribute [" << ss.str() << "]" << endl;
                        yat4tango::DynamicAttributeInfo dai;
                        dai.dev = m_device;
                        dai.tai.name = ss.str();
                        m_dyn_events_in_run[i] = new ULongUserData(ss.str());
                        dai.set_user_data(m_dyn_events_in_run[i]);
                        dai.tai.data_type = Tango::DEV_ULONG;
                        dai.tai.data_format = Tango::SCALAR;
                        dai.tai.writable = Tango::READ;
                        dai.tai.disp_level = Tango::OPERATOR;
                        dai.tai.unit = "cts";
                        dai.tai.format = "%d";
                        dai.tai.description = "Number of all event triggers.";

                        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_attribute_callback);
                        m_dim.dynamic_attributes_manager().add_attribute(dai);
                    }
                }
            }

            //Create channel(ii) attributes
            {
                m_dyn_channel.resize(m_nb_channels);
                for(int i = 0; i < m_nb_channels; i++)
                {
                    {
                        stringstream ss_channel("");
                        std::string name_channel("");
                        name_channel = "channel";
                        ss_channel << name_channel << std::setfill('0') << std::setw(2)<<i;
                        YAT_VERBOSE << "\t- Create dynamic attribute [" << ss_channel.str() << "]" << endl;
                        yat4tango::DynamicAttributeInfo dai;
                        dai.dev = m_device;
                        dai.tai.name = ss_channel.str();
                        m_dyn_channel[i] = new ChannelUserData(ss_channel.str(), m_nb_bins);
                        dai.set_user_data(m_dyn_channel[i]);
                        dai.tai.data_type = Tango::DEV_ULONG;
                        dai.tai.data_format = Tango::SPECTRUM;
                        dai.tai.writable = Tango::READ;
                        dai.tai.disp_level = Tango::OPERATOR;
                        dai.tai.unit = "cts";
                        dai.tai.format = "%d";
                        dai.tai.max_dim_x = m_nb_bins;
                        dai.tai.description = "Histogram of " + ss_channel.str() + " datas";

                        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_channel_callback);
                        m_dim.dynamic_attributes_manager().add_attribute(dai);
                    }
                }
            }
        }
        catch(Tango::DevFailed& df)
        {
            std::string err("Failed to instanciate dynamic attributes  - Tango exception caught - see log attribute for details");
            YAT_ERROR << err << std::endl;
            YAT_ERROR << std::string(df.errors[0].desc) << std::endl;
            return;
        }
    }

    // Creates all dynamic attributes
    void AttrView::create_dyn_stream_attributes()
    {
        YAT_INFO << "AttrView::create_dyn_stream_attributes()" << std::endl;  
        try
        {
            //Create fileGeneration attribute
            {
                std::string name = "fileGeneration";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_file_generation = new BooleanUserData(name);
                dai.set_user_data(m_dyn_file_generation);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_BOOLEAN;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::READ_WRITE;
                dai.tai.disp_level = Tango::EXPERT;
                dai.tai.unit = " ";
                dai.tai.format = "";
                dai.tai.description = "Available only for expert user. Enable/Disable writing the data to a file.";
                
                //Instanciate the read/write callbacks    
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_dynamique_stream_attribute_callback);
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_stream_attribute_callback);
                //Add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
                //Initialize write parts of fileGeneration attribute
                try
                {  
                    Tango::WAttribute &file_generation_attr = dai.dev->get_device_attr()->get_w_attr_by_name("fileGeneration");
                    bool val = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_file_generation();
                    m_dyn_file_generation->set_value(val);
                    file_generation_attr.set_write_value((Tango::DevBoolean*)&m_dyn_file_generation->get_value());
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_is_file_generation(val);
                }
                catch( Tango::DevFailed& df )
                {
                    std::stringstream m_status_message;
                    m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                    YAT_ERROR << m_status_message.str() << endl;
                }            
            }
            //Create streamType attribute
            {
                std::string name = "streamType";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_stream_type = new StringUserData(name);
                dai.set_user_data(m_dyn_stream_type);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_STRING;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::WRITE;
                dai.tai.disp_level = Tango::EXPERT;
                dai.tai.unit = " ";
                dai.tai.format = "%s";
                dai.tai.description = "Available only for expert user. The curent stream type. Available stream types :\nNO_STREAM. \nNEXUS_STREAM. \nCSV_STREAM.";
                //Instanciate the write callback
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_stream_attribute_callback);
                //- add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
                //Initialize write/read parts of streamType attribute
                try
                {  
                    Tango::WAttribute &stream_type_attr_str = dai.dev->get_device_attr()->get_w_attr_by_name("streamType");
                    std::string val = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_type();
                    m_dyn_stream_type->set_value(val);
                    stream_type_attr_str.set_write_value((Tango::DevString*)m_dyn_stream_type->get_value());
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_type(val); 
                }
                catch( Tango::DevFailed& df )
                {
                    std::stringstream m_status_message;
                    m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                    YAT_ERROR << m_status_message.str() << endl;
                }       
            }
            //Create streamTargetPath attribute
            {
                std::string name = "streamTargetPath";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_stream_target_path = new StringUserData(name);
                dai.set_user_data(m_dyn_stream_target_path);//m_dyn_stream_target_file
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_STRING;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::WRITE;
                dai.tai.disp_level = Tango::EXPERT;
                dai.tai.unit = " ";
                dai.tai.format = "%s";
                dai.tai.description = "Available only for expert user. Defines the root path for generated Stream files.";
                //Instanciate the write callback
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_stream_attribute_callback);
                //- add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
                //Initialize write parts of streamTargetPath attribute
                try
                {  
                    Tango::WAttribute &file_stream_target_path_attr = dai.dev->get_device_attr()->get_w_attr_by_name("streamTargetPath");
                    std::string val = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_target_path();
                    m_dyn_stream_target_path->set_value(val);
                    file_stream_target_path_attr.set_write_value((Tango::DevString*)m_dyn_stream_target_path->get_value());
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_target_path(val);
                }
                catch( Tango::DevFailed& df )
                {
                    std::stringstream m_status_message;
                    m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                    YAT_ERROR << m_status_message.str() << endl;
                }   
            }
            //Create streamTargetFile attribute
            {
                std::string name = "streamTargetFile";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_stream_target_file = new StringUserData(name);
                dai.set_user_data(m_dyn_stream_target_file);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_STRING;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::WRITE;
                dai.tai.disp_level = Tango::EXPERT;
                dai.tai.unit = " ";
                dai.tai.format = "%s";
                dai.tai.description = "Available only for expert user. Defines the file name for generated Stream files.";
                //Instanciate the write callback
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_stream_attribute_callback);
                //- add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
                //Initialize write parts of streamTargetFile attribute
                try
                {  
                    Tango::WAttribute &file_stream_target_file_attr = dai.dev->get_device_attr()->get_w_attr_by_name("streamTargetFile");
                    std::string val = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_target_file();
                    m_dyn_stream_target_file->set_value(val);
                    file_stream_target_file_attr.set_write_value((Tango::DevString*)m_dyn_stream_target_file->get_value());
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_target_file(val);
                }
                catch( Tango::DevFailed& df )
                {
                    std::stringstream m_status_message;
                    m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                    YAT_ERROR << m_status_message.str() << endl;
                }   
            }
            //Create streamNbAcqPerFile attribute
            {
                std::string name = "streamNbAcqPerFile";
                YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;       
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = name;
                //- associate the dyn. attr. with its data 
                m_dyn_stream_nb_acq_per_file = new ULongUserData(name);
                dai.set_user_data(m_dyn_stream_nb_acq_per_file);
                //Describe the dynamic attr
                dai.tai.data_type = Tango::DEV_ULONG;
                dai.tai.data_format = Tango::SCALAR;
                dai.tai.writable = Tango::WRITE;
                dai.tai.disp_level = Tango::EXPERT;
                dai.tai.unit = " ";
                dai.tai.format = "%d";
                dai.tai.description = "Available only for expert user. Defines the number of acquisitions for each Stream file.";
                //Instanciate the write callback
                dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrView::write_dynamique_stream_attribute_callback);
                //- add the dyn. attr. to the device
                m_dim.dynamic_attributes_manager().add_attribute(dai);
                //Initialize write/read parts of streamNbAcqPerFile attribute
                try
                {  
                    Tango::WAttribute &nb_acq_per_file_attr = dai.dev->get_device_attr()->get_w_attr_by_name("streamNbAcqPerFile");
                    int val = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_nb_acq_per_file();
                    m_dyn_stream_nb_acq_per_file->set_value(val);
                    nb_acq_per_file_attr.set_write_value((Tango::DevULong*)&m_dyn_stream_nb_acq_per_file->get_value());
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_nb_acq_per_file(val);
                }
                catch( Tango::DevFailed& df )
                {
                    std::stringstream m_status_message;
                    m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                    YAT_ERROR << m_status_message.str() << endl;
                }       
            }
        }
        catch(Tango::DevFailed& df)
        {
            std::string err("Failed to instanciate stream dynamic attributes  - Tango exception caught - see log attribute for details");
            YAT_ERROR << err << std::endl;
            YAT_ERROR << std::string(df.errors[0].desc) << std::endl;
            return;
        }
    }

    void AttrView::create_dyn_roi_attributes()
    {
        YAT_INFO << "AttrView::create_dyn_roi_attributes()" << std::endl;  
        try
        {
            m_dyn_rois_data.clear();
            m_dyn_rois_data.resize(m_nb_channels);
            for (int i = 0; i < m_nb_channels; ++i)
            {
                m_dyn_rois_data[i].resize(m_rois_data[i].size());
                for (int j = 0; j < m_rois_data[i].size() ; ++j)
                {            			
                    const string name = yat::String::str_format("roi%02d_%02d", i, j + 1);
                    yat4tango::DynamicAttributeInfo dai;
                    dai.dev = m_device;
                    dai.tai.name = name;
                    YAT_VERBOSE << "\t- Create dynamic attribute [" << name << "]" << endl;
                    m_dyn_rois_data[i][j] = new RoiUserData(name, i,j);
                    m_dyn_rois_data[i][j]->set_value( (Tango::DevULong)m_rois_data[i][j].roi_sum);
                    dai.set_user_data(m_dyn_rois_data[i][j]);
                    //- describe the dynamic attr we want...
                    dai.tai.data_type = Tango::DEV_ULONG;
                    dai.tai.data_format = Tango::SCALAR;
                    dai.tai.writable = Tango::READ;
                    dai.tai.disp_level = Tango::OPERATOR;
                    dai.tai.unit = "cts";
                    dai.tai.format = "%d";
                    dai.tai.description = "Roi data";
                    //- instanciate the read callback (called when the dyn. attr. is read)
                    dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrView::read_roi_callback);

                    //- add the dyn. attr. to the device
                    m_dim.dynamic_attributes_manager().add_attribute(dai);
                }
            }
            
        }
        catch(Tango::DevFailed& df)
        {
            std::string err("Failed to instanciate stream dynamic attributes  - Tango exception caught - see log attribute for details");
            YAT_ERROR << err << std::endl;
            YAT_ERROR << std::string(df.errors[0].desc) << std::endl;
            return;
        }
    }

    void AttrView::read_dynamique_attribute_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
    {
        try
        {
            Tango::DevState state = static_cast<Xspress3*>(m_device)->get_controller()->get_state();
            bool is_device_initialized = static_cast<Xspress3*>(m_device)->is_device_initialized();
            if ((state == Tango::FAULT && !is_device_initialized)
                || state == Tango::INIT 
                || state == Tango::DISABLE)
            {
                std::string reason = "It's currently not allowed to read attribute " + cbd.dya->get_name();
                Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                            reason.c_str(),
                                            "AttrView::read_dynamique_attribute_callback()");
            }

            // be sure that the pointer to the dyn. attr. is valid
            if(!cbd.dya)
            {
                THROW_DEVFAILED("INTERNAL_ERROR",
                                "unexpected NULL pointer to dynamic attribute",
                                "AttrView::read_dynamique_attribute_callback");
            }

            switch(cbd.dya->get_tango_data_type())
            {
                case Tango::DEV_STRING:
                {
                    StringUserData* user_data = cbd.dya->get_user_data<StringUserData>();
                    cbd.tga->set_value((Tango::DevString*)user_data->get_value());
                }
                break;

                case Tango::DEV_ULONG:
                {       
                    ULongUserData* user_data = cbd.dya->get_user_data<ULongUserData>();
                    cbd.tga->set_value((Tango::DevULong*)&user_data->get_value());
                }
                break;

                case Tango::DEV_ENUM:
                {
                    EnumUserData* user_data = cbd.dya->get_user_data<EnumUserData>();
                    cbd.tga->set_value((Tango::DevEnum*)&user_data->get_value());
                }
                break;
                
                case Tango::DEV_DOUBLE:
                {
                    DoubleUserData* user_data = cbd.dya->get_user_data<DoubleUserData>();
                    cbd.tga->set_value((Tango::DevDouble*)&user_data->get_value());
                }
                break;

                default:
                {
                    THROW_DEVFAILED("INTERNAL_ERROR",
                            "Unknown tango data type !",
                            "AttrView::read_dynamique_attribute_callback()");
                }
                break;
            }
        
        }
        catch (Tango::DevFailed& df)
        {
            YAT_ERROR << std::string(df.errors[0].desc) << endl;
            //- rethrow exception
            Tango::Except::re_throw_exception(df,
                                            "TANGO_DEVICE_ERROR",
                                            std::string(df.errors[0].desc).c_str(),
                                            "AttrView::read_dynamique_attribute_callback()");
        }
    }

    void AttrView::write_dynamique_attribute_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd)
    {
        YAT_VERBOSE << "AttrView::write_dynamique_attribute_callback() - [" << cbd.dya->get_name() << "] attribute." << endl;
        try
        {
            Tango::DevState state = dynamic_cast<Xspress3*> (m_device)->get_controller()->get_state();
            if(state == Tango::FAULT ||
            state == Tango::INIT ||
            state == Tango::RUNNING ||
            state == Tango::OFF )
            {
                std::string reason = "It's currently not allowed to write attribute " + cbd.dya->get_name();
                Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                            reason.c_str(),
                                            "AttrView::write_dynamique_attribute_callback()");
            }

            //- be sure the pointer to the dyn. attr. is valid
            if(!cbd.dya)
            {
                THROW_DEVFAILED("INTERNAL_ERROR",
                                "unexpected NULL pointer to dynamic attribute",
                                "AttrView::write_dynamique_attribute_callback");
            }
            switch(cbd.dya->get_tango_data_type())
            {
                case Tango::DEV_ULONG:
                {
                    Tango::DevULong val;
                    cbd.tga->get_write_value(val);
                    ULongUserData *user_data = cbd.dya->get_user_data<ULongUserData>();
                    
                    //nbFrame is the only attribute READ_WRITE of DEV_ULONG type
                    user_data->set_value(val);
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_nb_frames(val);
                    yat4tango::PropertyHelper::set_property(m_device, "MemorizedNbFrames", val);	

                }
                break;

                case Tango::DEV_DOUBLE:
                {
                    Tango::DevDouble val;
                    cbd.tga->get_write_value(val);
                    DoubleUserData *user_data = cbd.dya->get_user_data<DoubleUserData>();
                    
                    //exposureTime is the only attribute READ_WRITE of DEV_DOUBLE type
                    user_data->set_value(val);
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_exposure_time(val);
                    yat4tango::PropertyHelper::set_property(m_device, "MemorizedExposureTime", val);	
                }
                break;

                case Tango::DEV_ENUM:
                {
                    Tango::DevEnum val;
                    cbd.tga->get_write_value(val);
                    EnumUserData *user_data = cbd.dya->get_user_data<EnumUserData>();

                    //triggerMode is the only attribute READ_WRITE of DEV_DOUBLE type
                    user_data->set_value(val);
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_trigger_mode(static_cast<TriggerMode>(val));
                    std::string memorize_trigger_mode_str = (val) ? "GATE" : "INTERNAL";
                    yat4tango::PropertyHelper::set_property(m_device, "MemorizedTriggerMode", memorize_trigger_mode_str);	
                }
                break;

                default:
                {
                    THROW_DEVFAILED("INTERNAL_ERROR",
                            "Unknown tango data type !",
                            "AttrView::write_dynamique_attribute_callback()");
                }
                break;
            }
        }
        catch(Tango::DevFailed& df)
        {
            YAT_ERROR << std::string(df.errors[0].desc) << endl;
            //- rethrow exception
            Tango::Except::re_throw_exception(df,
                                            "TANGO_DEVICE_ERROR",
                                            string(df.errors[0].desc).c_str(),
                                            "AttrView::write_dynamique_attribute_callback()");
        }
    }

    void AttrView::read_channel_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
    {
        Tango::DevState state = static_cast<Xspress3*>(m_device)->get_controller()->get_state();
        bool is_device_initialized = static_cast<Xspress3*>(m_device)->is_device_initialized();
        if ((state == Tango::FAULT && !is_device_initialized)
            || state == Tango::INIT
            || state == Tango::DISABLE)

        {
            std::string reason = "It's currently not allowed to read attribute " + cbd.dya->get_name();
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                        reason.c_str(),
                                        "AttrView::read_channel_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "DynamicInterface::read_channel_callback");
        }
        try
        {
            ChannelUserData* channel_data = cbd.dya->get_user_data<ChannelUserData>();
            //- set the attribute value
            cbd.tga->set_value(const_cast<Tango::DevULong*> (&channel_data->get_value()[0]), channel_data->get_value().size());
        }
        catch(Tango::DevFailed& df)
        {
            YAT_ERROR <<std::string(df.errors[0].desc) << endl;
            //- rethrow exception
            Tango::Except::re_throw_exception(df,
                                            "TANGO_DEVICE_ERROR",
                                            string(df.errors[0].desc).c_str(),
                                            "AttrView::read_channel_callback");
        }
    }

    void AttrView::read_dynamique_stream_attribute_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
    {
        try
        {
            Tango::DevState state = static_cast<Xspress3*>(m_device)->get_controller()->get_state();
            bool is_device_initialized = static_cast<Xspress3*>(m_device)->is_device_initialized();
            if ((state == Tango::FAULT && !is_device_initialized)
                || state == Tango::INIT)
            {
                std::string reason = "It's currently not allowed to read attribute " + cbd.dya->get_name();
                Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                            reason.c_str(),
                                            "AttrView::read_dynamique_stream_attribute_callback()");
            }

            //- be sure the pointer to the dyn. attr. is valid
            if(!cbd.dya)
            {
                THROW_DEVFAILED("INTERNAL_ERROR",
                                "unexpected NULL pointer to dynamic attribute",
                                "DynamicInterface::read_dynamique_stream_attribute_callback");
            }

            switch(cbd.dya->get_tango_data_type())
            {
                case Tango::DEV_BOOLEAN:
                {     
                    BooleanUserData* user_data = cbd.dya->get_user_data<BooleanUserData>();
                    cbd.tga->set_value((Tango::DevBoolean*)&user_data->get_value());
                }
                break;

                default:
                {
                    THROW_DEVFAILED("INTERNAL_ERROR",
                            "Unknown tango data type !",
                            "AttrView::read_dynamique_stream_attribute_callback()");
                }
                break;
            }
        }

        catch(Tango::DevFailed& df)
        {
            YAT_ERROR << std::string(df.errors[0].desc) << endl;
            Tango::Except::re_throw_exception(df,
                                            "TANGO_DEVICE_ERROR",
                                            string(df.errors[0].desc).c_str(),
                                            "AttrView::read_dynamique_stream_attribute_callback");
        }
    }

    void AttrView::write_dynamique_stream_attribute_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd)
    {
        YAT_VERBOSE << "AttrView::write_dynamique_stream_attribute_callback() - [" << cbd.dya->get_name() << "] attribute." << std::endl;
        try
        {
            Tango::DevState state = dynamic_cast<Xspress3*> (m_device)->get_controller()->get_state();
            if(state == Tango::FAULT ||
            state == Tango::INIT ||
            state == Tango::RUNNING ||
            state == Tango::OFF )
            {
                std::string reason = "It's currently not allowed to write attribute " + cbd.dya->get_name();
                Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                            reason.c_str(),
                                            "AttrView::write_dynamique_stream_attribute_callback()");
            }

            //- be sure the pointer to the dyn. attr. is valid
            if(!cbd.dya)
            {
                YAT_ERROR << "WRITE INTERNAL_ERROR unexpected NULL pointer to dynamic attribute" << std::endl;
                THROW_DEVFAILED("INTERNAL_ERROR",
                                "unexpected NULL pointer to dynamic attribute",
                                "AttrView::write_dynamique_stream_attribute_callback2");
            }

            switch(cbd.dya->get_tango_data_type())
            {
                case Tango::DEV_BOOLEAN:
                {
                    Tango::DevBoolean val;
                    cbd.tga->get_write_value(val);
                    BooleanUserData *user_data = cbd.dya->get_user_data<BooleanUserData>();
                    user_data->set_value(val);
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_is_file_generation(val);
                    
                    //streamType value depends on fileGeneration value
                    bool is_file_generation_current = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_file_generation();
                    if(true == is_file_generation_current)
                    {
                        string stream_type_current_str = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_type();
                        if((stream_type_current_str != STREAM_TYPE_CSV) && 
                            (stream_type_current_str != STREAM_TYPE_NEXUS))
                        {
                            try
                            {
                                Tango::WAttribute &stream_type_attr_str = m_device->get_device_attr()->get_w_attr_by_name("streamType");
                                std::string stream_type_new_val_str = STREAM_TYPE_NEXUS;
                                m_dyn_stream_type->set_value(stream_type_new_val_str);
                                stream_type_attr_str.set_write_value((Tango::DevString*)m_dyn_stream_type->get_value());
                                static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_type(stream_type_new_val_str);//static_cast<StreamType>(val));
                                yat4tango::PropertyHelper::set_property(m_device, "MemorizedStreamType", stream_type_new_val_str);	
                            }
                            catch( Tango::DevFailed& df )
                            {
                                std::stringstream m_status_message;
                                m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                                YAT_ERROR << m_status_message.str() << endl;
                            }  
                        }
                    }
                    else
                    {
                        try
                        {
                            Tango::WAttribute &stream_type_attr_str = m_device->get_device_attr()->get_w_attr_by_name("streamType");
                            string stream_type_new_val_str = STREAM_TYPE_NO_STREAM;
                            m_dyn_stream_type->set_value(stream_type_new_val_str);
                            stream_type_attr_str.set_write_value((Tango::DevString*)m_dyn_stream_type->get_value());
                            static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_type(stream_type_new_val_str);
                            yat4tango::PropertyHelper::set_property(m_device, "MemorizedStreamType", stream_type_new_val_str);	
                        }
                        catch( Tango::DevFailed& df )
                        {
                            std::stringstream m_status_message;
                            m_status_message << "Initialization Failed : " << string(df.errors[0].desc) << endl;
                            YAT_ERROR << m_status_message.str() << endl;
                        }  
                    }
                }
                break;

                case Tango::DEV_STRING:
                {

                    Tango::DevString val;
                    cbd.tga->get_write_value(val);
                    StringUserData *user_data = cbd.dya->get_user_data<StringUserData>();
                    user_data->set_value(val);

                    if(cbd.dya->get_name() == "streamType")
                    {
                        std::string val_str = val;
                        std::transform(val_str.begin(), val_str.end(), val_str.begin(), ::toupper);
                        if((val_str != STREAM_TYPE_NO_STREAM) && 
                            (val_str != STREAM_TYPE_CSV) && 
                            (val_str != STREAM_TYPE_NEXUS))
                        {
                            Tango::Except::throw_exception("DEVICE_ERROR",
                                                        "Wrong Stream Type:\n"
                                                        "Possibles values are:\n"
                                                        "NO_STREAM\n"
                                                        "CSV_STREAM\n"
                                                        "NEXUS_STREAM",
                                                        "AttrView::write_dynamique_stream_attribute_callback()");
                        }
                        
                        static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_type(val_str);
                        yat4tango::PropertyHelper::set_property(m_device, "MemorizedStreamType", val_str);	

                        update_file_generation_attribute();
                    } 
                    else if(cbd.dya->get_name() == "streamTargetPath")
                    {
                        static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_target_path(val);
                        yat4tango::PropertyHelper::set_property(m_device, "MemorizedStreamTargetPath", val);	
                    }
                    else if(cbd.dya->get_name() == "streamTargetFile")
                    {
                        static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_target_file(val);
                        yat4tango::PropertyHelper::set_property(m_device, "MemorizedStreamTargetFile", val);	
                    }
                    else{/*Nothing to do*/}

                }
                break;

                case Tango::DEV_ULONG:
                {
                    Tango::DevULong val;
                    cbd.tga->get_write_value(val);
                    ULongUserData *user_data = cbd.dya->get_user_data<ULongUserData>();
                
                    //streamNbAcqPerFile is the only attribute READ_WRITE of DEV_ULONG type
                    user_data->set_value(val);
                    static_cast<Xspress3 *>(m_device)->get_controller()->set_stream_nb_acq_per_file(val);
                    yat4tango::PropertyHelper::set_property(m_device, "MemorizedStreamNbAcqPerFile", val);	
                }
                break;

                default:
                {
                    THROW_DEVFAILED("INTERNAL_ERROR",
                            "Unknown tango data type !",
                            "AttrView::write_dynamique_stream_attribute_callback()");
                }
                break;
            }
        }
        catch(Tango::DevFailed& df)
        {
            YAT_ERROR << std::string(df.errors[0].desc) << endl;
            //- rethrow exception
            Tango::Except::re_throw_exception(df,
                                            "TANGO_DEVICE_ERROR",
                                            string(df.errors[0].desc).c_str(),
                                            "AttrView::write_dynamique_stream_attribute_callback()");
        }
    }

    void AttrView::read_roi_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
    {
    	Tango::DevState state = static_cast<Xspress3*>(m_device)->get_controller()->get_state();
    	bool is_device_initialized = static_cast<Xspress3*>(m_device)->is_device_initialized();
    	if ((state == Tango::FAULT && !is_device_initialized)
    			|| state == Tango::INIT
                || state == Tango::DISABLE)
    	{
    		std::string reason = "It's currently not allowed to read attribute " + cbd.dya->get_name();
    		Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
    				reason.c_str(),
					"AttrView::read_roi_callback()");
    	}

    	//- be sure the pointer to the dyn. attr. is valid
    	if(!cbd.dya)
    	{
    		THROW_DEVFAILED("INTERNAL_ERROR",
    				"unexpected NULL pointer to dynamic attribute",
					"DynamicInterface::read_callback");
    	}
        try
        {
            //- set the attribute value
            RoiUserData* roi_data = cbd.dya->get_user_data<RoiUserData>();
            uint32_t value = m_rois_data.at(roi_data->get_channel()).at(roi_data->get_roi_num()).roi_sum;
            roi_data->set_value(value);
            cbd.tga->set_value((Tango::DevULong*)(&roi_data->get_value()));

        }
        catch(Tango::DevFailed& df)
        {
            ERROR_STREAM << df << endl;
            //- rethrow exception
            Tango::Except::re_throw_exception(df,
                                              "TANGO_DEVICE_ERROR",
                                              string(df.errors[0].desc).c_str(),
                                              "AttrViewMapping::read_hroi_callback");
        }
    }

    void AttrView::write_attribute_at_init()
    {
        YAT_INFO << "AttrView::write_attribute_at_init()." << std::endl;

        try
        {
            //MemorizedStreamTargetPath 
            std::string memorized_stream_target_path = yat4tango::PropertyHelper::get_property<std::string > (m_device, "MemorizedStreamTargetPath");
            Tango::WAttribute &stream_target_path_attr = m_device->get_device_attr()->get_w_attr_by_name("streamTargetPath");
            stream_target_path_attr.set_write_value(memorized_stream_target_path);
            
            yat4tango::DynamicAttributeWriteCallbackData cbd_stream_target_path;
            cbd_stream_target_path.dya = &m_dim.dynamic_attributes_manager().get_attribute("streamTargetPath");
            cbd_stream_target_path.tga = &stream_target_path_attr;
            write_dynamique_stream_attribute_callback(cbd_stream_target_path);

            //MemorizedStreamTargetFile
            std::string memorized_stream_target_file = yat4tango::PropertyHelper::get_property<std::string > (m_device, "MemorizedStreamTargetFile");
            Tango::WAttribute &stream_target_file_attr = m_device->get_device_attr()->get_w_attr_by_name("streamTargetFile");
            stream_target_file_attr.set_write_value(memorized_stream_target_file);
            
            yat4tango::DynamicAttributeWriteCallbackData cbd_stream_target_file;
            cbd_stream_target_file.dya = &m_dim.dynamic_attributes_manager().get_attribute("streamTargetFile");
            cbd_stream_target_file.tga = &stream_target_file_attr;
            write_dynamique_stream_attribute_callback(cbd_stream_target_file);
            
            //MemorizedExposureTime
            Tango::DevDouble memorized_exposure_time = yat4tango::PropertyHelper::get_property<Tango::DevDouble > (m_device, "MemorizedExposureTime");
            Tango::WAttribute &exposure_time_attr = m_device->get_device_attr()->get_w_attr_by_name("exposureTime");
            exposure_time_attr.set_write_value(memorized_exposure_time);
                       
            yat4tango::DynamicAttributeWriteCallbackData cbd_exposure_time;
            cbd_exposure_time.dya = &m_dim.dynamic_attributes_manager().get_attribute("exposureTime");
            cbd_exposure_time.tga = &exposure_time_attr;
            write_dynamique_attribute_callback(cbd_exposure_time);
            
            //MemorizedTriggerMode
            string memorized_trigger_mode_str = yat4tango::PropertyHelper::get_property<std::string> (m_device, "MemorizedTriggerMode");
            short memorized_trigger_mode = (memorized_trigger_mode_str == "GATE") ? TriggerMode::GATE : TriggerMode::INTERNAL;
            Tango::WAttribute &trigger_mode_attr = m_device->get_device_attr()->get_w_attr_by_name("triggerMode");
            trigger_mode_attr.set_write_value(memorized_trigger_mode);
            
            yat4tango::DynamicAttributeWriteCallbackData cbd_trigger_mode;
            cbd_trigger_mode.dya = &m_dim.dynamic_attributes_manager().get_attribute("triggerMode");
            cbd_trigger_mode.tga = &trigger_mode_attr;
            write_dynamique_attribute_callback(cbd_trigger_mode);
            
            //MemorizedStreamType
            std::string memorized_stream_type = yat4tango::PropertyHelper::get_property<std::string > (m_device, "MemorizedStreamType");
            Tango::WAttribute &stream_type_attr = m_device->get_device_attr()->get_w_attr_by_name("streamType");
            stream_type_attr.set_write_value(memorized_stream_type);
                        
            yat4tango::DynamicAttributeWriteCallbackData cbd_stream_type;
            cbd_stream_type.dya = &m_dim.dynamic_attributes_manager().get_attribute("streamType");
            cbd_stream_type.tga = &stream_type_attr;
            write_dynamique_stream_attribute_callback(cbd_stream_type);
            
            //MemorizedNbFrames
            Tango::DevULong memorized_nb_frames = yat4tango::PropertyHelper::get_property<Tango::DevULong> (m_device, "MemorizedNbFrames");
            Tango::WAttribute &nb_frames_attr = m_device->get_device_attr()->get_w_attr_by_name("nbFrames");
            nb_frames_attr.set_write_value(memorized_nb_frames);
                       
            yat4tango::DynamicAttributeWriteCallbackData cbd_nb_frames;
            cbd_nb_frames.dya = &m_dim.dynamic_attributes_manager().get_attribute("nbFrames");
            cbd_nb_frames.tga = &nb_frames_attr;
            write_dynamique_attribute_callback(cbd_nb_frames);
                        
            //MemorizedStreamNbAcqPerFile
            Tango::DevULong memorized_stream_nb_acq_per_file = yat4tango::PropertyHelper::get_property<Tango::DevULong> (m_device, "MemorizedStreamNbAcqPerFile");
            Tango::WAttribute &stream_nb_acq_per_file_attr = m_device->get_device_attr()->get_w_attr_by_name("streamNbAcqPerFile");
            stream_nb_acq_per_file_attr.set_write_value(memorized_stream_nb_acq_per_file);
            
            yat4tango::DynamicAttributeWriteCallbackData cbd_stream_nb_acq_per_file;
            cbd_stream_nb_acq_per_file.tga = &stream_nb_acq_per_file_attr;
            cbd_stream_nb_acq_per_file.dya = &m_dim.dynamic_attributes_manager().get_attribute("streamNbAcqPerFile");
            write_dynamique_stream_attribute_callback(cbd_stream_nb_acq_per_file);
            
            YAT_INFO << "AttrView::write_attribute_at_init() - Memorized write attributes :" << std::endl;
            YAT_INFO << "\t- stream_target_path = " 	 << memorized_stream_target_path << std::endl;
            YAT_INFO << "\t- stream_target_file = " 	 << memorized_stream_target_file << std::endl;
            YAT_INFO << "\t- stream_type = " 	         << memorized_stream_type << std::endl;
            YAT_INFO << "\t- stream_nb_acq_per_file = "  << memorized_stream_nb_acq_per_file << std::endl;
            YAT_INFO << "\t- exposure_time = " 	         << memorized_exposure_time << std::endl;
            YAT_INFO << "\t- nb_frames = " 	             << memorized_nb_frames << std::endl;
            YAT_INFO << "\t- trigger_mode = " 	         << memorized_trigger_mode_str << std::endl;

        }
        catch( Tango::DevFailed& df )
        {
            std::stringstream m_status_message;
            m_status_message << "Initialization Attribute Failed : " << string(df.errors[0].desc) << endl;
            YAT_ERROR << m_status_message.str() << endl;
        } 
    }

    void AttrView::update_file_generation_attribute()
    {
        YAT_VERBOSE << "update_file_generation_attribute" << std::endl;
        std::string stream_type_current_str = dynamic_cast<Xspress3*>(m_device)->get_controller()->get_stream_type();
        bool file_generation_new_val = false;
        if((stream_type_current_str == STREAM_TYPE_CSV) 
            || (stream_type_current_str == STREAM_TYPE_NEXUS))
        {
            file_generation_new_val = true;
        }
        else // NO_STREAM
        {
            file_generation_new_val = false;
        }

        static_cast<Xspress3 *>(m_device)->get_controller()->set_is_file_generation(file_generation_new_val);
        
        //fileGeneration value depends on streamType value
        Tango::WAttribute &stream_file_generation_attr = m_device->get_device_attr()->get_w_attr_by_name("fileGeneration");
        m_dyn_file_generation->set_value(file_generation_new_val);
        stream_file_generation_attr.set_write_value((Tango::DevBoolean*)&m_dyn_file_generation->get_value());
    }


}
