/*************************************************************************
/*! 
 * \file AttrView.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class AttrView
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef ATTR_VIEW_H
#define ATTR_VIEW_H

#include <tango.h>

#include <yat/any/Any.h>
#include <yat/utils/Callback.h>
#include <yat/memory/SharedPtr.h>
#include <yat4tango/DynamicInterfaceManager.h>
#include <yat4tango/DynamicAttributeManager.h>

#include "UserData.h"
#include "DataStore.h"

namespace Xspress3_ns
{

//--------------------------------------------------------------------------------
//! \class AttrView
//! \brief Class to manage all dynamic attributs
//--------------------------------------------------------------------------------
class AttrView : public Tango::LogAdapter
{
public:
    //! \brief Constructor
    AttrView(Tango::DeviceImpl *dev);

    //! \brief Destructor 
    virtual ~AttrView();

    //! \brief Create and initialize dynamic attributes
    void init(yat::SharedPtr<DataStore> data_store);

    //! \brief Initialize acquisition dynamic attributes at the beginning of acquisition 
    void init_acquisition_parametres();

    //! \brief Updates the data of the attributes, this method is fired by the DataStore
    void update_data(int frame, int channel, yat::SharedPtr<DataStore> data_store);

    //! \brief Updates common dynamic attributes
    void init_common_attributes();

    void write_attribute_at_init();

private:
    //! \brief Creates common and acquisition dynamic attributes
    void create_dyn_attributes();

    //! \brief Creates all stream dynamic attributes
    void create_dyn_stream_attributes();
    
    //! \brief Creates rois dynamic attributes
    void create_dyn_roi_attributes();

    //! \brief callback read method for tango dyn attributes
    //! \param cbd The dynamic attribute
    void read_dynamique_attribute_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);

    //! \brief callback write method for tango dyn attributes
    //! \param cbd The dynamic attribute
    void write_dynamique_attribute_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd);

    //! \brief callback read method for tango dyn spectrum attributes
    //! \param cbd The dynamic attribute
    void read_channel_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);

    //! \brief callback read method for tango stream dyn attributes
    //! \param cbd The stream dynamic attribute
    void read_dynamique_stream_attribute_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);

    //! \brief callback write method for tango stream dyn attributes
    //! \param cbd The stream dynamic attribute
    void write_dynamique_stream_attribute_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd);

    //! \brief callback read method for tango dyn rois attributes
    //! \param cbd The dynamic attribute
    void read_roi_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);
    
    void update_file_generation_attribute();
    //! \brief m_dim: Dynamic attributes objects        
    yat4tango::DynamicInterfaceManager m_dim;

    //! \brief m_device: Owner Device server object    
    Tango::DeviceImpl* m_device;

    //! \brief m_dyn_firmware_revision: User data attached to the firmwareRevision dynamic attribute
    StringUserData* m_dyn_firmware_revision;

    //! \brief m_dyn_nb_frames : User data attached to the nbFrames dynamic attribute
    ULongUserData* m_dyn_nb_frames;

    //! \brief m_dyn_current_frame : User data attached to the currentFrame dynamic attribute
    ULongUserData* m_dyn_current_frame;

    //! \brief m_dyn_board_type : User data attached to the boardType dynamic attribute
    StringUserData* m_dyn_board_type;

    //! \brief m_dyn_nb_modules : User data attached to the nbModules dynamic attribute
    ULongUserData* m_dyn_nb_modules;

    //! \brief m_dyn_nb_channels : User data attached to the nbChannels dynamic attribute
    ULongUserData* m_dyn_nb_channels;

    //! \brief m_dyn_nb_bins : User data attached to the nbBins dynamic attribute
    ULongUserData* m_dyn_nb_bins;

    //! \brief m_dyn_trigger_mode : User data attached to the triggerMode dynamic attribute
    EnumUserData* m_dyn_trigger_mode;

    //! \brief m_dyn_exposure_time : User data attached to the exposureTime dynamic attribute
    DoubleUserData* m_dyn_exposure_time;

    //! \brief m_dyn_current_mode : User data attached to the currentMode dynamic attribute
    StringUserData* m_dyn_current_mode;

    //! \brief m_dyn_realtime : User data attached to the realTime(i) dynamic attribute
    std::vector<DoubleUserData*> m_dyn_realtime;

    //! \brief m_dyn_realtime : User data attached to the deadTime(i) dynamic attribute
    std::vector<DoubleUserData*> m_dyn_deadtime;

    //! \brief m_dyn_realtime : User data attached to the icr(i) dynamic attribute
    std::vector<DoubleUserData*> m_dyn_icr;

    //! \brief m_dyn_realtime : User data attached to the ocr(i) dynamic attribute
    std::vector<DoubleUserData*> m_dyn_ocr;

    //! \brief m_dyn_realtime : User data attached to the events_in_run(i) dynamic attribute
    std::vector<ULongUserData*> m_dyn_events_in_run;

    //! \brief m_dyn_realtime : User data attached to the channel(i) dynamic attribute
    std::vector<ChannelUserData*> m_dyn_channel;

    //! \brief m_dyn_file_generation : User data attached to the fileGeneration stream dynamic attribute
    BooleanUserData* m_dyn_file_generation;

    //! \brief m_dyn_stream_type : User data attached to the streamType stream dynamic attribute
    StringUserData* m_dyn_stream_type;

    //! \brief m_dyn_stream_target_path : User data attached to the streamTargetPath stream dynamic attribute
    StringUserData* m_dyn_stream_target_path;

    //! \brief m_dyn_stream_target_file : User data attached to the streamTargetFile stream dynamic attribute
    StringUserData* m_dyn_stream_target_file;

    //! \brief m_dyn_stream_target_file : User data attached to the streamNbAcqPerFile stream dynamic attribute
    ULongUserData* m_dyn_stream_nb_acq_per_file;

    //! \brief m_dyn_rois_data : User data attached to the roi(i)(j) dynamic attribute
    std::vector< std::vector<RoiUserData*> > m_dyn_rois_data;

    //! \brief The container of rois datas
    std::vector< std::vector<RoisData> > m_rois_data;

    //! \brief The number of channels of the xspress3 device
    int m_nb_channels;

    //! \brief The number of bins of each frame
    int m_nb_bins;

std::string m_memorized_stream_target_path;
};

}

#endif // ATTR_VIEW_H
