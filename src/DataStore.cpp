/*************************************************************************/
/*! 
 *  \file   DataStore.cpp
 *  \brief  Implementation of DataStore methods.
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/
#include <tango.h>
#include "Controller.h"
#include "DataStore.h"

namespace Xspress3_ns
{
//Constructor
DataStore::DataStore(Tango::DeviceImpl *dev)
: yat4tango::DeviceTask(dev),
  m_device(dev),
  m_nb_channels(4),
  m_nb_frames(1),
  m_nb_bins(4096),
  m_last_acquired_frame(0)
{
    YAT_INFO << "DataStore::DataStore()" << std::endl;
    enable_timeout_msg(false);
    enable_periodic_msg(false);
    set_periodic_msg_period(DATASTORE_TASK_PERIODIC_MS);
    set_status("");
    set_state(Tango::STANDBY);
}

// Destructor
DataStore::~DataStore()
{
    YAT_INFO << "DataStore::~DataStore()" << std::endl;
    for(unsigned i = 0; i < m_datas.m_frame_data.size(); i++)
	{
        m_datas.m_frame_data[i].m_channel_data.clear();
	}
	m_datas.m_frame_data.clear();

	if(!m_rois_data.empty())
	{
		m_rois_data.clear();
	}
}

void DataStore::init(int nb_channels, int nb_frames, int nb_bins)
{
    YAT_INFO << "DataStore::init()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);
    m_nb_channels = nb_channels;
    m_nb_frames = nb_frames;
    m_nb_bins = nb_bins;

    m_datas.m_frame_data.clear();
    m_datas.m_frame_data.resize(m_nb_frames);

    for(unsigned iframe = 0; iframe < m_datas.m_frame_data.size(); ++iframe)
	{
        m_datas.m_frame_data[iframe].m_channel_data.clear();
        m_datas.m_frame_data[iframe].m_channel_data.resize(m_nb_channels);
	}
    
    for (int iframe = 0; iframe < m_nb_frames; iframe++)
    {
        for (int ich = 0; ich < m_nb_channels; ich++)
        {
            m_datas.m_frame_data[iframe].m_channel_data[ich].m_dead_time = 0.0;
            m_datas.m_frame_data[iframe].m_channel_data[ich].m_real_time = 0.0;
            m_datas.m_frame_data[iframe].m_channel_data[ich].m_input_count_rate = 0.0;
            m_datas.m_frame_data[iframe].m_channel_data[ich].m_output_count_rate = 0.0;
            m_datas.m_frame_data[iframe].m_channel_data[ich].m_events_in_run = 0;
            m_datas.m_frame_data[iframe].m_channel_data[ich].m_spectrum_datas.resize(m_nb_bins,0);
        }
    }

    m_rois_data.resize(m_nb_channels);
}

int DataStore::get_nb_channels()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_nb_channels;
}

int DataStore::get_nb_bins()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_nb_bins;
}

Tango::DevState DataStore::get_state()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return m_state;
}

void DataStore::set_state(Tango::DevState state)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

std::string DataStore::get_status()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

void DataStore::set_status(const std::string& status)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

std::vector<uint32_t> DataStore::get_frame_spectrum_data(int frame, int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    std::vector<uint32_t> spectrum_data = m_datas.m_frame_data[frame].m_channel_data[channel].m_spectrum_datas;
    return spectrum_data;
}

void DataStore::store_statistics_data(int frame, int channel, FrameData frame_data)
{
    yat::MutexLock scoped_lock(m_data_lock);
    set_state(Tango::RUNNING);

    double real_time = frame_data.m_real_time/80000000.0;
	m_datas.m_frame_data[frame].m_channel_data[channel].m_real_time = real_time;

    m_datas.m_frame_data[frame].m_channel_data[channel].m_dead_time = frame_data.m_dead_time;

    double output_count_rate = frame_data.m_output_count_rate/real_time;
	m_datas.m_frame_data[frame].m_channel_data[channel].m_output_count_rate = output_count_rate;

	double dead_time_correction_factor = frame_data.m_deadtime_correction_factor;
    double input_count_rate = (frame_data.m_input_count_rate/real_time)/(1 - (frame_data.m_dead_time/100) );
    m_datas.m_frame_data[frame].m_channel_data[channel].m_input_count_rate = input_count_rate;

    unsigned long events_in_run = frame_data.m_events_in_run;
    m_datas.m_frame_data[frame].m_channel_data[channel].m_events_in_run = events_in_run;
}

void DataStore::store_spectrum_data(int current_frame, int channel, uint32_t* data, size_t length)
{
    yat::MutexLock scoped_lock(m_data_lock);
    set_state(Tango::RUNNING);

    if(length!=0)
    {
        std::copy(data, data + length, &m_datas.m_frame_data[current_frame].m_channel_data[channel].m_spectrum_datas[0]);
    }

    //Update rois values with acquisition values
    update_rois_data(current_frame,channel);

    //notify data and view
    notify_data(channel, current_frame);
}

void DataStore::subscribe(Controller* observer)
{
    yat::MutexLock scoped_lock(m_data_lock);
    m_controller = observer;
}

void DataStore::abort(std::string status)
{
    YAT_INFO << "DataStore::abort()" << std::endl;    
    yat::Message* msg = yat::Message::allocate(DATASTORE_ABORT_MSG, DEFAULT_MSG_PRIORITY, true);
    msg->attach_data(status);
    post(msg);
}

void DataStore::process_message(yat::Message& msg)
{
    try
    {
        switch (msg.type())
        {
            case yat::TASK_INIT:
            {
                YAT_VERBOSE << "DataStore::process_message::TASK_INIT" << std::endl;
            }
            break;

            case yat::TASK_EXIT:
            {
                YAT_VERBOSE << "DataStore::process_message::TASK_EXIT" << std::endl;
            }
            break;

            case yat::TASK_TIMEOUT:
            {
                YAT_ERROR << "DataStore::process_message::TASK_TIMEOUT" << std::endl;
            }
            break;

            case yat::TASK_PERIODIC:
            {
                YAT_VERBOSE << "DataStore::process_message::TASK_PERIODIC" << std::endl;
            }
            break;

            case DATASTORE_CLOSE_DATA_MSG:
            {
                YAT_VERBOSE <<"DataStore::process_message::DATASTORE_CLOSE_DATA_MSG" << std::endl;
                try
                {
                    yat::MutexLock scoped_lock(m_data_lock);
                    if(m_controller != 0)
                    {
                        m_controller->close_stream();
                    }
                    set_state(Tango::STANDBY);
                }
                catch (Tango::DevFailed &df)
                {
                    ERROR_STREAM << df << endl;
                    set_state(Tango::FAULT);
                }
            }
            break;
            
            case DATASTORE_ABORT_MSG:
            {
                YAT_VERBOSE <<"DataStore::DATASTORE_ABORT_MSG" << std::endl;
                try
                {
                    yat::MutexLock scoped_lock(m_data_lock);
                    {
                        std::string status  = msg.get_data<std::string>();
                        set_state(Tango::FAULT);
                        set_status(status);
                        if(m_controller != 0)
                        {
                            m_controller->stop_acquisition();
                        }
                    }
                }
                catch (Tango::DevFailed &df)
                {
                    ERROR_STREAM << df << endl;
                    set_state(Tango::FAULT);
                }
            }
            break;
            
            default:
            {
                YAT_ERROR << "DataStore::process_message::unhandled msg type received!" << std::endl;
            }
            break;

        }
    }
    catch (yat::Exception& ex)
    {
        ex.dump();
        std::stringstream error_msg("");
        error_msg << "Origin\t: " << ex.errors[0].origin << endl;
        error_msg << "Desc\t: " << ex.errors[0].desc << endl;
        error_msg << "Reason\t: " << ex.errors[0].reason << endl;
        set_state(Tango::FAULT);
        YAT_ERROR << "Exception from - DataStore::process_message() : " << error_msg.str() << endl;
        
        //- rethrow exception
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                    std::string(error_msg.str()).c_str(),
                                    "DataStore::process_message()");
    }
} 


//Inform controller (observer) about new datas to update_data on (stream & view)
void DataStore::notify_data(int ichannel, int current_frame)
{
    yat::MutexLock scoped_lock(m_data_lock);
    m_last_acquired_frame = current_frame;
    if(m_controller != 0)
    {
        m_controller->update_data(ichannel, current_frame);
    }
}

double DataStore::get_realtime(int frame, int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double real_time = 0.0;

    real_time = m_datas.m_frame_data[frame].m_channel_data[channel].m_real_time;
    return real_time;
}

double DataStore::get_deadtime(int frame, int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double dead_time = 0.0;

    dead_time = m_datas.m_frame_data[frame].m_channel_data[channel].m_dead_time;
    return dead_time;
}

double DataStore::get_output_count_rate(int frame, int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double output_count_rate = 0.0;

    output_count_rate = m_datas.m_frame_data[frame].m_channel_data[channel].m_output_count_rate;
    return output_count_rate;
}

double DataStore::get_input_count_rate(int frame, int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double intput_count_rate = 0.0;

    intput_count_rate = m_datas.m_frame_data[frame].m_channel_data[channel].m_input_count_rate;
    return intput_count_rate;
}

unsigned long DataStore::get_events_in_run(int frame, int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    unsigned long events_in_run = 0;

    events_in_run = m_datas.m_frame_data[frame].m_channel_data[channel].m_events_in_run;
    return events_in_run;
}

int DataStore::get_current_frame() const
{
    return m_controller->get_current_frame();
}

int DataStore::get_nb_frames() const
{
    return m_nb_frames;
}

void DataStore::close_data()
{
    YAT_INFO << "DataStore::close_data()" << std::endl;
    post(DATASTORE_CLOSE_DATA_MSG);    
}

void DataStore::update_rois_data(int current_frame, int roi_channel)
{
    YAT_VERBOSE << "DataStore::update_rois_data()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);
    for(int num_roi = 0; num_roi < m_rois_data[roi_channel].size() ; num_roi++ )
    {    
        m_rois_data[roi_channel][num_roi].roi_sum = 0;    
        std::vector<uint32_t> spectrum = get_frame_spectrum_data(current_frame, roi_channel);
        for(int i = m_rois_data[roi_channel][num_roi].begin_roi ; i <= m_rois_data[roi_channel][num_roi].end_roi; ++i)
        {
            if(i < spectrum.size())
            {
                m_rois_data[roi_channel][num_roi].roi_sum += static_cast<uint32_t>(spectrum.at(i));
            }
            else
            {
                m_rois_data[roi_channel][num_roi].roi_sum += 0;
            }
        }
    }
}

void DataStore::set_rois_data(int roi_channel, std::vector< std::vector<RoisData> >& rois_data)
{
    YAT_VERBOSE << "DataStore::set_rois_data(roi_channel = " << roi_channel << ")" << std::endl;
    std::vector<uint32_t> spectrum = get_frame_spectrum_data(m_last_acquired_frame, roi_channel);

    std::vector< std::vector<RoisData> >::iterator it = rois_data.begin();
    for(int i = (*it).back().begin_roi; i <= (*it).back().end_roi; ++i)
    {
        if(i < spectrum.size())
        {
            (*it).back().roi_sum += static_cast<uint32_t>(spectrum.at(i));
        }
        else
        {
            (*it).back().roi_sum += 0;
        }
    }

    m_rois_data[roi_channel].push_back( (*it).back() );
}

void DataStore::remove_rois_data(int roi_channel)
{
    YAT_VERBOSE << "DataStore::remove_rois_data(" << roi_channel << ")" << std::endl;
    if(-1 == roi_channel)
    {
        for(int i = 0 ; i< m_rois_data.size() ; i++)
        {
           m_rois_data[i].clear();
           m_rois_data[i].resize(0);
        }
    }
    else
    {
        m_rois_data[roi_channel].clear();
        m_rois_data[roi_channel].resize(0);
    }
}

std::vector< std::vector<RoisData> > DataStore::get_rois_data() const
{
	return m_rois_data;
}

void DataStore::reset_rois()
{
    m_rois_data.clear();
    m_rois_data.resize(m_nb_channels);
}

}
