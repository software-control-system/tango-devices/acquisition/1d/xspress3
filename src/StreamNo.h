/*************************************************************************
/*! 
 * \file StreamNo.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class StreamNo
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef STREAM_NO_H
#define STREAM_NO_H

#include "Stream.h"

namespace Xspress3_ns
{
//--------------------------------------------------------------------------------
//! \class StreamNo
//! \brief specialisation class to manage the NO storage
//--------------------------------------------------------------------------------
class StreamNo : public Stream
{
public:
//! \brief Constructor
StreamNo(Tango::DeviceImpl *dev);
//! \brief Destructor 
~StreamNo();

void init(yat::SharedPtr<DataStore> data_store);
void open();
void close();
void abort();
void update_data(int currentFrame, int ichannel, yat::SharedPtr<DataStore> data_store);
void reset_index();
                            

};
}

#endif // STREAM_NO_H
