/*************************************************************************
/*! 
 * \file Driver.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief Class Driver
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef DRIVER_H
#define DRIVER_H

#include <tango.h>
#include <yat/utils/Logging.h>
#include <vector>

const int NUMERIC_NB_BINS_VALUE = 4096;

enum TriggerMode
{
    INTERNAL = 0,
    GATE = 1
};

enum StatisticsAttribute
{
    STAT_REALTIME = 0,
    STAT_DEADTIME,
    STAT_INPUT_COUNT_RATE,
    STAT_OUTPUT_COUNT_RATE,
    STAT_EVENTS_IN_RUN,
	STAT_DEAD_TIME_CORRECTION_FACTOR
};

namespace Xspress3_ns
{
//--------------------------------------------------------------------------------
//! \class Driver
//! \brief Base class to control an Xspress3 module
//--------------------------------------------------------------------------------
class Driver : public Tango::LogAdapter
{

public:

//! \brief constructor
Driver(Tango::DeviceImpl* dev)
    : Tango::LogAdapter(dev),
    m_device(dev)
{
    YAT_INFO << "Driver::Driver()" << endl;
}

//! \brief destructor 
virtual ~Driver()
{
    YAT_INFO << "Driver::~Driver()" << endl;
}

//! \brief Get numbre of xspress3 cards
virtual int get_nb_cards() const = 0;

//! \brief Get numbre of xspress3 channels
virtual int get_nb_channels() const = 0;

//! \brief Get xspress3 firmware revision
virtual std::string get_firmware_revision() const = 0;

//! \brief Gets type of xspress3 card
virtual std::string get_board_type() const = 0;

//! \brief Get current acquired frame number
virtual int get_current_frame() const = 0;

//! \brief Get frames number requested by user
virtual int get_nb_frames() const = 0;

//! \brief Set frames number requested by user
//! \param frames Frame number requested
virtual void set_nb_frames(int frames) = 0;

//! \brief Get bins number for xspress3 channel
virtual int get_nb_bins() const = 0;

//! \brief Get number of acquisition channel scalars
virtual int get_nb_scalars() const = 0;

//! \brief Get number element in spectrum data
virtual ulong get_nb_elements() const = 0;

//! \brief Get requested trigger mode
virtual TriggerMode get_trigger_mode() const = 0;

//! \brief Set requested trigger mode
//! \param trigger_mode Requested trigger mode
virtual void set_trigger_mode(TriggerMode trigger_mode) = 0;

//! \brief Set bins number for xspress3 channel
//! \param frames Bins number for xspress3 channel
virtual void set_nb_bins(int bins) = 0;

//! \brief Set bins number of channel scalars
//! \param scalars scalars number for xspress3 channel
virtual void set_nb_scalars(int scalars) = 0;

//! \brief Get the requested exposure time per frame
virtual double get_exposure_time() const = 0;

//! \brief Set the requested exposure time per frame
//! \param exposure_time The exposure time value
virtual void set_exposure_time(double exposure_time) = 0;

//! \brief Start acquisition action
virtual void start_acquisition() = 0;

//! \brief Stop acquisition action
virtual void stop_acquisition() = 0;

//! \brief Get current device state
virtual Tango::DevState get_state() = 0;

//! \brief Get current device status
virtual std::string get_status() = 0;    

//! \brief Set current device state
virtual void set_state(Tango::DevState state) = 0;


//! \brief Get is stop acquisition is requested
virtual bool is_abort() const = 0;

//! \brief Set is stop acquisition is requested
//! \param abort Boolean to send stop acquisition request
virtual void abort(bool abort) = 0;

//! \brief Chek is acquisition is running
virtual bool is_running() const = 0;

//! \brief Get spectrum data
virtual std::vector<Tango::DevULong> get_spectrum_data(int current_frame, int channel) = 0;

//! \brief Get statistics data
virtual void get_statistics_data(int frame, int channel, const char* name,  void* value) = 0;

protected:

//! \brief m_device: Owner device server
Tango::DeviceImpl* m_device; 

};

}

#endif // DRIVER_H
