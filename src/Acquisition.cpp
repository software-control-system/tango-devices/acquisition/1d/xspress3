/*************************************************************************/
/*! 
 *  \file   Acquisition.cpp
 *  \brief  Implementation of Acquisition methods.
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include "Acquisition.h"
#include "Xspress3Handler.h"
#include "Simulator.h"
#include <tango.h>
#include <yat/utils/Logging.h>

namespace Xspress3_ns
{
// Constructor
Acquisition::Acquisition(Tango::DeviceImpl* dev, std::vector<std::string> vec_properties, yat::SharedPtr<DataStore> store)
 : yat4tango::DeviceTask(dev),
    m_store(store),
    m_device(dev),
    m_is_need_collecting_last_data(false),
    m_nb_frame_collected(0)
{
    YAT_INFO << "Acquisition::Acquisition()" << std::endl;
    enable_timeout_msg(false);
    enable_periodic_msg(false);
    set_periodic_msg_period(10);

    if(vec_properties.size() > 0)
    {
        std::vector<std::string> board_type_params;
        yat::StringUtil::split(vec_properties.at(0), ';', &board_type_params, true);

        if(BOARD_TYPE_XSPRESS3_TYPE == board_type_params.at(0))
        {
            m_driver.reset(new Xspress3Handler(dev, vec_properties));
        }
        else if(BOARD_TYPE_SIMULATOR_TYPE == board_type_params.at(0))
        {
            m_driver.reset(new Simulator(dev, vec_properties));
        }
        else{/*Nothing*/}
    }
    
    int nb_frames = get_nb_frames();
    int nb_channel = get_nb_channels();
    int nb_bins = get_nb_bins();
  
    m_store->init(nb_channel, nb_frames, nb_bins);

    set_status("");
    set_state(Tango::STANDBY);
}

// Destructor
Acquisition::~Acquisition()
{
    YAT_INFO << "Acquisition::~Acquisition()" << std::endl;
    m_driver.reset();
}

// Set device state
void Acquisition::set_state(Tango::DevState state)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_state = state;
}

// Set device status
void Acquisition::set_status(const std::string& status)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str();
}

bool Acquisition::is_running()
{
    return m_driver->is_running();
}

void Acquisition::enable_collecting_last_data()
{
    YAT_VERBOSE << "Acquisition::enable_collecting_last_data()" << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);
    m_is_need_collecting_last_data = true;
}

void Acquisition::disable_collecting_last_data()
{
    yat::MutexLock scoped_lock(m_data_lock);
    m_is_need_collecting_last_data = false;
}

bool Acquisition::is_need_collecting_last_data()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_is_need_collecting_last_data;
}

// Acquisition::process_message
void Acquisition::process_message(yat::Message& msg)
{
    try
    {
        switch (msg.type())
        {
            case yat::TASK_INIT:
            {                
                YAT_VERBOSE << "Acquisition::process_message::TASK_INIT" << std::endl;
                set_state(Tango::STANDBY);
            }
            break;

            case yat::TASK_EXIT:
            {
                YAT_VERBOSE << "Acquisition::process_message::TASK_EXIT" << std::endl;
            }
            break;
            
            case yat::TASK_TIMEOUT:
            {
                YAT_ERROR << "Acquisition::process_message::TASK_TIMEOUT" << std::endl;
            }
            break;

            case yat::TASK_PERIODIC:
            {
                    if(is_running())
                    {
                        if(!is_need_collecting_last_data()) 
                        {
                            //Enable collecting the final data at the end of acquisition
                            enable_collecting_last_data();
                        }	
                        //Get statistics and spectrum data values from driver and store them
                        collect_data();
                    }
                    // Stop command launched, we need to collect the last data
                    else
                    {
                        // Collect the last data
                        if(is_need_collecting_last_data())                
                        {
                            YAT_VERBOSE << "Acquisition::process_message() - Collecting final data needed " << std::endl;
                            //Collecting the final data
                            do
                            {
                                collect_data();
                            }
							while(m_nb_frame_collected < get_current_frame());

                            m_store->close_data();
                            //Disable collecting the final data at the end of acquisition
                            disable_collecting_last_data();
                        }
                    }
            }
            break;

            case kMSG_STOP_CAPTURE:
            {
                YAT_VERBOSE << "Acquisition::process_message::kMSG_STOP_CAPTURE" << std::endl;
                m_driver->abort(true);
                m_driver->stop_acquisition();
                m_store->close_data();
            }
            break;

            default:
            {
                YAT_ERROR << "Acquisition::process_message::unhandled msg type received!" << std::endl;
            }
            break;
        }//switch
    }//try
    catch (yat::Exception& ex)
    {
        ex.dump();
        std::stringstream error_msg("");
        error_msg << "Origin\t: " << ex.errors[0].origin << endl;
        error_msg << "Desc\t: " << ex.errors[0].desc << endl;
        error_msg << "Reason\t: " << ex.errors[0].reason << endl;
        YAT_ERROR << "Exception from - Acquisition::process_message() : " << error_msg.str() << endl;
        stop_acquisition();
        set_state(Tango::FAULT);
        set_status(error_msg.str());
    }
}

// Numbre of cards
int Acquisition::get_nb_cards() const
{
    return m_driver->get_nb_cards();
}
    
// Numbre of available channels
int Acquisition::get_nb_channels() const
{
    return m_driver->get_nb_channels();
}

// Firmware revision
std::string Acquisition::get_firmware_revision() const
{
    return m_driver->get_firmware_revision();
}

std::string Acquisition::get_board_type() const
{
    return m_driver->get_board_type();
}

int Acquisition::get_current_frame() const
{
    return m_driver->get_current_frame();
}

int Acquisition::get_nb_frames() const
{
    return m_driver->get_nb_frames();
}

void Acquisition::set_nb_frames(int frames)
{
    m_driver->set_nb_frames(frames);
}

TriggerMode Acquisition::get_trigger_mode() const
{
    return m_driver->get_trigger_mode();
}

void Acquisition::set_trigger_mode(TriggerMode trigger_mode)
{
    m_driver->set_trigger_mode(trigger_mode); 
}
   
double Acquisition::get_exposure_time() const
{
    return m_driver->get_exposure_time();
}

void Acquisition::set_exposure_time(double exposure_time)
{
    m_driver->set_exposure_time(exposure_time);
}

void Acquisition::start_acquisition()
{
    YAT_INFO << "Acquisition::start_acquisition()" << endl;
    try
    {
        m_nb_frame_collected = 0;
        set_status("");
        m_driver->abort(false);
        enable_periodic_msg(true);   
        m_driver->start_acquisition();
    }
    catch (Tango::DevFailed& df)
    {
        set_state(Tango::FAULT);
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << endl;
        status << "Desc\t: " << df.errors[0].desc << endl;
        set_status(status.str());
        YAT_ERROR << "Exception from - Acquisition::start_acquisition(): " << status.str() << endl;
    }
}

void Acquisition::stop_acquisition()
{
    YAT_INFO << "Acquisition::stop_acquisition()" << std::endl;
    try
    {
        yat::Message* msg = yat::Message::allocate(kMSG_STOP_CAPTURE, HIGHEST_MSG_PRIORITY, false);
        post(msg);
    }
    catch (Tango::DevFailed& df)
    {
        set_state(Tango::FAULT);
        std::stringstream status;
        status.str("");
        status << "Origin\t: " << df.errors[0].origin << endl;
        status << "Desc\t: " << df.errors[0].desc << endl;
        set_status(status.str());
        YAT_ERROR << "Exception from - Acquisition::stop_acquisition(): " << status.str() << endl;
    }
}

Tango::DevState Acquisition::get_state()
{
    yat::MutexLock scoped_lock(m_state_lock);
    switch(m_driver->get_state())
    {
        case Tango::STANDBY:
            if(m_state!=Tango::ALARM && m_state!=Tango::FAULT)
                set_state(Tango::STANDBY);
            break;

        case Tango::RUNNING:
            if(m_state!=Tango::ALARM && m_state!=Tango::FAULT)
                set_state(Tango::RUNNING);
            break;

        case Tango::FAULT:
            set_state(Tango::FAULT);
            set_status(m_driver->get_status());
            break;
        
        default:
            YAT_VERBOSE <<"Acquisition::get_state():UNKNOWN State!"<< std::endl;
            break;
    }

    return m_state;
}

std::string Acquisition::get_status()
{
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

ulong Acquisition::get_nb_elements() const
{
    return m_driver->get_nb_elements();
}

ulong Acquisition::get_nb_bins() const
{
    return m_driver->get_nb_bins();
}

ulong Acquisition::get_nb_scalars() const
{
    return m_driver->get_nb_scalars();
}

void Acquisition::collect_data()
{
    int nb_frames = get_nb_frames();
    int current_frame = get_current_frame();

    if(current_frame > m_nb_frame_collected)
    {
        int nb_channels = get_nb_channels();
        for(int ichannel = 0; ichannel < nb_channels ; ichannel++)
        {

            FrameData frame_data;
            m_driver->get_statistics_data(m_nb_frame_collected, ichannel, "realTime", &frame_data.m_real_time);
            m_driver->get_statistics_data(m_nb_frame_collected, ichannel, "deadTime", &frame_data.m_dead_time);
            m_driver->get_statistics_data(m_nb_frame_collected, ichannel, "inputCountRate", &frame_data.m_input_count_rate);
            m_driver->get_statistics_data(m_nb_frame_collected, ichannel, "outputCountRate", &frame_data.m_output_count_rate);
            m_driver->get_statistics_data(m_nb_frame_collected, ichannel, "eventsInRun", &frame_data.m_events_in_run);
            m_driver->get_statistics_data(m_nb_frame_collected, ichannel, "deadTimeCorrectionFactor", &frame_data.m_deadtime_correction_factor);

            //push statisctics into DataStore
            m_store->store_statistics_data(m_nb_frame_collected, ichannel, frame_data);

            std::vector<uint32_t> spectrum = m_driver->get_spectrum_data(m_nb_frame_collected, ichannel);

            //Push channel data into DataStore
            m_store->store_spectrum_data(m_nb_frame_collected, ichannel, (uint32_t*) & spectrum[0],spectrum.size());
        }
        
        YAT_VERBOSE << " Acquisition::collect_data() - Collect Frame " << m_nb_frame_collected << " done. " << std::endl;
        m_nb_frame_collected++;
    }
}

}
