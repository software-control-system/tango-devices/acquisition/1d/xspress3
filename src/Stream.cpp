/*************************************************************************/
/*! 
 *  \file   Stream.cpp
 *  \brief  Class used to manage the stream of acquisition data.
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include "Stream.h"
#include <yat/utils/Logging.h>

namespace Xspress3_ns
{
// constructor
Stream::Stream(Tango::DeviceImpl *dev)
: Tango::LogAdapter(dev),
m_is_channel_enabled(false),
m_is_realtime_enabled(false),
m_is_deadtime_enabled(false),
m_is_icr_enabled(false),
m_is_ocr_enabled(false),
m_is_events_in_run_enabled(false)
{
    YAT_INFO << "Stream::Stream() " << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
Stream::~Stream()
{
    YAT_INFO << "Stream::~Stream()" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
const std::vector<std::string>& Stream::get_stream_items()
{
    return m_stream_items;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void Stream::set_stream_items(const std::vector<std::string>& stream_items)
{
    YAT_VERBOSE << "Stream::set_stream_items( nb_items = " << stream_items.size()<<" )"<< std::endl;    
    //- update m_stream_items
	m_stream_items = stream_items;

    //- store which data must be stored
    if ( std::find(m_stream_items.begin(), m_stream_items.end(), "channel") != m_stream_items.end() )
    {
        m_is_channel_enabled = true;
    }
    if ( std::find(m_stream_items.begin(), m_stream_items.end(), "realtime") != m_stream_items.end() )
    {
        m_is_realtime_enabled = true;
    }
    if ( std::find(m_stream_items.begin(), m_stream_items.end(), "deadtime") != m_stream_items.end() )
    {
        m_is_deadtime_enabled = true;
    }
    if ( std::find(m_stream_items.begin(), m_stream_items.end(), "icr") != m_stream_items.end() )
    {
        m_is_icr_enabled = true;
    }
    if ( std::find(m_stream_items.begin(), m_stream_items.end(), "ocr") != m_stream_items.end() )
    {
        m_is_ocr_enabled = true;
    }
    if ( std::find(m_stream_items.begin(), m_stream_items.end(), "eventsinrun") != m_stream_items.end() )
    {
        m_is_events_in_run_enabled = true;
    }
}

}