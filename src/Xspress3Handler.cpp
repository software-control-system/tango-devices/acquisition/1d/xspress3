/*************************************************************************/
/*! 
 *  \file   Xspress3Handler.cpp
 *  \brief  Implementation of Xspress3Handler methods.
 *	\author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 */
/*************************************************************************/

#include "Xspress3Handler.h"
#include <yat/utils/Logging.h>
#include <yat/threading/Thread.h>
#include <yat/threading/Utilities.h>
#include <yat/time/Timer.h>
#include <tango.h>
// SDK
#include <xspress3.h>
#include <chrono>

namespace Xspress3_ns
{
// constructor
Xspress3Handler::Xspress3Handler(Tango::DeviceImpl* dev, std::vector<std::string> vec_properties): 
 Driver(dev),
 m_nb_cards(2),
 m_nb_channels(4), 
 m_max_frames(16384),
 m_base_ip_address("192.168.0.1"),
 m_base_port(30123), 
 m_base_mac_address("02.00.00.00.00.00"),
 m_debug(0),
 m_nb_frames(1),
 m_nb_acq_frames(0),
 m_exp_time(2.0),
 m_nb_bins(NUMERIC_NB_BINS_VALUE),
 m_nb_scalars(11),
 m_nb_scalars_without_deadtime_factors(9),
 m_vec_properties(vec_properties),
 m_is_abort(false),
 m_is_running(false),
 m_trigger_mode(TriggerMode::INTERNAL),
 m_config_directory_name(""),
 m_expert_acquistion_polling_period (50)
{
    YAT_INFO << "Xspress3Handler::Xspress3Handler()" << std::endl;
    m_card = -1;
    
    YAT_VERBOSE << "Xspress3Handler: Number of properties =" << m_vec_properties.size()  << endl;
    if(m_vec_properties.size() == 10)
    {
        std::istringstream (m_vec_properties.at(1)) >> m_nb_cards;
        std::istringstream (m_vec_properties.at(2)) >> m_nb_channels;
        std::istringstream (m_vec_properties.at(3)) >> m_max_frames;
        m_base_ip_address = m_vec_properties.at(4);
        std::istringstream (m_vec_properties.at(5)) >> m_base_port;
        m_base_mac_address = m_vec_properties.at(6);
        std::istringstream (m_vec_properties.at(7)) >> m_debug;
        m_config_directory_name = m_vec_properties.at(8);
        std::istringstream (m_vec_properties.at(9)) >> m_expert_acquistion_polling_period ;
    }
    else
    {
       YAT_ERROR << "Xspress3Handler::Xspress3Handler() Uncorrect number of properties " <<endl;
       set_state(Tango::FAULT);
       Tango::Except::throw_exception ("CONFIG_ERROR",
                                        "Incorrect number of properties",
                                        "Xspress3Handler::Xspress3Handler()");
    }
    
    m_nb_bins = m_max_frames/m_nb_channels;
    YAT_VERBOSE << "Xspress3Handler::Xspress3Handler() - Number of bins per channel: " << m_nb_bins << std::endl;

    if (m_statistics_attribute_map.empty() )
    {
        m_statistics_attribute_map["realTime"] = STAT_REALTIME;
        m_statistics_attribute_map["deadTime"] = STAT_DEADTIME;
        m_statistics_attribute_map["inputCountRate"] = STAT_INPUT_COUNT_RATE;
        m_statistics_attribute_map["outputCountRate"] = STAT_OUTPUT_COUNT_RATE;
        m_statistics_attribute_map["eventsInRun"] = STAT_EVENTS_IN_RUN;
        m_statistics_attribute_map["deadTimeCorrectionFactor"] = STAT_DEAD_TIME_CORRECTION_FACTOR;
    }

	{
		yat::MutexLock scoped_lock(m_data_locker);
	    m_data.reserve(m_nb_channels);
	    m_data.resize(m_nb_channels);    
	}

    m_vec_deadtime_parameters.reserve(m_nb_channels);
    m_vec_deadtime_parameters.resize(m_nb_channels);  
    
    init();
    
    set_state(Tango::STANDBY);
}
                        
// Destructor 
Xspress3Handler::~Xspress3Handler()
{
    YAT_INFO << "Xspress3Handler::~Xspress3Handler()" << endl;

    if (xsp3_close(m_handle) < 0)
    {
        YAT_ERROR << "ERROR calling xsp3_close. Error message: " << xsp3_get_error_message() <<endl;
        Tango::Except::throw_exception ("CONFIG_ERROR",
                                        static_cast<const char*>(xsp3_get_error_message()),
                                        "Xspress3Handler::~Xspress3Handler()");
    }

	{
		yat::MutexLock scoped_lock(m_data_locker);
	    if( !m_data.empty() )
	    {
	        m_data.clear();
	    }
	}
	
    if( !m_vec_deadtime_parameters.empty() )
    {
        m_vec_deadtime_parameters.clear();
    }

    m_vec_properties.clear();
}

// Connects to the Xspress3
void Xspress3Handler::init()
{    
    YAT_INFO << "Xspress3Handler::init()" << std::endl;
    if ((m_handle = xsp3_config(m_nb_cards, m_max_frames, (char*)m_base_ip_address.c_str(), 
        m_base_port, (char*)m_base_mac_address.c_str(), m_nb_channels,
        0, "NULL", 0, 0)) < 0) 
    {
        YAT_ERROR << "ERROR calling xsp3_config. Error message: " << xsp3_get_error_message() <<endl;
        set_state(Tango::FAULT);
        Tango::Except::throw_exception ("CONFIG_ERROR",
                                        static_cast<const char*>(xsp3_get_error_message()),
                                        "Xspress3Handler::init()");
    }

    init_roi(-1);
    set_card(0);

    int gen = xsp3_get_generation(m_handle, m_card);
    for (int i=0; i<m_nb_cards; i++) 
    {
        set_card(i);
        setup_clocks(gen == 3 ? (m_nb_cards > 1 ? XSP4_CLK_SRC_MIDPLN_LMK61E2 : XSP3M_CLK_SRC_LMK61E2): 
            (gen == 2 ? XSP3M_CLK_SRC_CDCM61004 : XSP3_CLK_SRC_XTAL), 
            XSP3_CLK_FLAGS_MASTER | XSP3_CLK_FLAGS_NO_DITHER, 
            0);
    }
    
    set_card(-1);

    if (m_config_directory_name != "") 
    {
        restore_settings();
    }

    set_run_mode();
    set_state(Tango::STANDBY);
}

// Select the currently active card
void Xspress3Handler::set_card(int card) 
{
	yat::AutoMutex<yat::Mutex> amutex(m_locker);
    m_card = card;
}

// Numbre of xspress3 cards
int Xspress3Handler::get_nb_cards() const
{
    return m_nb_cards;
}

// Numbre of simulated xspress3 channels
int Xspress3Handler::get_nb_channels() const
{
    return m_nb_channels;
}

// Xspress3 firmware revision
std::string Xspress3Handler::get_firmware_revision() const
{
    int revision;
    if ((revision = xsp3_get_revision(m_handle)) < 0)
    {
        YAT_ERROR << "ERROR calling xsp3_get_revision. Error message: " << xsp3_get_error_message() <<endl;
        Tango::Except::throw_exception ("CONFIG_ERROR",
                                        static_cast<const char*>(xsp3_get_error_message()),
                                        "Xspress3Handler::get_firmware_revision()");
    }

    stringstream ss;
    int major = (revision >> 12) & 0xfff;
    int minor = revision & 0xfff;
    ss << major << "." << minor;	

    return ss.str();
}

std::string Xspress3Handler::get_board_type() const
{
    return m_vec_properties.at(0);
}

int Xspress3Handler::get_current_frame() const
{
    return m_nb_acq_frames;
}

int Xspress3Handler::get_nb_frames() const
{
    return m_nb_frames;
}

void Xspress3Handler::set_nb_frames(int frames)
{
    yat::AutoMutex<yat::Mutex> amutex(m_locker);
    if (frames < 0) 
    {
        YAT_ERROR << "Number of frames to acquire has not been set";
    }
    m_nb_frames = frames;
    set_timing_mode();
}

int Xspress3Handler::get_nb_bins() const
{
    return m_nb_bins;
}

void Xspress3Handler::set_nb_bins(int bins)
{
    m_nb_bins = bins;
}

int Xspress3Handler::get_nb_scalars() const
{
    return m_nb_scalars;
}

void Xspress3Handler::set_nb_scalars(int scalars)
{
    m_nb_scalars = scalars;
}

TriggerMode Xspress3Handler::get_trigger_mode() const
{
    return m_trigger_mode;
}

void Xspress3Handler::set_trigger_mode(TriggerMode trigger_mode)
{
    m_trigger_mode = trigger_mode;
    set_timing_mode();
}

double Xspress3Handler::get_exposure_time() const
{
    return m_exp_time;
}

void Xspress3Handler::set_exposure_time(double exposure_time)
{
    m_exp_time = exposure_time;
    set_timing_mode();
}

void Xspress3Handler::start_acquisition()
{
    YAT_INFO << "Xspress3Handler::start_acquisition()" << endl;
    yat::MutexLock scoped_lock(m_locker);

    set_state(Tango::RUNNING);
    m_nb_acq_frames = 0;
    m_is_running = true;

	{
		yat::MutexLock scoped_lock(m_data_locker);
	    m_data.clear();
	    m_data.resize(m_nb_channels);
	}
	
    if (xsp3_histogram_clear(m_handle, 0, m_nb_channels, 0, m_nb_frames) < 0)
    {
        YAT_ERROR << "ERROR calling xsp3_histogram_clear Error message: " << xsp3_get_error_message();
    }

    struct timespec start, end;
    clock_gettime(CLOCK_REALTIME, &start);
    std::chrono::high_resolution_clock::time_point acquisition_start = std::chrono::high_resolution_clock::now();

    if (xsp3_histogram_start(m_handle, -1) < 0)
    {
        YAT_ERROR << "ERROR calling xsp3_itfg_start. Error message: " << xsp3_get_error_message() << endl;
    }

    {
		yat::Timer t_global0;	
        while ((m_nb_acq_frames < m_nb_frames) && is_running() && !is_abort())
        {
            if (TriggerMode::INTERNAL ==  m_trigger_mode) 
            {
                YAT_VERBOSE << "TriggerMode is INTERNAL" << std::endl;
				//wait an exposure time ...
                yat::Timer t0;				
				Tango::DevULong sec = (unsigned long)(m_exp_time);
				Tango::DevULong msec = ((m_exp_time - sec)*1000);					
				yat::ThreadingUtilities::sleep(sec, msec*1000000); // sec,nanosec;
                YAT_VERBOSE << "--------------------------------------------" << std::endl;
                YAT_VERBOSE << "Simulated Exposure Time elapsed: " << t0.elapsed_msec() <<" msec" << endl;
                YAT_VERBOSE << "--------------------------------------------" << std::endl;
               
                if((m_nb_acq_frames < m_nb_frames -1 ))
                {
                    pause();
                    restart();
                }

                int completed_frame;
                int temp_curr_frame = m_nb_acq_frames + 1;
                struct timespec delay, remain;
                delay.tv_sec = 0;
                delay.tv_nsec = (int)(1E9*0.5);
                
                yat::Timer t1;
                
                do
                {
                    check_progress(completed_frame);
                    nanosleep(&delay, &remain);
                }
				while(completed_frame < temp_curr_frame);
                            
                clock_gettime(CLOCK_REALTIME, &end);
                double elapsed;
                elapsed = (end.tv_sec - start.tv_sec) * 1e9;
                elapsed = (elapsed + (end.tv_nsec - start.tv_nsec)) * 1e-9;

                read_frame(m_nb_acq_frames);
                ++m_nb_acq_frames;
				
				if(m_nb_acq_frames+1 == m_nb_frames)
				{
					YAT_INFO << "--------------------------------------------" << std::endl;
					YAT_INFO << "Number of acquired frames = " << m_nb_acq_frames + 1 << std::endl;
					YAT_INFO << "ExposureTime value per frame = " << m_exp_time <<" seconds" << std::endl;
					YAT_INFO << "The acquisition lasted :" << t_global0.elapsed_msec() << setprecision(5) <<" msec " << std::endl;;
					YAT_INFO << "--------------------------------------------" << std::endl;			
				}				
            }
            else if(TriggerMode::GATE ==  m_trigger_mode)
            {
				YAT_VERBOSE << "TriggerMode is GATE" << std::endl;
                int completed_frame;
                struct timespec delay, remain;
                delay.tv_sec = 0;
                delay.tv_nsec = (int)(m_expert_acquistion_polling_period  * 1000000);
                YAT_VERBOSE << " m_nb_acq_frames "<< m_nb_acq_frames <<std::endl;
                int temp_curr_frame = m_nb_acq_frames;
                
                std::chrono::high_resolution_clock::time_point check_progress_start = std::chrono::high_resolution_clock::now();
                
                do 
                {
                    check_progress(completed_frame);
                    nanosleep(&delay, &remain);
                    
                    if (is_abort()) 
                    {
                        stop();
                        break;
                    }
                }
				while (completed_frame <= m_nb_acq_frames);

				YAT_VERBOSE << "Check_progress() - completed_frame = " << completed_frame <<  " - temp_curr_frame = " <<  temp_curr_frame << std::endl;
                std::chrono::high_resolution_clock::time_point check_progress_end = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double, std::milli> check_progress_duration = check_progress_end - check_progress_start;

                //YAT_VERBOSE << "check_progress() loop elapsed for: " << check_progress_duration.count() << " ms" << std::endl;
                do
                {
                    read_frame(temp_curr_frame);
                    temp_curr_frame++;
                }
				while(temp_curr_frame < completed_frame);

                m_nb_acq_frames = (completed_frame > m_nb_frames) ? m_nb_frames : completed_frame;
                YAT_VERBOSE << " completed_frame " << completed_frame  <<std::endl;
                YAT_VERBOSE << " m_nb_frames "     << m_nb_frames      <<std::endl;
                YAT_VERBOSE << " m_nb_acq_frames " << m_nb_acq_frames  <<std::endl;

                std::chrono::high_resolution_clock::time_point acquisition_end = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double, std::milli> acquisition_duration = acquisition_end - acquisition_start;
                //YAT_VERBOSE << " The acquisition of "<< m_nb_acq_frames <<" frame(s) lasted :" << acquisition_duration.count()  <<" ms " << std::endl;
            }
        }
    }

    set_state(Tango::STANDBY);
    m_is_running = false;
}

void Xspress3Handler::stop_acquisition()
{
    YAT_INFO << "Xspress3Handler::stop_acquisition()" << endl;
    yat::MutexLock scoped_lock(m_locker);
    
    if (xsp3_histogram_stop(m_handle, m_card) < 0)
    {
        YAT_ERROR << "Acquisition can't stoped. Error message: " << xsp3_get_error_message() << endl;
    }

    int idle_count = 0;
    while (idle_count < 2) 
    {
        struct timespec delay, remain;
        delay.tv_sec = 0;
        delay.tv_nsec = (int)(1E9*(0.01)); // sleep for 10msec
        nanosleep(&delay, &remain);
        if (xsp3_histogram_is_any_busy(m_handle) == 0) 
        {
            ++idle_count;
        }
    }
}

Tango::DevState Xspress3Handler::get_state() 
{
    yat::AutoMutex<yat::Mutex> amutex(m_state_locker);
    return m_state;
}

void Xspress3Handler::set_state(Tango::DevState state)
{
    yat::AutoMutex<yat::Mutex> amutex(m_state_locker);
    m_state = state;
}

std::string Xspress3Handler::get_status()
{
    yat::AutoMutex<yat::Mutex> amutex(m_state_locker);
    return (m_status.str());
}

void Xspress3Handler::set_status(const std::string& status)
{
    yat::MutexLock scoped_lock(m_state_locker);
    m_status.str("");
    m_status << status.c_str();
}

/**
 * Check which frames have been acquired
 *
 * @param[out] frame_num the nos of frames of data acquired.
 */
void Xspress3Handler::check_progress(int& frame_num) 
{
    //YAT_VERBOSE << "Xspress3Handler::check_progress()" << endl;
    
    int fn = xsp3_scaler_check_progress(m_handle);
    //YAT_VERBOSE << "Xspress3Handler::check_progress() - Acquired frame is fn = " << fn << std::endl;

    if (fn < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }

    frame_num = fn;
}

/**
 * Read a frame of xspress3 data comprising histogram & scaler data (used by read thread only).
 * The scaler data is added to the end of each channel of histogram data
 * @verbatim
 * The scaler data block will comprise of the following values
 * scaler 0 - Time
 * scaler 1 - ResetTicks
 * scaler 2 - ResetCount
 * scaler 3 - AllEvent
 * scaler 4 - AllGood
 * scaler 5 - InWIn 0
 * scaler 6 - In Win 1
 * scaler 7 - PileUp
 * scaler 8 - TotalTicks
 * @endverbatim
 *
 * @param bptr a pointer to the buffer for the returned data
 * @param frame_nb is the time frame
 */
void Xspress3Handler::read_frame(int frame_nb)
{
    //YAT_VERBOSE << "Xspress3Handler::read_frame()" << endl;
	yat::MutexLock scoped_lock(m_locker);
	yat::MutexLock scoped_lock_data(m_data_locker);
    u_int32_t scaler_data[ m_nb_scalars_without_deadtime_factors * m_nb_channels];
    if (xsp3_scaler_read(m_handle, scaler_data, 0, 0, frame_nb, m_nb_scalars_without_deadtime_factors , m_nb_channels, 1) < 0)
    {
        YAT_ERROR << xsp3_get_error_message();
    }

    m_vec_deadtime_parameters.clear();
    m_vec_deadtime_parameters.resize(m_nb_channels);
    
    for (int chan = 0; chan < m_nb_channels; ++chan)
    {
    	m_buffer.reset(new u_int32_t[m_nb_bins + m_nb_scalars]);

        if (xsp3_histogram_read3d(m_handle, m_buffer.get(), 0, chan, frame_nb, m_nb_bins, 1, 1) < 0)
        {
            YAT_ERROR << xsp3_get_error_message();
        }

        for (int i = 0; i < m_nb_scalars_without_deadtime_factors ; ++i)
        {
            (m_buffer.get())[m_nb_bins + i ] = scaler_data[chan * m_nb_scalars_without_deadtime_factors + i];
        }

        Xspress3_TriggerB trig_b;
        xsp3_get_trigger_b(m_handle, chan, &trig_b);
        double evtwidth = (double)trig_b.event_time;
        double resets = scaler_data[chan * m_nb_scalars_without_deadtime_factors + 1];
        double allevt = scaler_data[chan * m_nb_scalars_without_deadtime_factors + 3];
        double ctime = scaler_data[chan * m_nb_scalars_without_deadtime_factors + 0];
        
        //YAT_VERBOSE << "current channel = " << chan << std::endl;
        //YAT_VERBOSE << "evtwidth = " << evtwidth << "- resets = " << resets << "- allevt = " << allevt << "ctime = " << ctime << std::endl;

	    (m_buffer.get())[ m_nb_bins + 9] = 100.0*(allevt*(evtwidth+1) + resets)/ctime;
	    (m_buffer.get())[ m_nb_bins + 10 ] = ctime/(ctime - (allevt*(evtwidth+1) + resets));        

        m_vec_deadtime_parameters[chan].push_back(evtwidth);
        m_vec_deadtime_parameters[chan].push_back(resets);
        m_vec_deadtime_parameters[chan].push_back(allevt);
        m_vec_deadtime_parameters[chan].push_back(ctime);

        m_data[chan] = m_buffer;
    }
}

ulong Xspress3Handler::get_nb_elements() const
{
    return (ulong)(m_nb_bins + m_nb_scalars);
}

bool Xspress3Handler::is_abort() const
{
    return m_is_abort;
}

std::vector<Tango::DevULong> Xspress3Handler::get_spectrum_data(int current_frame, int channel)
{
    //YAT_VERBOSE << "Xspress3Handler::get_spectrum_data() " << std::endl;
	yat::MutexLock scoped_lock(m_data_locker);	
    
    std::vector<Tango::DevULong> result;
	result.resize(m_nb_bins);

    u_int32_t* buffer = m_data[channel].get();
    for(int i = 0 ; i < m_nb_bins ; ++i )
    {
        result.at(i) = (Tango::DevULong) *(buffer+i);
    }
	
	return result;
}

void Xspress3Handler::get_statistics_data(int frame, int channel, const char* name, void* value)
{
    //YAT_VERBOSE << "Xspress3Handler::get_statistics_data - " << name << std::endl;
	yat::MutexLock scoped_lock(m_data_locker);
    u_int32_t* buffer = m_data[channel].get();

    switch(m_statistics_attribute_map[name])
    {
    case STAT_REALTIME:
        *(static_cast<double*> (value)) =(double)(buffer[m_nb_bins + 8]);
        break;

    case STAT_DEADTIME:
        *(static_cast<double*> (value))  = 100.0*(m_vec_deadtime_parameters[channel][2]*(m_vec_deadtime_parameters[channel][0]+1) + m_vec_deadtime_parameters[channel][1])/m_vec_deadtime_parameters[channel][3] ;
        break;

    case STAT_INPUT_COUNT_RATE:
        *(static_cast<double*> (value)) = (double)(buffer[m_nb_bins + 4] );
        break;

    case STAT_OUTPUT_COUNT_RATE:
        *(static_cast<double*> (value)) = (double)(buffer[m_nb_bins + 4]);
        break;

    case STAT_EVENTS_IN_RUN:
        *(static_cast<unsigned long*> (value)) = (unsigned long)(buffer[m_nb_bins + 3]);
        break;

    case STAT_DEAD_TIME_CORRECTION_FACTOR:
        *(static_cast<double*> (value)) = m_vec_deadtime_parameters[channel][3]/(m_vec_deadtime_parameters[channel][3] - (m_vec_deadtime_parameters[channel][2]*(m_vec_deadtime_parameters[channel][0]+1) + m_vec_deadtime_parameters[channel][1]));
        break;

    default:
        YAT_ERROR << "Simulator::get_statistics_data() - Unknown acquisition attribute name" << std::endl;
        break;
    }        
}

void Xspress3Handler::abort(bool abort)
{
    YAT_VERBOSE << "Xspress3Handler::abort()" << endl;
    yat::Mutex aLock;
    aLock.lock();
    m_is_abort = abort;
    aLock.unlock();
    YAT_VERBOSE << "Xspress3Handler::abort() Set abort to " << std::boolalpha << m_is_abort << endl;
}

bool Xspress3Handler::is_running() const
{
    return m_is_running;
}

void Xspress3Handler::set_timing_mode() 
{    
    if (!m_exp_time) return;
    if (!m_nb_frames) return;

    YAT_VERBOSE << "Xspress3Handler::set_timing_mode() " << m_trigger_mode << " - exp: " << m_exp_time << "- m_nb_frames: " << m_nb_frames << std::endl;

    int alt_ttl_mode = 0;
    int debounce = 80;

    if (TriggerMode::INTERNAL ==  m_trigger_mode) 
    {      
        set_card(0);
        set_timing(1, 0, alt_ttl_mode, debounce, false, false, false);

        for (int i=1;i<m_nb_cards; i++) 
        {
            set_card(i);
            set_timing(4, 0, alt_ttl_mode, 0, false, false, false);
        }

        set_card(-1);
        set_itfg_timing(m_nb_frames, 0, 3);
    } 
    else if (TriggerMode::GATE ==  m_trigger_mode) 
    {
        set_timing(4, 0, alt_ttl_mode, debounce, false, false, false);
    }
    
}

/**
 * Setup Time Framing source
 *
 * @param[in] time_src 0 = default software
 *                     1 = Use TTL input 1 as Veto (default is software controlled Veto)
 *                     2 = Use TTL inputs 0 and 1 as Frame Zero and Veto respectively
 *                     3 = Use 4 Pin LVDS input for veto only
 *                     4 = Use 4 Pin LVDS input for Veto and Frame 0
 * @param[in] first_frame Specify first time frame (default 0)
 * @param[in] alt_ttl_mode 0 = default timing mode
 *                         1 = TTL 0..3 output in-window0 for channels 0..3 respectively
                           2 = TTL 0..3 output in-win0(0), in-win0(1), live-level(0) and live-level(1) respectively. Count live when high
                           3 = TTL 0..3 output in-win0(0), in-win0(1), live-toggle(0) and live-toggle(1) respectively. Count live (rising edges)
                           4 = TTL 0..3 output in-win0, all-event, all-good and live-level from channel 0
                           5 = TTL 0..3 output in-win0, all-event, all-good and live-toggle from channel 0
 * @param[in] debounce Set debounce time in 80 MHz cycles to ignore glitches and ringing on Frame 0 and Framing signals
 * @param[in] loop_io Loop TTL In 0..3 to TTL OUt 0..3 for hardware testing
 * @param[in] f0_invert Invert Frame 0 input
 * @param[in] veto_invert Invert Veto input
 */
void Xspress3Handler::set_timing(int time_src, int first_frame, int alt_ttl_mode, int debounce, bool loop_io, bool f0_invert, bool veto_invert) 
{
    YAT_VERBOSE << "Xspress3Handler::set_timing()" << std::endl;

    int t_src=0;
    u_int32_t time_fixed=0;
    u_int32_t time_a=0;
    int debounce_val;
    int alt_ttl;
    switch (time_src)
    {
        case 0:
            t_src = XSP3_GTIMA_SRC_SOFTWARE;
            break;
        case 1:
            t_src = XSP3_GTIMA_SRC_INTERNAL;
            break;
        case 3:
            t_src = XSP3_GTIMA_SRC_IDC;
            break;
        case 4:
            t_src = XSP3_GTIMA_SRC_TTL_VETO_ONLY;
            break;
        case 5:
            t_src = XSP3_GTIMA_SRC_TTL_BOTH;
            break;
        case 6:
            t_src = XSP3_GTIMA_SRC_LVDS_VETO_ONLY;
            break;
        case 7:
            t_src = XSP3_GTIMA_SRC_LVDS_BOTH;
            break;
        default:
            YAT_ERROR << "Invalid time frame source" << std::endl;
    }

    time_a = XSP3_GLOB_TIMA_TF_SRC(t_src);

    if (f0_invert)
    {
        time_a |= XSP3_GLOB_TIMA_F0_INV;
    }

    if (veto_invert)
    {
        time_a |= XSP3_GLOB_TIMA_VETO_INV;
    }

    if (debounce > 255) 
    {
        YAT_ERROR << "debounce value %d, should be less 255";
    }
    
    /// Default debounce = 80 cycles, 1 us 
    debounce_val = (debounce < 0) ? 80 : debounce;
    time_a |= XSP3_GLOB_TIMA_DEBOUNCE(debounce_val);

    if (loop_io)
    {
        time_a |= XSP3_GLOB_TIMA_LOOP_IO;
    }

    switch (alt_ttl_mode) 
    {
        case       0 : alt_ttl = XSP3_ALT_TTL_TIMING_VETO; break;
        case 0x00400 : alt_ttl = XSP3_ALT_TTL_TIMING_ALL; break;
        case 0x00800 : alt_ttl = XSP3_ALT_TTL_INWINDOW; break;
        case 0x01000 : alt_ttl = XSP3_ALT_TTL_INWINLIVE; break;
        case 0x02000 : alt_ttl = XSP3_ALT_TTL_INWINLIVETOGGLE; break;
        case 0x04000 : alt_ttl = XSP3_ALT_TTL_INWINGOODLIVE; break;
        case 0x08000 : alt_ttl = XSP3_ALT_TTL_INWINGOODLIVETOGGLE; break;
        case 0x10000 : alt_ttl = XSP3_ALT_TTL_TIMING_VETO_GR; break;
        case 0x20000 : alt_ttl = XSP3_ALT_TTL_TIMING_ALL_GR; break;
        default:
            YAT_ERROR << "Invalid alternate ttl mode for TTL Out" << std::endl;
    }

    time_a |= XSP3_GLOB_TIMA_ALT_TTL(alt_ttl);

    time_fixed = (first_frame < 0) ? 0 : first_frame;

    if (xsp3_set_glob_timeA(m_handle, m_card, time_a) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }

    if (xsp3_set_glob_timeFixed(m_handle, m_card, time_fixed) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

/**
 * Setup xspress3 internal time frame generator.
 *
 * @param[in] nframe Number of time frames
 * @param[in] triggerMode
 * @param[in] gapMode Gap between frame
 */
void Xspress3Handler::set_itfg_timing(int nframes, int triggerMode, int gapMode) 
{
    YAT_INFO << "Xspress3Handler::set_itfg_timing()" << std::endl;

    int trig_mode;
    u_int32_t itime;
    int gap_mode;

    if (m_exp_time <= 0.0) 
    {
        YAT_ERROR << "Exposure time has not been set" << std::endl;
    }
    
    switch (triggerMode) 
    {
        case Burst:
            trig_mode = XSP3_ITFG_TRIG_MODE_BURST;
            break;
        case SoftwarePause:
            trig_mode = XSP3_ITFG_TRIG_MODE_SOFTWARE;
            break;
        case HardwarePause:
            trig_mode = XSP3_ITFG_TRIG_MODE_HARDWARE;
            break;
        case SoftwareOnlyFirst:
            trig_mode = XSP3_ITFG_TRIG_MODE_SOFTWARE_ONLY_FIRST;
            break;
        case HardwareOnlyFirst:
            trig_mode = XSP3_ITFG_TRIG_MODE_HARDWARE_ONLY_FIRST;
            break;
        default:
            YAT_ERROR << "Invalid trigger mode qualifiers" << std::endl;
    }

    if (m_exp_time > 12.5E-9*2.0*0x7FFFFFFF) 
    {
        YAT_ERROR << "Collection time " << m_exp_time << " too long, must be <= 12.5E-9*2.0*0x7FFFFFFF" << std::endl;
    }

    itime = (u_int32_t)(m_exp_time/12.5E-9);
    if (itime < 2) 
    {
        YAT_ERROR << "Minimum collection = 25 ns" << std::endl;

    }

    switch (gapMode) 
    {
        case Gap25ns:
            gap_mode = XSP3_ITFG_GAP_MODE_25NS;
            break;
        case Gap200ns:
            gap_mode = XSP3_ITFG_GAP_MODE_200NS;
            break;
        case Gap500ns:
            gap_mode = XSP3_ITFG_GAP_MODE_500NS;
            break;
        case Gap1us:
        default:
            gap_mode = XSP3_ITFG_GAP_MODE_1US;
            break;
    }

    // use cardNos = 0 even in multi card systems for synchronisation
    if (xsp3_itfg_setup(m_handle, 0, nframes, itime, trig_mode, gap_mode) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

/**
 * Set up ADC data processing clocks source.
 *
 * @param[in] clk_src clock source {@see #ClockSrc}
 * @param[in] flags setup flags {@see #ClockFlags}
 * @param[in] tp_type test pattern type in the IO spartan FPGAs
 */
void Xspress3Handler::setup_clocks(int clk_src, int flags, int tp_type)
{
    YAT_INFO << "Xspress3Handler::setup_clocks() " << std::endl;
    if (xsp3_clocks_setup(m_handle, m_card, clk_src, flags, tp_type) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

/**
 * Set the run mode flags
 *
 * @param[in] scalers
 * @param[in] hist
 * @param[in] playback
 * @param[in] scope
 */
void Xspress3Handler::set_run_mode(bool playback, bool scope, bool scalers, bool hist) 
{
    YAT_INFO << "Xspress3Handler::set_run_mode" << std::endl;    
    int flags = 0;
    
    if (playback)
    {
        flags |= XSP3_RUN_FLAGS_PLAYBACK;
    }

    if (scope)
    {
        flags |= XSP3_RUN_FLAGS_SCOPE;
    }

    if (scalers)
    {
        flags |= XSP3_RUN_FLAGS_SCALERS;
    }

    if (hist)
    {
        flags |= XSP3_RUN_FLAGS_HIST;
    }

    if (xsp3_set_run_flags(m_handle, flags) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

void Xspress3Handler::restore_settings(bool force_mismatch) 
{
    YAT_INFO << "Xspress3Handler::restor_settings() " << std::endl;
   
    if (xsp3_restore_settings(m_handle, (char*) m_config_directory_name.c_str(), force_mismatch) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

void Xspress3Handler::init_roi(int chan) 
{
    YAT_INFO << "Xspress3Handler::init_roi()" << std::endl;

    if (xsp3_init_roi(m_handle, chan) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

void Xspress3Handler::restart() 
{
    YAT_VERBOSE << "Xspress3Handler::restart()" << std::endl;

    if (xsp3_histogram_continue(m_handle, m_card) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

void Xspress3Handler::start() 
{
    YAT_INFO << "Xspress3Handler::start()" << std::endl;

    if (xsp3_histogram_start(m_handle, m_card) < 0)
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

void Xspress3Handler::pause() 
{
    YAT_VERBOSE << "Xspress3Handler::pause()" << std::endl;

    if (xsp3_histogram_pause(m_handle, m_card) < 0) 
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }
}

void Xspress3Handler::stop() 
{
    YAT_INFO << "Xspress3Handler::stop()" << std::endl;

    if (xsp3_histogram_stop(m_handle, m_card) < 0)
    {
        YAT_ERROR << xsp3_get_error_message() << std::endl;
    }

    int idle_count = 0;
    while (idle_count < 2) 
    {
        struct timespec delay, remain;
        delay.tv_sec = 0;
        delay.tv_nsec = (int)(1E9*(0.01)); // sleep for 10msec
        nanosleep(&delay, &remain);
        if (xsp3_histogram_is_any_busy(m_handle) == 0) 
        {
            ++idle_count;
        }
    }
}

}
