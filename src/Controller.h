/*************************************************************************
/*! 
 * \file Controller.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class Controller
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <tango.h>

#include "DataStore.h"

#include <yat/memory/UniquePtr.h>
#include <yat/threading/Mutex.h>
#include <yat4tango/DeviceTask.h>

#include "Acquisition.h"
#include "AttrView.h"
#include "Stream.h"
#include "StreamCsv.h"
#include "StreamNexus.h"
#include "StreamNo.h"

// ============================================================================
// SOME USER DEFINED MESSAGES FOR THE CAPTURE TASK
// ============================================================================
#define kMSG_START_CAPTURE            (yat::FIRST_USER_MSG + 1001)
#define kMSG_END_CAPTURE              (yat::FIRST_USER_MSG + 1002)
#define kMSG_STOP_CAPTURE             (yat::FIRST_USER_MSG + 1003)
#define kMSG_GET_DATA                 (yat::FIRST_USER_MSG + 1004)

namespace Xspress3_ns
{
const std::string STREAM_TYPE_NO_STREAM = "NO_STREAM";
const std::string STREAM_TYPE_NEXUS = "NEXUS_STREAM";
const std::string STREAM_TYPE_CSV = "CSV_STREAM";


//--------------------------------------------------------------------------------
//! \class Controller
//! \brief class Controller
//--------------------------------------------------------------------------------
class Controller : public yat4tango::DeviceTask
{
public:
//! \brief Constructor
Controller(Tango::DeviceImpl* dev, std::vector<std::string> vec_properties, std::vector<std::string> vec_stream_items, std::string expert_stream_memory_mode, std::string expert_stream_write_mode);

//! \brief Destructor 
virtual ~Controller();

//! \brief Initilalize Acquisition and Data store and Tango View
void Initialize();

//! \brief Initilalize memorized writable attributes
void write_attribute_at_init();

void update_view();

//! \brief Informs Tango View that data is refreshed
//! \param ichannel The channel number.
//! \param current_frame The current frame.
void update_data(int ichannel, int current_frame);

//! \brief Gets number of xspress3 cards
int get_nb_cards() const;

//! \brief Gets number of acquisition channels
int get_nb_channels() const;

//! \brief Gets xspress3 firmware revision
std::string get_firmware_revision() const;

//! \brief Gets type of xspress3 card
std::string get_board_type() const;

//! \brief Gets currentMode value
std::string get_current_mode() const;

//! \brief Gets number of bins per channel
int get_nb_bins() const;

//! \brief Sets the task state
//! \param state The state.
void set_state(const Tango::DevState state);

//! \brief Sets the task status
//! \param state The status.
void set_status(const std::string& status);

//! \brief Gets the last state of the task
Tango::DevState get_state();

//! \brief Gets the last status of the task
std::string get_status();

//! \brief Get current acquired frame number
int get_current_frame() const;

//! \brief Get frames number requested by user
int get_nb_frames() const;

//! \brief Set frames number requested by user
//! \param frames Frame number requested
void set_nb_frames(int frames);

//! \brief Get requested trigger mode
TriggerMode get_trigger_mode() const;

//! \brief Set requested trigger mode
//! \param trigger_mode Requested trigger mode
void set_trigger_mode(TriggerMode trigger_mode);

//! \brief Get the requested exposure time per frame
double get_exposure_time() const;

//! \brief Set the requested exposure time per frame
//! \param exposure_time The exposure time value
void set_exposure_time(double exposure_time);

//! \brief Return True if writing data into Files enabled, False otherwise
bool get_file_generation() const;

//! \brief Enable/Disable writing data into Files enabled
//! \param is_file_generation is writing data into Files enabled
void set_is_file_generation(bool is_file_generation);

//! \brief Get the stream type. Available values are: NO_STREAM - NEXUS_STREAM - CSV_STREAM
std::string get_stream_type() const;

//! \brief Set the stream type
//! \param stream_type the stream type
void set_stream_type(std::string stream_type);

//! \brief Get the root path for generated Stream files
std::string get_stream_target_path() const;

//! \brief Set the root path for generated Stream files
//! \param stream_target_path the root path
void set_stream_target_path(std::string stream_target_path);

//! \brief Get the file name for generated Stream files
std::string get_stream_target_file() const;

//! \brief Set the file name for generated Stream files
//! \param stream_target_file the file name
void set_stream_target_file(std::string stream_target_file);

//! \brief Get the number of acquisitions for each Stream file
int get_stream_nb_acq_per_file() const;

//! \brief Set the number of acquisitions for each Stream file
//! \param stream_nb_acq_per_file the number of acquisitions 
void set_stream_nb_acq_per_file(int stream_nb_acq_per_file);

void set_dataset_prefix(const std::string& dataset_prefix);

//! \brief Start acquisition action
void start_acquisition();

//! \brief Stop acquisition action
void stop_acquisition();

//! \brief Close stream
void close_stream();

//! \brief Define rois for a specific channel
void set_rois_data(int roi_channel, std::vector< std::vector<RoisData> >& rois_data);

//! \brief Get the rois data
std::vector< std::vector<RoisData> > get_rois_data() const;

//! \brief Remove rois for a specific channel
void remove_rois_data(int roi_channel);

//! \brief Initialize rois
void reset_rois();

//! \brief Initialize stream files index
void stream_reset_index();

private:
//! \brief Implements yat4tango::DeviceTask pure virtual method
//! \param msg Message to handle.
virtual void process_message(yat::Message& msg);

//! \brief Compute internal state/status of task
void compute_state_status();

//! \brief Initialize DataStore object
void build_data_store();

//! \brief Initialize Acquisition object
void build_acquisition();

//! \brief Initialize The view object
void build_attributes();

//! \brief Initialize Stream object
void build_stream();

//! \brief Update stream attributes
void update_stream_parameters(std::string streamType, string stream_path, string stream_file, std::string stream_prefix, int stream_nb_acq_per_file);

//! \brief m_state: Device state
Tango::DevState m_state;

//! \brief m_state: Device status
std::stringstream m_status;

//! \brief m_state_lock: Mutex used to protect state and status access
yat::Mutex m_state_lock;

//! \brief m_device: Owner Device server object
Tango::DeviceImpl* m_device;

//! \brief m_board_type: Board type (XSPRESS3 or SIMULATOR)
std::string m_board_type;

//! \brief m_acquisition: Acquisition object
yat::SharedPtr<Acquisition> m_acquisition;

//! \brief m_attr_view: View object
yat::UniquePtr<AttrView>  m_attr_view;

//! \brief m_stream: Stream object
yat::SharedPtr<Stream> m_stream;

//! \brief m_store: DataStore dobject
yat::SharedPtr<DataStore> m_store;  

//! \brief Vector of string containing tango properties values
std::vector<std::string> m_vec_properties;

//! \brief m_is_file_generation: Is writing data into Files enabled
bool m_is_file_generation;

//! \brief m_stream_items: List of items that we want to store in Nexus files 
std::vector<std::string> m_stream_items;

//! \brief m_stream_type: Stream type
std::string m_stream_type;

//! \brief m_current_stream_type: Cuurent stream type
std::string m_current_stream_type;

//! \brief m_stream_memory_mode: Write data mode for the NexusCpp library
std::string m_stream_memory_mode;

//! \brief m_stream_write_mode: Memory management of the NexusCpp library
std::string m_stream_write_mode;

//! \brief m_stream_target_path: Root path for generated Stream files
std::string m_stream_target_path;

//! \brief m_current_stream_target_path: Current root path for generated Stream files
std::string m_current_stream_target_path;

//! \brief m_stream_target_file: File name for generated Stream files
std::string m_stream_target_file;

//! \brief m_current_stream_target_file:- Current file name for generated Stream files
std::string m_current_stream_target_file;

//! \brief m_stream_nb_acq_per_file: Number of acquisitions for each Stream file
int m_stream_nb_acq_per_file;

//! \brief m_current_stream_nb_acq_per_file: Cuurent number of acquisitions for each Stream file.
int m_current_stream_nb_acq_per_file;

std::string m_dataset_prefix;

//! \brief m_is_acquisition_initialized: Equal to True if the Acquisition/Driver is correctly initialized, Otherwise False
bool m_is_acquisition_initialized;

};

}

#endif // CONTROLLER_H

