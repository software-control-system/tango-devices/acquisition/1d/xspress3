/*************************************************************************
/*! 
 * \file StreamNexus.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class StreamNexus
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef STREAM_NEXUS_H
#define STREAM_NEXUS_H

#include "Stream.h"
#include <nexuscpp/nexuscpp.h>
#include <yat/utils/Logging.h>

namespace Xspress3_ns
{
//--------------------------------------------------------------------------------
//! \class StreamNexus
//! \brief specialisation class to manage the NEXUS storage
//--------------------------------------------------------------------------------
class StreamNexus : public Stream, public nxcpp::IExceptionHandler
{
public:
    //! \brief Constructor
    StreamNexus(Tango::DeviceImpl *dev);
    //! \brief Destructor
    ~StreamNexus();
    
    void init(yat::SharedPtr<DataStore> data_store);
    void open();
    void close();
    void abort();
    void reset_index();
    void update_data(int current_frame, int ichannel, yat::SharedPtr<DataStore> data_store);

    void set_target_path(const std::string& target_path);
    void set_file_name(const std::string& file_name);
    void set_memory_mode(const std::string& memory_mode);	
    void set_write_mode(const std::string& write_mode);
    void set_nb_acq_per_file(int nb_acq_per_file);
    void set_dataset_prefix(const std::string& dataset_prefix);

 void OnNexusException(const nxcpp::NexusException &ex)
    {
        YAT_VERBOSE << "StreamNexus::OnNexusException()" << endl;
        ostringstream ossMsgErr;
        ossMsgErr << endl;
        ossMsgErr << "===============================================" << endl;
        ossMsgErr << "Origin\t: " << ex.errors[0].origin << endl;
        ossMsgErr << "Desc\t: " << ex.errors[0].desc << endl;
        ossMsgErr << "Reason\t: " << ex.errors[0].reason << endl;
        ossMsgErr << "===============================================" << endl;
        YAT_ERROR << ossMsgErr.str() << endl;
        yat::MutexLock scoped_lock(m_data_lock);        
        //1 - finalize the DataStreamer , this will set m_writer = 0 in order to avoid any new push data
        if (m_writer)
        {   
            close();
        }
        //2 -  inform m_store in order to stop acquisition & to set its state to FAULT
        m_store->abort(ex.errors[0].desc);
    };
private:

    void store_statistics(int channel, double realtime, double deadtime, double icr, double ocr, unsigned long events_in_run);
    void store_spectrum_data(int channel, uint32_t* data, size_t length);

    yat::Mutex m_data_lock;
    
    //- Nexus stuff	
#if defined(USE_NX_FINALIZER)
    static nxcpp::NexusDataStreamerFinalizer m_data_streamer_finalizer;
    static bool m_is_data_streamer_finalizer_started;
#endif
    
    yat::SharedPtr<DataStore> m_store;

    std::string m_target_path;
    std::string m_file_name;
    std::string m_memory_mode;	
    std::string m_write_mode;
    std::string m_dataset_prefix;

    int m_nb_acq_per_file;
    int m_nb_bins;
    int m_nb_frames;

    nxcpp::DataStreamer* m_writer;
    std::vector<string> m_channel_names;
    std::vector<string> m_realtime_names;
    std::vector<string> m_deadtime_names;
    std::vector<string> m_icr_names;
    std::vector<string> m_ocr_names;
    std::vector<string> m_events_inrun_names;

};

}

#endif // STREAM_NEXUS_H
