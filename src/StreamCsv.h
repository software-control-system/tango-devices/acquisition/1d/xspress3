/*************************************************************************
/*! 
 * \file StreamCsv.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class StreamCsv
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef STREAM_CSV_H
#define STREAM_CSV_H

#include "Stream.h"
#include <tango.h>
#include <yat/memory/SharedPtr.h>
#include <yat/memory/UniquePtr.h>
#include <yat/threading/Mutex.h>
#include "DataStore.h"
#include <vector>

namespace Xspress3_ns
{

//--------------------------------------------------------------------------------
//! \class StreamCsv
//! \brief specialisation class to manage the CSV storage
//--------------------------------------------------------------------------------
class StreamCsv : public Stream
{
public:
//! \brief Constructor
StreamCsv(Tango::DeviceImpl *dev);
//! \brief Destructor 
virtual ~StreamCsv();

void init(yat::SharedPtr<DataStore> data_store);
void open();
void close();
void abort();
void update_data(int current_frame, int ichannel, yat::SharedPtr<DataStore> data_store);
void reset_index();

void set_target_path(const std::string& target_path);
void set_file_name(const std::string& file_name);

private:
    yat::Mutex m_data_lock;
    std::string m_target_path;
    std::string m_file_name;
};

}

#endif // STREAM_CSV_H
