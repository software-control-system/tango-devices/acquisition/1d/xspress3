/*************************************************************************
/*! 
 * \file Aquisition.h
 * \author Salwa BAHJI - SOLEIL (consultant THALES SERVICES NUMERIQUES)
 * \date 6 December 2021
 * \brief class Acquisition
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
/*************************************************************************/

#ifndef ACQUISITION_H
#define ACQUISITION_H

#include <tango.h>
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Mutex.h>
#include <yat4tango/DeviceTask.h>

#include "Driver.h"
#include "DataStore.h"

// ============================================================================
// SOME USER DEFINED MESSAGES FOR THE CAPTURE TASK
// ============================================================================
#define kMSG_START_CAPTURE            (yat::FIRST_USER_MSG + 1001)
#define kMSG_END_CAPTURE              (yat::FIRST_USER_MSG + 1002)
#define kMSG_STOP_CAPTURE             (yat::FIRST_USER_MSG + 1003)
#define kMSG_GET_DATA                 (yat::FIRST_USER_MSG + 1004)

namespace Xspress3_ns
{
const std::string BOARD_TYPE_XSPRESS3_TYPE = "XSPRESS3";
const std::string BOARD_TYPE_SIMULATOR_TYPE = "SIMULATOR";

//--------------------------------------------------------------------------------
//! \class Acquisition
//! \brief Class to manage the acquisition
//--------------------------------------------------------------------------------
class Acquisition : public yat4tango::DeviceTask
{
public:
//! \brief Constructor
Acquisition(Tango::DeviceImpl *dev, std::vector<std::string> vec_properties, yat::SharedPtr<DataStore> store);

//! \brief Destructor
virtual ~Acquisition();

//! \brief Get numbre of xspress3 cards
int get_nb_cards() const;

//! \brief Get numbre of xspress3 channels
int get_nb_channels() const;

//! \brief Get xspress3 firmware revision
std::string get_firmware_revision() const;

//! \brief Gets type of xspress3 card
std::string get_board_type() const;

//! \brief Get current acquired frame number
int get_current_frame() const;

//! \brief Get frames number requested by user
int get_nb_frames() const;

//! \brief Set frames number requested by user
//! \param frames Frame number requested
void set_nb_frames(int frames);

//! \brief Get requested trigger mode
TriggerMode get_trigger_mode() const;

//! \brief Set requested trigger mode
//! \param trigger_mode Requested trigger mode
void set_trigger_mode(TriggerMode trigger_mode);

//! \brief Get the requested exposure time per frame
double get_exposure_time() const;

//! \brief Set the requested exposure time per frame
//! \param exposure_time The exposure time value
void set_exposure_time(double exposure_time);

//! \brief Start acquisition action
void start_acquisition();

//! \brief Stop acquisition action
void stop_acquisition();

//! \brief Get current device state
Tango::DevState get_state();

//! \brief Get device status
std::string get_status();

//! \brief Get number element in spectrum data
ulong get_nb_elements() const;

//! \brief Get bins number for xspress3 channel
ulong get_nb_bins() const;

//! \brief Get number of acquisition channel scalars
ulong get_nb_scalars() const;

//! \brief Inform Task to collect the last available data
void enable_collecting_last_data();

//! \brief Inform Task that no need to collect the last data
void disable_collecting_last_data();

//! \brief Is collecting the last spectrum data is needed
bool is_need_collecting_last_data();

//! \brief Collect specturm and statistics data from the Driver and store it in the Data Store
void collect_data();

//! \brief Chek if Driver acquisition is running
bool is_running();

private:
//! \brief Implements yat4tango::DeviceTask pure virtual method
//! \param msg Message to handle.
virtual void process_message(yat::Message& msg);

//! \brief Setter of the state
//! \param state Device state
void set_state(Tango::DevState state);

//! \brief Setter of the status
//! \param status Device status
void set_status(const std::string& status);

//! m_driver: Handle library wrapper
yat::SharedPtr<Driver> m_driver;

//! \brief m_device: Owner device server
Tango::DeviceImpl* m_device;

//! \brief m_store: DataStore dobject
yat::SharedPtr<DataStore> m_store;

//! \brief m_state: Device state
Tango::DevState m_state;

//! \brief m_status: Device status
std::stringstream m_status;

//! \brief m_state_lock: Mutex used to protect state and status access
yat::Mutex m_state_lock;

//! \brief m_data_lock: Mutex used to protect data access
yat::Mutex m_data_lock;

//! \brief is collecting the last spectrum data is needed
bool m_is_need_collecting_last_data;

//! \brief Number of the collected current frame
int m_nb_frame_collected ;
};

}

#endif // ACQUISITION_H
