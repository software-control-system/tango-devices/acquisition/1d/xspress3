cmake_minimum_required(VERSION 3.15)
project(${PROJECT_NAME} CXX)

find_package(yat4tango CONFIG REQUIRED)
find_package(nexuscpp CONFIG REQUIRED)
if (NOT CMAKE_SYSTEM_NAME STREQUAL "Windows")
    find_package(crashreporting2 CONFIG)
endif()

add_compile_definitions(PROJECT_NAME=${PROJECT_NAME})
add_compile_definitions(PROJECT_VERSION=${PROJECT_VERSION})
add_compile_definitions(USE_NX_FINALIZER)
add_compile_definitions(linux)

file(GLOB_RECURSE sources
    src/*.cpp
)

set(includedirs 
    src
    sdk/include
)

add_executable(${EXECUTABLE_NAME} ${sources})
target_include_directories(${EXECUTABLE_NAME} PRIVATE ${includedirs})
target_link_libraries(${EXECUTABLE_NAME} PRIVATE yat4tango::yat4tango)
target_link_libraries(${EXECUTABLE_NAME} PRIVATE nexuscpp::nexuscpp)

target_link_directories(${EXECUTABLE_NAME} PRIVATE ./sdk/bin)
target_link_libraries(${EXECUTABLE_NAME} PRIVATE img_mod)
target_link_libraries(${EXECUTABLE_NAME} PRIVATE xspress3)

if (NOT CMAKE_SYSTEM_NAME STREQUAL "Windows")
    target_link_libraries(${EXECUTABLE_NAME} PRIVATE crashreporting2::crashreporting2)
endif()

install(TARGETS ${EXECUTABLE_NAME} DESTINATION "." RUNTIME DESTINATION bin)
install(DIRECTORY sdk/bin/ DESTINATION lib FILES_MATCHING PATTERN "*.so.1.0")
install(CODE "execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink 
    libimg_mod.so.1.0 \
    ${CMAKE_INSTALL_PREFIX}/lib/libimg_mod.so   \
    )"
)
install(CODE "execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink \
    libxspress3.so.1.0 \
    ${CMAKE_INSTALL_PREFIX}/lib/libxspress3.so   \
    )"
)
