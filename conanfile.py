from conan import ConanFile

class Xspress3Recipe(ConanFile):
    name = "xspress3"
    executable = "ds_Xspress3"
    version = "1.3.4"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Arafat Noureddine"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/acquisition/1d/xspress3"
    description = "Xspress3 device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*", "sdk/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("nexuscpp/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
