.. Xspress3 card:

Xspress3
--------

.. image:: Xspress3.png 

Introduction
``````````````
The XSPRESS3 system contains a Xilinx Virtex-5 FPGA with two embedded PowerPC processors. PPC1 manages the DMA engines.
PPC2 runs the Xilinx micro kernel and communicates to the Intel 64 bit Linux server PC by 1 GBit Ethernet,TCP sockets.
Bulk data and event lists to be histogrammed are sent from the firmware to the Server PC by 10G Ethernet, UDP.

The Xspress3 is an electronics equipment for acquiring and processing signals from X-ray, silicon or Germanium spectrometers.
It is made up of one or many boxes, each made up of one or many acquisition channels, one or many trigger inputs, and one or many synchronization outputs.
These boxes are interfaced by Ethernet and controlled by an IP protocol. 
An xspress3 library (c++ sdk) provided by Quantum allows to communicate with these boxes in order to configure the acquisition and collect the data.
The Software Development Toolkit (SDK) is provided for Linux only.

Properties
````````````````````

====================================== ========================= ===================  ===========================================
Property name                          Default value             Type                 Description
====================================== ========================= ===================  ===========================================
BaseIPAdress                           192.168.0.1               String               The base IP address of the xspress3
BaseMacAddress                         02.00.00.00.00.00         String               The base MAC address of the xspress3
BasePort                               30123                     Long                 The base port of the xspress3
BoardType                              XSPRESS3                  String               The type of the connected card. Available values: XSPRESS3 and SIMULATOR;Number of cards;Number of channels
ConfigurationFilePath                  /home/...                 String               The directory containing calibration files
Debug                                  0                         Long                 The verbose mode: 0 is off, 1 is on, 2 is verbose
ExpertStreamItems                      CHANNEL ICR OCR           Array of String      The list of Items managed by the Streamer. Each item defined here will be saved in the stream for all available channels. Available values: a mix of: CHANNEL, ICR, OCR, DEADTIME, REALTIME, EVENTSINRUN
ExpertStreamMemoryMode                 COPY                      String               The Nexus stream memory mode. Available values: COPY and NO_COPY
ExpertStreamWriteMode                  SYNCHRONOUS               String               The Nexus stream write mode. Available values: SYNCHRONOUS and ASYNCHRONOUS
ExpertStreamPollingAcquisition         50                        Double               Duration of the driver polling in order to read the current acquired frame.(specified in milliseconds)
MaxFrames                              16384                     Long                 Each card can handle 8192 frames
NbCards                                2                         Long                 Number of cards in the system
NbChannels                             4                         Long                 Each card has 2 channels
Rois                                   -1;0;4095                 Array of String      Memorize the latest Rois configuration, for channels
SpoolID                                TO_BE_DEFINED             String               Used  only by the Flyscan application
RoisFile                               ALIAS;FILE_PATH_NAME      String               Define the list of rois files ``*.txt``  and their associated alias.
StreamItemsPrefix                      	                         String               Prefix preceding each static attributes in Nexus files to differ from those coming from other Xspress3`s instances
MemorizedExposureTime                  1.0                       Double               Only the device could modify this property. Memorized exposure time value in seconds.
MemorizedNbFrames                      1                         Long                 Only the device could modify this property. Memorized frames number.
MemorizedStreamNbAcqPerFile            1                         Long                 Only the device could modify this property. Memorized number of acquisitions for each Stream file.
MemorizedStreamTargetFile              TO_BE_DEFINED             String               Only the device could modify this property. Memorized file name for generated Stream files.
MemorizedStreamTargetPath              TO_BE_DEFINED             String               Only the device could modify this property. Memorized root path for generated Stream files.
MemorizedStreamType                    NO_STREAM                 String               Only the device could modify this property. Memorized stream type. Available Values: NO_STREAM - NEXUS_STREAM - CSV_STREAM.
MemorizedTriggerMode                   INTERNAL                  String               Only the device could modify this property. Memorized trigger mode. Available Values: INTERNAL and GATE.
====================================== ========================= ===================  ===========================================


Commands
````````````````````

======================= =============== ======================= ===========================================
Command name            Arg. in         Arg. out                Description
======================= =============== ======================= ===========================================
Init                    Void            Void                    Do not use (Use Init of the Generic device)
State                   Void            Long                    Return the device state
Status                  Void            String                  Return the device state as a string
Snap                    Void            String                  Starts the acquisition of N frames and stops at the end
Stop                    Void            String                  Stops the acquisition at user's request
SetRoisFromList         String          Void                    Define Rois for each channel from user input
RemoveRois              Long            Void                    Delete Rois of channel from user input
GetRois                 Void            Array of String         Return the list of Rois configured and stored in Rois device property
StreamResetIndex        Void            Void                    Reset the stream Nexus buffer index to 1
GetDataStreams          Void            Array of String         Return the flyscan data streams associated with this device
SetRoisFromFile         String          Void                    Define a set of roi for each channel <br>
                                                                The set of roi is read from a file indicated by its alias <br>
GetRoisFilesAlias       Void            Array of String         Get the list of alias of Rois Files (The contents of RoisFiles property)
======================= =============== ======================= ===========================================


Dynamic Attributes
````````````````````````````

=============================== ======================== ================== ===============================================
Attribute name                  Read/Write               Type               Description
=============================== ======================== ================== ===============================================
firmwareRevision                R                        String             Revision of the firmware
boardType                       R                        String             The type of the connected card. Available values : XSPRESS3 / SIMULATOR
nbModules                       R                        ULong              Number of cards in the system
nbChannels                      R                        ULong              Number of channels in the system
nbBins                          R                        ULong              Number of bins per channel
triggerMode                     R/W                      Enum               The current trigger mode. Available values : INTERNAL / GATE
nbFrames                        R/W                      ULong              Number of frames for the acquisition
currentFrame                    R                        ULong              The current frame being acquired
exposureTime                    R/W                      Double             Exposure time per frame for acquisition (specified in seconds)
realTime(i)                     R                        Double             TotalTicks*12.5/1E9 => scaler8*12.5/1E9
deadTime(i)                     R                        Double             Deadtime % => scaler9
outputCountRate(i)              R                        Double             Output Count Rate: AllGood/Time => scaler4/(scaler8/80000000.0)
inputCountRate(i)               R                        Double             Input Count Rate: OCR/(1-(Deadtime/100))
                                                                            => (scaler4/(scaler8/80000000.0))/(1-(scaler10 /100))
eventsInRun                     R                        ULong              Number of all event triggers => scaler3
channel(i)                      R                        ULong              Histogram of channel datas
roi(i)_(j)                      R                        ULong              Region Of Interest j for channel i : spectrum sum for roi user input
fileGeneration                  R/W                      Boolean            Available only for expert user. Enable/Disable writing the data to a file.
streamType                      W                        String             Available only for expert user. The curent stream type. Available values :NO_STREAM - NEXUS_STREAM - CSV_STREAM.
streamTargetPath                W                        String             Available only for expert user. The root path for generated Stream files.
streamTargetFile                W                        String             Available only for expert user. The file name for generated Stream files.
streamNbAcqPerFile              W                        Long               Available only for expert user. The number of acquisitions for each Stream file.
=============================== ======================== ================== ===============================================

i = number of channel

The list of the 11 scalars returned by Xspress3, for each channel:

=============================== ============================ ===============================================
Scalars Number                  Name                         Description
=============================== ============================ ===============================================
scaler0                         Time                         Total exposure time, may show lower than programmed time if data packets are dropped.Can be used to scale for dropped packets.
scaler1                         ResetTicks                   Time in Reset or reset crostalk glitch padding.
scaler2                         ResetCount                   Number of Resets.
scaler3                         AllEvent                     Number of all event triggers.
scaler4                         AllGood                      Number of events with positivie energy > Good threshold.
scaler5                         InWIn 0                      Number of events in window 0.
scaler6                         InWIn 1                      Number of events in window 1.
scaler7                         PileUp                       Number of events detected as pileup.
scaler8                         TotalTicks                   Total time in 80MHz ticks of exposure, even if some packets are dropped.
scaler9                         Deadtime %                   Deadtime %
scaler10                        Deadtime correction factor   Deadtime correction factor
=============================== ============================ ===============================================

